package DatabaseClasses;
import java.io.BufferedReader;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.DriverManager;

import javax.swing.JOptionPane;

public class DBConnection {
	
	public Connection con() {
		
		String IPAddress = "";
		String UserName = "";
		String PassWord = "";
		
// LOAD DBCONFIG.CFG
		try {
	        FileReader fstream = new FileReader("dbconfig.cfg");
	        BufferedReader info = new BufferedReader(fstream);
	           IPAddress = info.readLine();
	           UserName = info.readLine();
	           PassWord = info.readLine();
	        info.close();
	    } 
		catch (Exception e1) {
			System.out.println(e1);
	        System.out.println("A r error has occurred");
	    }
		
// DATABASE CONNECTIVITY
		try {
			String driver = "com.mysql.cj.jdbc.Driver";
			String url = "jdbc:mysql://"+IPAddress+"/studentinfo";
			Class.forName(driver);
			
			return DriverManager.getConnection(url,UserName, PassWord);
			
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Deatils of Error: " + e, "Connection Failed!", JOptionPane.ERROR_MESSAGE);
		}
		return null;
	}
}
