package DatabaseClasses;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.swing.JOptionPane;

import LoginClassComponents.LoginFrame;
import WindowFrameClasses.PreferencesFrame;
import WindowFrameClasses.StudentManagementFrame;
import WindowFrameClasses.SuspendedStudentsFrame;
import WindowFrameClasses.ViolationRecordFrame;
import WindowFrameClasses.mainFrame;
import net.proteanit.sql.DbUtils;
import java.time.format.DateTimeFormatter;  
import java.time.LocalDateTime;   

public class userAccountTracker {
	
	DBConnection db = new DBConnection();
	public static String name;
	private String dispName;
	
	public void logInTrigger() {
        LoginFrame frame = new LoginFrame();
        
        String query, query2;
        try {
            query = "SELECT name from studentinfo.credentialsdb WHERE (Username = '"+ frame.uName +"')";
            PreparedStatement pst = db.con().prepareStatement(query);
            ResultSet rs = pst.executeQuery();

            if (rs.next()) {
                name = rs.getString(1);
            }
            db.con().close();
        } catch (Exception e1) {
            System.out.println(e1);
        }

        try {
            query2 = "UPDATE studentinfo.credentialsdb SET activeStatus = '1' WHERE (Username = '"+ frame.uName +"');";
            PreparedStatement pst = db.con().prepareStatement(query2);
            pst.executeUpdate();
            
            db.con().close();
        } catch (Exception e) {
            // TODO: handle exception
        }
    }
	
	public void logOutTrigger() {
		try {
			String query = "UPDATE `studentinfo`.`credentialsdb` SET `activeStatus` = '0' WHERE (`activeStatus` = 1);";
			PreparedStatement pst = db.con().prepareStatement(query);
			pst.executeUpdate();
			
			db.con().close();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	public void accountDeleting() {
		StudentManagementFrame frame = new StudentManagementFrame();
		//Time Code
			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd hh:mm:ss");  
			LocalDateTime now = LocalDateTime.now();
			try {
					String query = "INSERT INTO `studentinfo`.`activitylogdb` "
							+ "(`Name`, `Action`, `Details`, `Date and Time`) "
							+ "VALUES (?, ?, ?, ?);";
							
					PreparedStatement pst = db.con().prepareStatement(query);
					pst.setString(1, name);
					pst.setString(2, "Deleted");
					pst.setString(3, frame.deletedStudentName);
					pst.setString(4, dtf.format(now));
					pst.execute();
					
					db.con().close();
			} catch (Exception e) {
				JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
			}
	}
	
	public void accountCreating() {
			StudentManagementFrame frame = new StudentManagementFrame();
			//Time Code
				DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd hh:mm:ss");  
				LocalDateTime now = LocalDateTime.now();
				
				try {
					String query = "INSERT INTO `studentinfo`.`activitylogdb` "
								+ "(`Name`, `Action`, `Details`, `Date and Time`) "
								+ "VALUES (?, ?, ?, ?);";
									
						PreparedStatement pst = db.con().prepareStatement(query);
						pst.setString(1, name);
						pst.setString(2, "Created");
						pst.setString(3, frame.createdStudentName);
						pst.setString(4, dtf.format(now));
						pst.execute();
						
						db.con().close();
				} catch (Exception e) {
						JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
			}
	}
	
	public void accountUpdating() {
		StudentManagementFrame frame = new StudentManagementFrame();
		//Time Code
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd hh:mm:ss");  
		LocalDateTime now = LocalDateTime.now();
		
			try {
				String query = "INSERT INTO `studentinfo`.`activitylogdb` "
								+ "(`Name`, `Action`, `Details`, `Date and Time`) "
									+ "VALUES (?, ?, ?, ?);";
							
						PreparedStatement pst = db.con().prepareStatement(query);
						pst.setString(1, name);
						pst.setString(2, "Updated");
						pst.setString(3, frame.updatedStudentName);
						pst.setString(4, dtf.format(now));
						pst.execute();
						
						db.con().close();
			} catch (Exception e) {
				JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
		}
	}
	
	public void violationClearing() {
		ViolationRecordFrame frame = new ViolationRecordFrame();
		//Time Code
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd hh:mm:ss");  
		LocalDateTime now = LocalDateTime.now();
		
		try {
			String query = "INSERT INTO `studentinfo`.`activitylogdb` "
						+ "(`Name`, `Action`, `Details`, `Date and Time`) "
						+ "VALUES (?, ?, ?, ?);";
									
				PreparedStatement pst = db.con().prepareStatement(query);
				pst.setString(1, name);
				pst.setString(2, "Cleared Violation");
				pst.setString(3, frame.clearedViolationName);
				pst.setString(4, dtf.format(now));
				pst.execute();
				
				db.con().close();
		} catch (Exception e) {
		}
	}
	
	public void violationDeleting() {
		ViolationRecordFrame frame = new ViolationRecordFrame();
		//Time Code
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd hh:mm:ss");  
		LocalDateTime now = LocalDateTime.now();
		
		try {
			String query = "INSERT INTO `studentinfo`.`activitylogdb` "
						+ "(`Name`, `Action`, `Details`, `Date and Time`) "
						+ "VALUES (?, ?, ?, ?);";
									
				PreparedStatement pst = db.con().prepareStatement(query);
				pst.setString(1, name);
				pst.setString(2, "Deleted Violation");
				pst.setString(3, frame.deletedViolationName);
				pst.setString(4, dtf.format(now));
				pst.execute();
				
				db.con().close();			
		} catch (Exception e) {
			}
	}
	
	public void suspensionClearing() {
		SuspendedStudentsFrame frame = new SuspendedStudentsFrame();
		//Time Code
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd hh:mm:ss");  
		LocalDateTime now = LocalDateTime.now();
		
		try {
			String query = "INSERT INTO `studentinfo`.`activitylogdb` "
						+ "(`Name`, `Action`, `Details`, `Date and Time`) "
						+ "VALUES (?, ?, ?, ?);";
									
				PreparedStatement pst = db.con().prepareStatement(query);
				pst.setString(1, name);
				pst.setString(2, "Cleared Suspension");
				pst.setString(3, frame.clearedSuspensionName);
				pst.setString(4, dtf.format(now));
				pst.execute();
				
				db.con().close();	
		} catch (Exception e) {
			}
	}
	
	public void dataImport() {
		//Time Code
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd hh:mm:ss");  
		LocalDateTime now = LocalDateTime.now();
		
		try {
			String query = "INSERT INTO `studentinfo`.`activitylogdb` "
						+ "(`Name`, `Action`, `Details`, `Date and Time`) "
						+ "VALUES (?, ?, ?, ?);";
									
				PreparedStatement pst = db.con().prepareStatement(query);
				pst.setString(1, name);
				pst.setString(2, "CSV File Import");
				pst.setString(3, "Batch of Students");
				pst.setString(4, dtf.format(now));
				pst.execute();
				
				db.con().close();				
		} catch (Exception e) {
			}
	}
	
	public void dataExport() {
		//Time Code
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd hh:mm:ss");  
		LocalDateTime now = LocalDateTime.now();
		
		try {
			String query = "INSERT INTO `studentinfo`.`activitylogdb` "
						+ "(`Name`, `Action`, `Details`, `Date and Time`) "
						+ "VALUES (?, ?, ?, ?);";
									
				PreparedStatement pst = db.con().prepareStatement(query);
				pst.setString(1, name);
				pst.setString(2, "CSV File Export");
				pst.setString(3, "Batch of Students");
				pst.setString(4, dtf.format(now));
				pst.execute();
				
				db.con().close();				
		} catch (Exception e) {
			}
	}
	
	public void generateReport() {
		//Time Code
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd hh:mm:ss");  
		LocalDateTime now = LocalDateTime.now();
		
		try {
			String query = "INSERT INTO `studentinfo`.`activitylogdb` "
						+ "(`Name`, `Action`, `Details`, `Date and Time`) "
						+ "VALUES (?, ?, ?, ?);";
									
				PreparedStatement pst = db.con().prepareStatement(query);
				pst.setString(1, name);
				pst.setString(2, "PDF File Export");
				pst.setString(3, "Report Generation");
				pst.setString(4, dtf.format(now));
				pst.execute();
				db.con().close();				
		} 
		catch (Exception e) {
			}
	}
	
	public void detectMode() {
		//Time Code
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd hh:mm:ss");  
		LocalDateTime now = LocalDateTime.now();
		
		try {
			String query = "INSERT INTO `studentinfo`.`activitylogdb` "
						+ "(`Name`, `Action`, `Details`, `Date and Time`) "
						+ "VALUES (?, ?, ?, ?);";
									
				PreparedStatement pst = db.con().prepareStatement(query);
				pst.setString(1, name);
				pst.setString(2, "Object Recognition State");
				pst.setString(3, "Toggle Detection Mode");
				pst.setString(4, dtf.format(now));
				pst.execute();
				db.con().close();				
		} 
		catch (Exception e) {
			}
	}
	
	public void idleMode() {
		//Time Code
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd hh:mm:ss");  
		LocalDateTime now = LocalDateTime.now();
		
		try {
			String query = "INSERT INTO `studentinfo`.`activitylogdb` "
						+ "(`Name`, `Action`, `Details`, `Date and Time`) "
						+ "VALUES (?, ?, ?, ?);";
									
				PreparedStatement pst = db.con().prepareStatement(query);
				pst.setString(1, name);
				pst.setString(2, "Object Recognition State");
				pst.setString(3, "Toggle Idle Mode");
				pst.setString(4, dtf.format(now));
				pst.execute();
				db.con().close();				
		} 
		catch (Exception e) {
			}
	}
	
	public String rootName() {
		String userName = "EACAdmin";
		return userName;
	}
	
	public String rootPass() {
		String passWord = "eac-cavitesystems2022";
		return passWord;
	}
}