package DatabaseClasses;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import LoginClassComponents.splashScreen;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.border.LineBorder;
import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class dbConfig extends JFrame {

	private JPanel contentPane;
	private JTextField field_IPAddress;
	private JTextField field_userName;
	private JPasswordField field_passWord;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					dbConfig frame = new dbConfig();
					frame.setLocationRelativeTo(null);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public dbConfig() {
		setTitle("Database Configuration");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 575, 390);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lbl_ActionHistory = new JLabel("Reconfiguration Required");
		lbl_ActionHistory.setFont(new Font("Baskerville Old Face", Font.PLAIN, 22));
		lbl_ActionHistory.setBounds(10, 11, 233, 40);
		contentPane.add(lbl_ActionHistory);
		
		JTextArea lbl_descriptionActLog1 = new JTextArea();
		lbl_descriptionActLog1.setText("To access the database, re-enter the correct IP address of the host device of the Uniform \r\nRecognition along with the credentials of the data server.");
		lbl_descriptionActLog1.setOpaque(false);
		lbl_descriptionActLog1.setFont(new Font("Baskerville Old Face", Font.PLAIN, 15));
		lbl_descriptionActLog1.setEditable(false);
		lbl_descriptionActLog1.setBounds(20, 62, 529, 40);
		contentPane.add(lbl_descriptionActLog1);
		
		JLabel hostAddLBL = new JLabel("Host Address");
		hostAddLBL.setFont(new Font("Baskerville Old Face", Font.PLAIN, 17));
		hostAddLBL.setBounds(84, 149, 93, 33);
		contentPane.add(hostAddLBL);
		
		field_IPAddress = new JTextField("");
		field_IPAddress.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_ENTER) {
					dbConfigSave();
				}
			}
		});
		field_IPAddress.setColumns(10);
		field_IPAddress.setBounds(187, 149, 275, 29);
		contentPane.add(field_IPAddress);
		
		field_userName = new JTextField("");
		field_userName.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_ENTER) {
					dbConfigSave();
				}
			}
		});
		field_userName.setColumns(10);
		field_userName.setBounds(187, 193, 275, 29);
		contentPane.add(field_userName);
		
		JLabel uNameLBL = new JLabel("Username");
		uNameLBL.setFont(new Font("Baskerville Old Face", Font.PLAIN, 17));
		uNameLBL.setBounds(104, 189, 73, 33);
		contentPane.add(uNameLBL);
		
		JLabel passwordLBL = new JLabel("Password");
		passwordLBL.setFont(new Font("Baskerville Old Face", Font.PLAIN, 17));
		passwordLBL.setBounds(109, 233, 68, 33);
		contentPane.add(passwordLBL);
		
		field_passWord = new JPasswordField("");
		field_passWord.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_ENTER) {
					dbConfigSave();
				}
			}
		});
		field_passWord.setBounds(187, 233, 275, 33);
		contentPane.add(field_passWord);
		
		JButton btn_saveConfig = new JButton("");
		btn_saveConfig.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				btn_saveConfig.setIcon(new ImageIcon(dbConfig.class.getResource("/imgSource/BtnSave.png")));
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btn_saveConfig.setIcon(new ImageIcon(dbConfig.class.getResource("/imgSource/BtnSave.png")));
			}
			@Override
			public void mouseEntered(MouseEvent e) {
				btn_saveConfig.setIcon(new ImageIcon(dbConfig.class.getResource("/imgSource/BtnSave_hover.png")));
			}
			@Override
			public void mousePressed(MouseEvent e) {
				btn_saveConfig.setIcon(new ImageIcon(dbConfig.class.getResource("/imgSource/BtnSave_clicked.png")));
			}
		});
		btn_saveConfig.setIcon(new ImageIcon(dbConfig.class.getResource("/imgSource/BtnSave.png")));
		btn_saveConfig.setBorder(new LineBorder(new Color(0, 0, 0)));
		btn_saveConfig.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dbConfigSave();
			}
		});
		btn_saveConfig.setFont(new Font("Baskerville Old Face", Font.PLAIN, 16));
		btn_saveConfig.setBounds(157, 298, 120, 33);
		contentPane.add(btn_saveConfig);
		
		JButton btn_Cancel = new JButton("");
		btn_Cancel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				btn_Cancel.setIcon(new ImageIcon(dbConfig.class.getResource("/imgSource/BtnCancel.png")));
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btn_Cancel.setIcon(new ImageIcon(dbConfig.class.getResource("/imgSource/BtnCancel.png")));
			}
			@Override
			public void mouseEntered(MouseEvent e) {
				btn_Cancel.setIcon(new ImageIcon(dbConfig.class.getResource("/imgSource/BtnCancel_hover.png")));
			}
			@Override
			public void mousePressed(MouseEvent e) {
				btn_Cancel.setIcon(new ImageIcon(dbConfig.class.getResource("/imgSource/BtnCancel_clicked.png")));
			}
		});
		btn_Cancel.setIcon(new ImageIcon(dbConfig.class.getResource("/imgSource/BtnCancel.png")));
		btn_Cancel.setBorder(new LineBorder(new Color(0, 0, 0)));
		btn_Cancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int response = JOptionPane.showConfirmDialog(null, "No changes were made? Are you sure do you want to exit?", "Warning!", JOptionPane.YES_OPTION, JOptionPane.NO_OPTION);
				if(response == JOptionPane.YES_OPTION) {
					dispose();
				}
			}
		});
		btn_Cancel.setFont(new Font("Baskerville Old Face", Font.PLAIN, 16));
		btn_Cancel.setBounds(287, 298, 120, 33);
		contentPane.add(btn_Cancel);
		
		JLabel BG = new JLabel("");
		BG.setIcon(new ImageIcon(dbConfig.class.getResource("/imgSource/dbConfigBG.jpg")));
		BG.setBounds(0, 0, 559, 351);
		contentPane.add(BG);
	}
	
	public void dbConfigSave() {
		
		String IPAddress = field_IPAddress.getText();
		String UserName = field_userName.getText();
		String PassWord = field_passWord.getText();
		
		if(IPAddress.equals("") || UserName.equals("") || PassWord.equals("")) {
			JOptionPane.showMessageDialog(null, "Incomplete Credentials","Invalid Input",JOptionPane.WARNING_MESSAGE);
		}
		else {
			try {
				FileWriter fstream = new FileWriter("dbconfig.cfg");
				BufferedWriter info = new BufferedWriter(fstream);
	            	info.write(IPAddress);
	            	info.newLine();
	            	info.write(UserName);
	            	info.newLine();
	            	info.write(PassWord);
	            	info.close();
	            	JOptionPane.showMessageDialog(null, "Configuration Updated! The application will restart.");
	            	dispose();
	            	
	            	splashScreen frame = new splashScreen();
	            	frame.setLocationRelativeTo(null);
	            	frame.setVisible(true);
	            	
			} 
			catch (Exception e1) {
				System.out.println(e1);
				System.out.println("A write error has occurred");
			}
		}
	}
	
}
