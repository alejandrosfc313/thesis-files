package LoginClassComponents;
import java.awt.*;
import javax.swing.*;
import javax.swing.border.EmptyBorder;

import DatabaseClasses.DBConnection;
import DatabaseClasses.dbConfig;
import DatabaseClasses.dbConfig;
import DatabaseClasses.userAccountTracker;
import it.sauronsoftware.junique.AlreadyLockedException;
import it.sauronsoftware.junique.JUnique;

import java.awt.event.*;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class splashScreen extends JFrame {

	private JPanel contentPane;
	private Timer timer;
	private static JProgressBar loadingProgress;
	DBConnection db = new DBConnection();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				String appId = "myapplicationid";
				boolean alreadyRunning;
				try {
					JUnique.acquireLock(appId);
					alreadyRunning = false;
				} catch (AlreadyLockedException e) {
					alreadyRunning = true;
					JOptionPane.showMessageDialog(null, "Application is already running!");
				}
				if (!alreadyRunning) {
					// Start sequence here
					splashScreen frame = new splashScreen();
					frame.setVisible(true);
					frame.setLocationRelativeTo(null);
				}
			}
		});
	}
	/**
	 * Create the frame.
	 */
	public splashScreen() {
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 490, 320);
		setUndecorated(true);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		loadingProgress = new JProgressBar();
		loadingProgress.setBounds(115, 268, 265, 17);
		loadingProgress.setValue(0);
		loadingProgress.setStringPainted(true);
		contentPane.add(loadingProgress);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon(splashScreen.class.getResource("/imgSource/SplashScreen.jpg")));
		lblNewLabel.setBounds(0, 0, 490, 320);
		contentPane.add(lblNewLabel);
		
		
		ActionListener updateProBar = new ActionListener() {
			public void actionPerformed(ActionEvent actionEvent) {
				int val = loadingProgress.getValue();
				
				if (val >= 100) {
					timer.stop();
					accountChecker();
					return;
				}
		        	loadingProgress.setValue(++val);
				}
			};
		    
		    timer = new Timer(2, updateProBar);
		    timer.start();
	}
	
	public void accountChecker() {
		try {                 																

            PreparedStatement pst = (PreparedStatement) db.con().prepareStatement("SELECT Username, Password from studentinfo.credentialsdb");

            ResultSet rs = pst.executeQuery();
            if (rs.next()) {        	
				LoginFrame frame = new LoginFrame();
				userAccountTracker uAT = new userAccountTracker();
				
				uAT.logOutTrigger();
				frame.setVisible(true);
				dispose();
				
            } else {
                InitialLoginFrame frame = new InitialLoginFrame();
                frame.setVisible(true);
                dispose();
                
            }
            db.con().close();
        } 
		catch (Exception e) {
            int response = JOptionPane.showConfirmDialog(null, "Database Reconfiguration Required, proceed?", "Warning!", JOptionPane.YES_OPTION, JOptionPane.NO_OPTION);
			if(response == JOptionPane.YES_OPTION) {
				dispose();
				
				dbConfig frame = new dbConfig();
				frame.setLocationRelativeTo(null);
				frame.setVisible(true);
			}
			else {
				JOptionPane.showMessageDialog(null, "The Application will now close");
				System.exit(0);
			}
       }
	}
}
