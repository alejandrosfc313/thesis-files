package LoginClassComponents;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import DatabaseClasses.DBConnection;
import DatabaseClasses.userAccountTracker;
import accountManagementClasses.RegisterFrame;
import it.sauronsoftware.junique.AlreadyLockedException;
import it.sauronsoftware.junique.JUnique;
import javax.swing.border.LineBorder;
import java.awt.Color;

public class InitialLoginFrame extends JFrame {

	private JPanel contentPane;
	private JTextField textField_user;
	private JPasswordField passwordField;
	private JButton btn_login;
	DBConnection db = new DBConnection();
	userAccountTracker uAT = new userAccountTracker();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				String appId = "myapplicationid";
				boolean alreadyRunning;
				try {
					JUnique.acquireLock(appId);
					alreadyRunning = false;
				} catch (AlreadyLockedException e) {
					alreadyRunning = true;
					JOptionPane.showMessageDialog(null, "Application is already running!");
				}
				if (!alreadyRunning) {
					// Start sequence here
					InitialLoginFrame frame = new InitialLoginFrame();
					frame.setVisible(true);
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public InitialLoginFrame() {
		setTitle("Violation Monitoring Dashboard - Authenticator");
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 455, 275);
		setLocationRelativeTo(null);
		setUndecorated(false);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		textField_user = new JTextField();
		textField_user.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_ENTER) {
					btn_login.doClick();
				}
			}
		});
		textField_user.setBounds(140, 49, 220, 37);
		contentPane.add(textField_user);
		textField_user.setColumns(10);
		
		passwordField = new JPasswordField();
		passwordField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_ENTER) {
					btn_login.doClick();
				}
			}
		});
		passwordField.setBounds(140, 97, 220, 37);
		contentPane.add(passwordField);
		
		JLabel lbl_user = new JLabel("Username");
		lbl_user.setFont(new Font("Baskerville Old Face", Font.PLAIN, 16));
		lbl_user.setBounds(64, 49, 66, 37);
		contentPane.add(lbl_user);
		
		JLabel lbl_pw = new JLabel("Password");
		lbl_pw.setFont(new Font("Baskerville Old Face", Font.PLAIN, 16));
		lbl_pw.setBounds(68, 97, 62, 37);
		contentPane.add(lbl_pw);
		
		btn_login = new JButton("");
		btn_login.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				btn_login.setIcon(new ImageIcon(InitialLoginFrame.class.getResource("/imgSource/LoginBtn.png")));
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btn_login.setIcon(new ImageIcon(InitialLoginFrame.class.getResource("/imgSource/LoginBtn.png")));
			}
			@Override
			public void mouseEntered(MouseEvent e) {
				btn_login.setIcon(new ImageIcon(InitialLoginFrame.class.getResource("/imgSource/LoginBtn_hover.png")));
			}
			@Override
			public void mousePressed(MouseEvent e) {
				btn_login.setIcon(new ImageIcon(InitialLoginFrame.class.getResource("/imgSource/LoginBtn_clicked.png")));
			}
		});
		btn_login.setIcon(new ImageIcon(InitialLoginFrame.class.getResource("/imgSource/LoginBtn.png")));
		btn_login.setBorder(new LineBorder(new Color(0, 0, 0)));
		btn_login.setFont(new Font("Baskerville Old Face", Font.PLAIN, 18));
		btn_login.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String userName = textField_user.getText();
                String password = passwordField.getText().toString();
				
				if((userName.equals("")) || (password.equals(""))) {
					JOptionPane.showMessageDialog(null, "Incomplete credentials!", "Warning", JOptionPane.WARNING_MESSAGE);
				}
				else if (userName.equals(uAT.rootName()) && (password.equals(uAT.rootPass()))) {
					JOptionPane.showMessageDialog(null, "Proceed to accound registration.", "Welcome", JOptionPane.INFORMATION_MESSAGE);
					dispose();
					
					RegisterFrame frame = new RegisterFrame();
					frame.setVisible(true);
					
				}
				else {
					JOptionPane.showMessageDialog(null, "Invalid credentials!", "Error", JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		btn_login.setBounds(167, 164, 110, 45);
		contentPane.add(btn_login);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon(InitialLoginFrame.class.getResource("/imgSource/login bg.jpg")));
		lblNewLabel.setBounds(0, 0, 439, 236);
		contentPane.add(lblNewLabel);
	}
}
