package LoginClassComponents;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import DatabaseClasses.DBConnection;
import DatabaseClasses.userAccountTracker;
import WindowFrameClasses.mainFrame;
import accountManagementClasses.AccountRecoveryFrame;
import it.sauronsoftware.junique.AlreadyLockedException;
import it.sauronsoftware.junique.JUnique;
import javax.swing.border.LineBorder;
import java.awt.Color;

public class LoginFrame extends JFrame {

	private JPanel contentPane;
	private JTextField textField_user;
	private JPasswordField passwordField;
	private JButton btn_login;
	DBConnection db = new DBConnection();
	userAccountTracker uAT = new userAccountTracker();
	public static String uName;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				String appId = "myapplicationid";
				boolean alreadyRunning;
				try {
					JUnique.acquireLock(appId);
					alreadyRunning = false;
				} catch (AlreadyLockedException e) {
					alreadyRunning = true;
					JOptionPane.showMessageDialog(null, "Application is already running!");
				}
				if (!alreadyRunning) {
					// Start sequence here
					LoginFrame frame = new LoginFrame();
					frame.setVisible(true);
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public LoginFrame() {		
		setTitle("Violation Monitoring Dashboard - Login");
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 455, 284);
		setLocationRelativeTo(null);
		setUndecorated(false);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		textField_user = new JTextField();
		textField_user.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_ENTER) {
					userLogin();
				}
			}
		});
		
		JButton btn_forgotPass = new JButton("Forgot Password?");
		btn_forgotPass.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AccountRecoveryFrame frame = new AccountRecoveryFrame();
				frame.setVisible(true);
				dispose();
				
			}
		});
		btn_forgotPass.setOpaque(false);
		btn_forgotPass.setFont(new Font("Baskerville Old Face", Font.PLAIN, 14));
		btn_forgotPass.setContentAreaFilled(false);
		btn_forgotPass.setBorderPainted(false);
		btn_forgotPass.setBounds(295, 211, 134, 23);
		contentPane.add(btn_forgotPass);
		textField_user.setBounds(141, 53, 220, 37);
		contentPane.add(textField_user);
		textField_user.setColumns(10);
		
		passwordField = new JPasswordField();
		passwordField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_ENTER) {
					userLogin();
				}
			}
		});
		passwordField.setBounds(141, 101, 220, 37);
		contentPane.add(passwordField);
		
		JLabel lbl_user = new JLabel("Username");
		lbl_user.setFont(new Font("Baskerville Old Face", Font.PLAIN, 16));
		lbl_user.setBounds(65, 53, 66, 37);
		contentPane.add(lbl_user);
		
		JLabel lbl_pw = new JLabel("Password");
		lbl_pw.setFont(new Font("Baskerville Old Face", Font.PLAIN, 16));
		lbl_pw.setBounds(71, 101, 60, 37);
		contentPane.add(lbl_pw);
		
		btn_login = new JButton("");
		btn_login.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				btn_login.setIcon(new ImageIcon(LoginFrame.class.getResource("/imgSource/LoginBtn.png")));
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btn_login.setIcon(new ImageIcon(LoginFrame.class.getResource("/imgSource/LoginBtn.png")));
			}
			@Override
			public void mouseEntered(MouseEvent e) {
				btn_login.setIcon(new ImageIcon(LoginFrame.class.getResource("/imgSource/LoginBtn_hover.png")));
			}
			@Override
			public void mousePressed(MouseEvent e) {
				btn_login.setIcon(new ImageIcon(LoginFrame.class.getResource("/imgSource/LoginBtn_clicked.png")));
			}
		});
		btn_login.setBorder(new LineBorder(new Color(0, 0, 0)));
		btn_login.setIcon(new ImageIcon(LoginFrame.class.getResource("/imgSource/LoginBtn.png")));
		btn_login.setFont(new Font("Baskerville Old Face", Font.PLAIN, 18));
		btn_login.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				userLogin();
			}
		});
		btn_login.setBounds(164, 158, 110, 45);
		contentPane.add(btn_login);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon(LoginFrame.class.getResource("/imgSource/login bg.jpg")));
		lblNewLabel.setBounds(0, 0, 439, 245);
		contentPane.add(lblNewLabel);
	}
	
	public void userLogin() {

		String userName = textField_user.getText();
        String password = passwordField.getText().toString();
		
		
		if((userName.equals("")) || (password.equals(""))) {
			JOptionPane.showMessageDialog(null, "Incomplete credentials!", "Error", JOptionPane.ERROR_MESSAGE);
		}
		else if ((textField_user.getText().equals(uAT.rootName())) && (passwordField.getText().toString().equals(uAT.rootPass()))) {
			JOptionPane.showMessageDialog(null, "You have successfully logged in with root privileges", "Welcome SuperUser", JOptionPane.PLAIN_MESSAGE);
			dispose();
			
			mainFrame frame = new mainFrame();
			frame.setTitle("Violation Monitoring Dashboard (Root Access)");
			frame.setVisible(true);
			frame.start();
			
		}else {
			try {                 																
                PreparedStatement pst = (PreparedStatement) db.con().prepareStatement("SELECT Username, Password from studentinfo.credentialsdb WHERE BINARY Username=? and Password=?");
                pst.setString(1, userName);
                pst.setString(2, password);
                ResultSet rs = pst.executeQuery();
                if (rs.next()) {
					JOptionPane.showMessageDialog(null, "You have successfully logged in!", "Welcome", JOptionPane.PLAIN_MESSAGE);
                    dispose();
					
					uName = textField_user.getText();
                    userAccountTracker uAT = new userAccountTracker();
                    uAT.logInTrigger();
                    

                    mainFrame frame = new mainFrame();	                        
                    frame.start();
                    frame.setTitle("Violation Monitoring Dashboard");
                    frame.setVisible(true);
                    frame.displayName();
                    frame.showStats();
                } else {
                    JOptionPane.showMessageDialog(null, "Invalid credentials", "Access Denied", JOptionPane.ERROR_MESSAGE);
                }
                db.con().close();
            } catch (Exception e1) {
           }
        
		}
	}
	
}
