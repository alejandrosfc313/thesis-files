package WindowFrameClasses;
import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import DatabaseClasses.DBConnection;
import DatabaseClasses.userAccountTracker;
import LoginClassComponents.LoginFrame;
import it.sauronsoftware.junique.AlreadyLockedException;
import it.sauronsoftware.junique.JUnique;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.ImageIcon;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;
import java.awt.Color;
import java.awt.Desktop;

import javax.swing.JScrollPane;
import javax.swing.JTextField;
import java.awt.FlowLayout;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.border.LineBorder;

public class mainFrame extends JFrame {

	private JPanel contentPane;
	static mainFrame framemf = new mainFrame();
	DBConnection db = new DBConnection();
	private final SimpleDateFormat dynamicTime  = new SimpleDateFormat("hh:mm aa");
	private final SimpleDateFormat date  = new SimpleDateFormat("E, MMM dd yyyy");
    private int currentSecond;
    private Calendar calendar;
	private JLabel time;
	protected String currentDate;
	public JLabel caLenDAR;
	public JLabel lbl_displayName;
	private String dispName;
	private int totalStudents;
	private JLabel lbl_outputTotalStudent;
	private int totalViolation;
	private JLabel lbl_outputTotalViolation;
	private int totalSuspended;
	private JLabel lbl_outputTotalSuspended;
	private int statusSet;
	private JLabel lbl_outputOperatingMode;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				String appId = "myapplicationid";
				boolean alreadyRunning;
				try {
					JUnique.acquireLock(appId);
					alreadyRunning = false;
				} catch (AlreadyLockedException e) {
					alreadyRunning = true;
					JOptionPane.showMessageDialog(null, "Application is already running!");
				}
				if (!alreadyRunning) {
					// Start sequence here
					framemf.setVisible(true);
					framemf.start();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public mainFrame() {		
		setResizable(false);
		setTitle("Violation Monitoring Dashboard");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1270, 740);
		setLocationRelativeTo(null);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel_sideBtn = new JPanel();
		panel_sideBtn.setLayout(null);
		panel_sideBtn.setBackground(new Color(220, 20, 60));
		panel_sideBtn.setBounds(10, 233, 262, 383);
		contentPane.add(panel_sideBtn);
		
		JButton btn_StudentManagement = new JButton("");
		btn_StudentManagement.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				btn_StudentManagement.setIcon(new ImageIcon(mainFrame.class.getResource("/imgSource/StudentMgmtBtn_Hover.jpg")));
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btn_StudentManagement.setIcon(new ImageIcon(mainFrame.class.getResource("/imgSource/StudentMgmtBtn.jpg")));
			}
			@Override
			public void mousePressed(MouseEvent e) {
				btn_StudentManagement.setIcon(new ImageIcon(mainFrame.class.getResource("/imgSource/StudentMgmtBtn_Clicked.jpg")));
			}
			@Override
			public void mouseReleased(MouseEvent e) {
				btn_StudentManagement.setIcon(new ImageIcon(mainFrame.class.getResource("/imgSource/StudentMgmtBtn.jpg")));
			}
		});
		btn_StudentManagement.setBorder(new LineBorder(new Color(0, 0, 0)));
		btn_StudentManagement.setIcon(new ImageIcon(mainFrame.class.getResource("/imgSource/StudentMgmtBtn.jpg")));
		btn_StudentManagement.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				StudentManagementFrame frame = new StudentManagementFrame();
				frame.setVisible(true);
				
				dispose();
			}
		});
		btn_StudentManagement.setBounds(0, 0, 262, 60);
		panel_sideBtn.add(btn_StudentManagement);
		
		JButton btn_ViolationRecord = new JButton("");
		btn_ViolationRecord.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseExited(MouseEvent e) {
				btn_ViolationRecord.setIcon(new ImageIcon(mainFrame.class.getResource("/imgSource/ViolationRecordBtn.jpg")));
			}
			@Override
			public void mouseReleased(MouseEvent e) {
				btn_ViolationRecord.setIcon(new ImageIcon(mainFrame.class.getResource("/imgSource/ViolationRecordBtn.jpg")));
			}
			@Override
			public void mouseEntered(MouseEvent e) {
				btn_ViolationRecord.setIcon(new ImageIcon(mainFrame.class.getResource("/imgSource/ViolationRecordBtn_Hover.jpg")));
			}
			@Override
			public void mousePressed(MouseEvent e) {
				btn_ViolationRecord.setIcon(new ImageIcon(mainFrame.class.getResource("/imgSource/ViolationRecordBtn_Clicked.jpg")));
			}
		});
		btn_ViolationRecord.setIcon(new ImageIcon(mainFrame.class.getResource("/imgSource/ViolationRecordBtn.jpg")));
		btn_ViolationRecord.setBorder(new LineBorder(new Color(0, 0, 0)));
		btn_ViolationRecord.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ViolationRecordFrame frame = new ViolationRecordFrame();
				frame.setVisible(true);
				
				dispose();
			}
		});
		btn_ViolationRecord.setBounds(0, 142, 262, 60);
		panel_sideBtn.add(btn_ViolationRecord);
		
		JButton btn_Preferences = new JButton("");
		btn_Preferences.setBorder(new LineBorder(new Color(0, 0, 0)));
		btn_Preferences.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseExited(MouseEvent e) {
				btn_Preferences.setIcon(new ImageIcon(mainFrame.class.getResource("/imgSource/PreferencesBtn.jpg")));
			}
			@Override
			public void mouseReleased(MouseEvent e) {
				btn_Preferences.setIcon(new ImageIcon(mainFrame.class.getResource("/imgSource/PreferencesBtn.jpg")));
			}
			@Override
			public void mouseEntered(MouseEvent e) {
				btn_Preferences.setIcon(new ImageIcon(mainFrame.class.getResource("/imgSource/PreferencesBtn_Hover.jpg")));
			}
			@Override
			public void mousePressed(MouseEvent e) {
				btn_Preferences.setIcon(new ImageIcon(mainFrame.class.getResource("/imgSource/PreferencesBtn_Clicked.jpg")));
			}
		});
		btn_Preferences.setIcon(new ImageIcon(mainFrame.class.getResource("/imgSource/PreferencesBtn.jpg")));
		btn_Preferences.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				PreferencesFrame frame = new PreferencesFrame();
				frame.setVisible(true);
				frame.ShowSQLDataActivityLog();
				
				dispose();
			}
		});
		btn_Preferences.setBounds(0, 213, 262, 60);
		panel_sideBtn.add(btn_Preferences);
		
		JButton btn_SuspensionLog = new JButton("");
		btn_SuspensionLog.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseExited(MouseEvent e) {
				btn_SuspensionLog.setIcon(new ImageIcon(mainFrame.class.getResource("/imgSource/SuspendedLogBtn.jpg")));
			}
			@Override
			public void mouseReleased(MouseEvent e) {
				btn_SuspensionLog.setIcon(new ImageIcon(mainFrame.class.getResource("/imgSource/SuspendedLogBtn.jpg")));
			}
			@Override
			public void mouseEntered(MouseEvent e) {
				btn_SuspensionLog.setIcon(new ImageIcon(mainFrame.class.getResource("/imgSource/SuspendedLogBtn_Hover.jpg")));
			}
			@Override
			public void mousePressed(MouseEvent e) {
				btn_SuspensionLog.setIcon(new ImageIcon(mainFrame.class.getResource("/imgSource/SuspendedLogBtn_Clicked.jpg")));
			}
		});
		btn_SuspensionLog.setBorder(new LineBorder(new Color(0, 0, 0)));
		btn_SuspensionLog.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SuspendedStudentsFrame frame = new SuspendedStudentsFrame();
				frame.setVisible(true);
				
				dispose();
			}
		});
		btn_SuspensionLog.setIcon(new ImageIcon(mainFrame.class.getResource("/imgSource/SuspendedLogBtn.jpg")));
		btn_SuspensionLog.setBounds(0, 71, 262, 60);
		panel_sideBtn.add(btn_SuspensionLog);
		
		JPanel panel_sideBtnSmall = new JPanel();
		panel_sideBtnSmall.setBackground(new Color(220, 20, 60));
		panel_sideBtnSmall.setBounds(10, 629, 262, 60);
		contentPane.add(panel_sideBtnSmall);
		panel_sideBtnSmall.setLayout(null);
		
		JButton btn_LogOut = new JButton("");
		btn_LogOut.setToolTipText("Logout");
		btn_LogOut.setBorder(new LineBorder(new Color(0, 0, 0)));
		btn_LogOut.setBounds(187, 11, 65, 40);
		panel_sideBtnSmall.add(btn_LogOut);
		btn_LogOut.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				btn_LogOut.setIcon(new ImageIcon(mainFrame.class.getResource("/imgSource/smallLogoutBtn_Hover.jpg")));
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btn_LogOut.setIcon(new ImageIcon(mainFrame.class.getResource("/imgSource/smallLogoutBtn.jpg")));
			}
			@Override
			public void mousePressed(MouseEvent e) {
				btn_LogOut.setIcon(new ImageIcon(mainFrame.class.getResource("/imgSource/smallLogoutBtn_Clicked.jpg")));
			}
			@Override
			public void mouseReleased(MouseEvent e) {
				btn_LogOut.setIcon(new ImageIcon(mainFrame.class.getResource("/imgSource/smallLogoutBtn.jpg")));
			}
		});
		btn_LogOut.setIcon(new ImageIcon(mainFrame.class.getResource("/imgSource/smallLogoutBtn.jpg")));
		
		JButton btn_Prefs = new JButton("");
		btn_Prefs.setToolTipText("Preferences");
		btn_Prefs.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseExited(MouseEvent e) {
				btn_Prefs.setIcon(new ImageIcon(mainFrame.class.getResource("/imgSource/smallPrefBtn.jpg")));
			}
			@Override
			public void mouseReleased(MouseEvent e) {
				btn_Prefs.setIcon(new ImageIcon(mainFrame.class.getResource("/imgSource/smallPrefBtn.jpg")));
			}
			@Override
			public void mouseEntered(MouseEvent e) {
				btn_Prefs.setIcon(new ImageIcon(mainFrame.class.getResource("/imgSource/smallPrefBtn_Hover.jpg")));
			}
			@Override
			public void mousePressed(MouseEvent e) {
				btn_Prefs.setIcon(new ImageIcon(mainFrame.class.getResource("/imgSource/smallPrefBtn_Clicked.jpg")));
			}
		});
		btn_Prefs.setIcon(new ImageIcon(mainFrame.class.getResource("/imgSource/smallPrefBtn.jpg")));
		btn_Prefs.setBorder(new LineBorder(new Color(0, 0, 0)));
		btn_Prefs.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				PreferencesFrame frame = new PreferencesFrame();
				frame.setVisible(true);
				frame.ShowSQLDataActivityLog();
				
				dispose();
			}
		});
		btn_Prefs.setBounds(112, 11, 65, 40);
		panel_sideBtnSmall.add(btn_Prefs);
		
		JButton btn_Refresh = new JButton("");
		btn_Refresh.setToolTipText("Refresh");
		btn_Refresh.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				displayName();
				showStats();
			}
		});
		btn_Refresh.setBorder(new LineBorder(new Color(0, 0, 0)));
		btn_Refresh.setIcon(new ImageIcon(mainFrame.class.getResource("/imgSource/smallRefreshBtn.jpg")));
		btn_Refresh.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseExited(MouseEvent e) {
				btn_Refresh.setIcon(new ImageIcon(mainFrame.class.getResource("/imgSource/smallRefreshBtn.jpg")));
			}
			@Override
			public void mouseReleased(MouseEvent e) {
				btn_Refresh.setIcon(new ImageIcon(mainFrame.class.getResource("/imgSource/smallRefreshBtn.jpg")));
			}
			@Override
			public void mouseEntered(MouseEvent e) {
				btn_Refresh.setIcon(new ImageIcon(mainFrame.class.getResource("/imgSource/smallRefreshBtn_Hover.jpg")));
			}
			@Override
			public void mousePressed(MouseEvent e) {
				btn_Refresh.setIcon(new ImageIcon(mainFrame.class.getResource("/imgSource/smallRefreshBtn_Clicked.jpg")));
			}
		});
		btn_Refresh.setBounds(34, 11, 65, 40);
		panel_sideBtnSmall.add(btn_Refresh);
		btn_LogOut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				logoutFunction();
			}
		});
		
		JPanel panel_displayName = new JPanel();
		panel_displayName.setBackground(new Color(220, 20, 60));
		panel_displayName.setBounds(10, 122, 266, 100);
		contentPane.add(panel_displayName);
		panel_displayName.setLayout(null);
		
		JLabel lbl_Welcome = new JLabel("Welcome...");
		lbl_Welcome.setForeground(new Color(255, 255, 255));
		lbl_Welcome.setFont(new Font("Baskerville Old Face", Font.PLAIN, 20));
		lbl_Welcome.setBounds(10, 11, 93, 27);
		panel_displayName.add(lbl_Welcome);
		
		lbl_displayName = new JLabel("Root User");
		lbl_displayName.setHorizontalAlignment(SwingConstants.CENTER);
		lbl_displayName.setForeground(new Color(255, 255, 255));
		lbl_displayName.setFont(new Font("Baskerville Old Face", Font.PLAIN, 25));
		lbl_displayName.setBounds(10, 49, 246, 40);
		panel_displayName.add(lbl_displayName);
		
		JPanel panel = new JPanel();
		panel.setBounds(284, 122, 951, 477);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JPanel panel_studentMgmt = new JPanel();
		panel_studentMgmt.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				StudentManagementFrame frame = new StudentManagementFrame();
				frame.setVisible(true);
				dispose();
			}
		});
		panel_studentMgmt.setBackground(new Color(138,198,209));
		panel_studentMgmt.setBounds(30, 65, 290, 130);
		panel.add(panel_studentMgmt);
		panel_studentMgmt.setLayout(null);
		
		JLabel lbl_studentMgmt = new JLabel("Total Number of Students");
		lbl_studentMgmt.setFont(new Font("Baskerville Old Face", Font.PLAIN, 20));
		lbl_studentMgmt.setHorizontalAlignment(SwingConstants.CENTER);
		lbl_studentMgmt.setBounds(0, 95, 290, 35);
		panel_studentMgmt.add(lbl_studentMgmt);
		
		lbl_outputTotalStudent = new JLabel("0");
		lbl_outputTotalStudent.setHorizontalAlignment(SwingConstants.CENTER);
		lbl_outputTotalStudent.setFont(new Font("Baskerville Old Face", Font.PLAIN, 50));
		lbl_outputTotalStudent.setBounds(210, 24, 70, 60);
		panel_studentMgmt.add(lbl_outputTotalStudent);
		
		JPanel panel_ViolationRecord = new JPanel();
		panel_ViolationRecord.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				ViolationRecordFrame frame = new ViolationRecordFrame();
				frame.setVisible(true);
				
				dispose();
			}
		});
		panel_ViolationRecord.setLayout(null);
		panel_ViolationRecord.setBackground(new Color(255, 182, 185));
		panel_ViolationRecord.setBounds(330, 65, 290, 130);
		panel.add(panel_ViolationRecord);
		
		JLabel lbl_ViolationRecord = new JLabel("Students with Violation");
		lbl_ViolationRecord.setHorizontalAlignment(SwingConstants.CENTER);
		lbl_ViolationRecord.setFont(new Font("Baskerville Old Face", Font.PLAIN, 20));
		lbl_ViolationRecord.setBounds(0, 95, 290, 35);
		panel_ViolationRecord.add(lbl_ViolationRecord);
		
		lbl_outputTotalViolation = new JLabel("0");
		lbl_outputTotalViolation.setHorizontalAlignment(SwingConstants.CENTER);
		lbl_outputTotalViolation.setFont(new Font("Baskerville Old Face", Font.PLAIN, 50));
		lbl_outputTotalViolation.setBounds(210, 24, 70, 60);
		panel_ViolationRecord.add(lbl_outputTotalViolation);
		
		JPanel panel_ViolationPenalty = new JPanel();
		panel_ViolationPenalty.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				SuspendedStudentsFrame frame = new SuspendedStudentsFrame();
				frame.setVisible(true);
				
				dispose();
			}
		});
		panel_ViolationPenalty.setLayout(null);
		panel_ViolationPenalty.setBackground(new Color(250, 227, 217));
		panel_ViolationPenalty.setBounds(630, 65, 290, 130);
		panel.add(panel_ViolationPenalty);
		
		JLabel lbl_ViolationPenalty = new JLabel("Suspended Students");
		lbl_ViolationPenalty.setHorizontalAlignment(SwingConstants.CENTER);
		lbl_ViolationPenalty.setFont(new Font("Baskerville Old Face", Font.PLAIN, 20));
		lbl_ViolationPenalty.setBounds(0, 95, 290, 35);
		panel_ViolationPenalty.add(lbl_ViolationPenalty);
		
		lbl_outputTotalSuspended = new JLabel("0");
		lbl_outputTotalSuspended.setHorizontalAlignment(SwingConstants.CENTER);
		lbl_outputTotalSuspended.setFont(new Font("Baskerville Old Face", Font.PLAIN, 50));
		lbl_outputTotalSuspended.setBounds(210, 24, 70, 60);
		panel_ViolationPenalty.add(lbl_outputTotalSuspended);
		
		JLabel lbl_Dashboard = new JLabel("Dashboard");
		lbl_Dashboard.setFont(new Font("Baskerville Old Face", Font.PLAIN, 35));
		lbl_Dashboard.setBounds(10, 11, 153, 31);
		panel.add(lbl_Dashboard);
		
		JPanel panel_operatingMode = new JPanel();
		panel_operatingMode.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				PreferencesFrame frame = new PreferencesFrame();
				frame.setVisible(true);
				frame.settingTabs.setSelectedIndex(2);
				frame.loadStatus();
				
				dispose();
			}
		});
		panel_operatingMode.setLayout(null);
		panel_operatingMode.setBackground(new Color(187, 222, 214));
		panel_operatingMode.setBounds(30, 206, 290, 130);
		panel.add(panel_operatingMode);
		
		JLabel lbl_studentMgmt_1 = new JLabel("Object Recognition Status");
		lbl_studentMgmt_1.setHorizontalAlignment(SwingConstants.CENTER);
		lbl_studentMgmt_1.setFont(new Font("Baskerville Old Face", Font.PLAIN, 20));
		lbl_studentMgmt_1.setBounds(0, 95, 290, 35);
		panel_operatingMode.add(lbl_studentMgmt_1);
		
		lbl_outputOperatingMode = new JLabel("...");
		lbl_outputOperatingMode.setHorizontalAlignment(SwingConstants.CENTER);
		lbl_outputOperatingMode.setFont(new Font("Baskerville Old Face", Font.PLAIN, 35));
		lbl_outputOperatingMode.setBounds(10, 24, 270, 60);
		panel_operatingMode.add(lbl_outputOperatingMode);
		
		caLenDAR = new JLabel("New label");
		caLenDAR.setForeground(Color.BLACK);
		caLenDAR.setHorizontalAlignment(SwingConstants.CENTER);
		caLenDAR.setFont(new Font("Baskerville Old Face", Font.PLAIN, 20));
		caLenDAR.setBounds(282, 660, 953, 21);
		contentPane.add(caLenDAR);
		
		time = new JLabel("HH:MM:SS");
		time.setForeground(Color.BLACK);
		time.setFont(new Font("Baskerville Old Face", Font.PLAIN, 40));
		time.setHorizontalAlignment(SwingConstants.CENTER);
		time.setBounds(282, 621, 953, 40);
		contentPane.add(time);
		
		JLabel Background = new JLabel("");
		Background.setOpaque(true);
		Background.setIcon(new ImageIcon(mainFrame.class.getResource("/imgSource/mainFrameBG.jpg")));
		Background.setBounds(0, 0, 1254, 702);
		contentPane.add(Background);
	}
	
	private void reset(){
        calendar = Calendar.getInstance();
        currentSecond = calendar.get(Calendar.SECOND);
    }
	
	public void start(){
        reset();
        Timer timer = new Timer();
        timer.scheduleAtFixedRate( new TimerTask(){

			public void run(){
                if( currentSecond == 60 ) {
                    reset();
                }
                time.setText(String.format("%s", dynamicTime.format(calendar.getTime()), currentSecond ));
                caLenDAR.setText(String.format("%s", date.format(calendar.getTime()), currentSecond ));
                currentSecond++;
            }
        }, 0, 1000 );
    }
	
	public void logoutFunction() {

		//Logout Confirmation
		int response = JOptionPane.showConfirmDialog(null, "Are you sure you do you want to log out?", "Logout", JOptionPane.YES_OPTION, JOptionPane.NO_OPTION);
			if(response == JOptionPane.YES_OPTION) {
					LoginFrame lg = new LoginFrame();
					userAccountTracker uAT = new userAccountTracker();
					
					uAT.logOutTrigger();
					lg.setVisible(true);
					dispose();
		}
	}
	
	public void displayName() {
        try {
        	String query = "SELECT name from studentinfo.credentialsdb WHERE (activeStatus = 1)";
            PreparedStatement pst = db.con().prepareStatement(query);
            ResultSet rs = pst.executeQuery();

            if (rs.next()) {
                dispName = rs.getString(1);

                lbl_displayName.setText(dispName);
            }            
            db.con().close();
        } catch (Exception e1) {
       }
	}
	
	public void showStats() {
		//Load Total Number of Students
		try {
            String query = "SELECT count(*) FROM studentinfo.studentdb;";
            PreparedStatement pst = db.con().prepareStatement(query);
            ResultSet rs = pst.executeQuery();

            if (rs.next()) {
                totalStudents = rs.getInt(1);

                lbl_outputTotalStudent.setText(Integer.toString(totalStudents));
            }            
            db.con().close();
        } catch (Exception e1) {
       }
		
		//Load Total Students with Violation
		try {
            String query = "SELECT count(*) FROM studentinfo.studentdb WHERE `Violation Count` > 0;";
            PreparedStatement pst = db.con().prepareStatement(query);
            ResultSet rs = pst.executeQuery();

            if (rs.next()) {
                totalViolation = rs.getInt(1);

                lbl_outputTotalViolation.setText(Integer.toString(totalViolation));
            }            
            db.con().close();
        } catch (Exception e1) {
       }
		
		//Load Students with Suspension
		try {
            String query = "SELECT count(*) FROM studentinfo.studentdb WHERE `Violation Count`>=3;";
            PreparedStatement pst = db.con().prepareStatement(query);
            ResultSet rs = pst.executeQuery();

            if (rs.next()) {
                totalSuspended = rs.getInt(1);

                lbl_outputTotalSuspended.setText(Integer.toString(totalSuspended));
            }            
            db.con().close();
        } catch (Exception e1) {
       }
		
		//Load Object Recognition Mode
		try {
            String query = "SELECT * from studentinfo.status_set";
            PreparedStatement pst = db.con().prepareStatement(query);
            ResultSet rs = pst.executeQuery();
            
            if (rs.next()) {
                statusSet = rs.getInt(1);
                if(statusSet == 1) {
                	lbl_outputOperatingMode.setText("Detection Mode");
                }
                if(statusSet == 0) {
                	lbl_outputOperatingMode.setText("Idle Mode");
                }
                if(statusSet == 2) {
                	lbl_outputOperatingMode.setText("Camera Error");
                }
            }
            db.con().close();
        } catch (Exception e) {
            // TODO: handle exception
        }
	}
	
	public void loadDocu() {
			try {  
				//constructor of file class having file as argument  
				File file = new File("docs/Documentation.pdf");   
				
				if(!Desktop.isDesktopSupported()) {//check if Desktop is supported by Platform or not  
					System.out.println("not supported");  
					return;  
				}  
				
				Desktop desktop = Desktop.getDesktop();  
				if(file.exists())         //checks file exists or not  
					desktop.open(file);              //opens the specified file  
			}catch(Exception e)  {  
				e.printStackTrace();  
		}  
	}
}
