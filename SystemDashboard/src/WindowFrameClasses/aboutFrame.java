package WindowFrameClasses;

import java.awt.BorderLayout;
import java.awt.ComponentOrientation;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JTextArea;
import javax.swing.JTextPane;

public class aboutFrame extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					aboutFrame frame = new aboutFrame();
					frame.setLocationRelativeTo(null);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public aboutFrame() {
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 630, 450);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon(aboutFrame.class.getResource("/imgSource/BackgroundImg.jpg")));
		lblNewLabel.setBounds(10, 11, 300, 389);
		contentPane.add(lblNewLabel);
		
		JLabel lblTitle1 = new JLabel("School Uniform Monitoring System");
		lblTitle1.setHorizontalAlignment(SwingConstants.CENTER);
		lblTitle1.setFont(new Font("Baskerville Old Face", Font.PLAIN, 15));
		lblTitle1.setBounds(320, 11, 284, 25);
		contentPane.add(lblTitle1);
		
		JLabel lblViolationMonitoringSystem = new JLabel("Violation Monitoring System");
		lblViolationMonitoringSystem.setHorizontalAlignment(SwingConstants.CENTER);
		lblViolationMonitoringSystem.setFont(new Font("Baskerville Old Face", Font.PLAIN, 15));
		lblViolationMonitoringSystem.setBounds(320, 160, 284, 25);
		contentPane.add(lblViolationMonitoringSystem);
		
		JLabel lblDevelopedBy = new JLabel("Developed by:");
		lblDevelopedBy.setFont(new Font("Baskerville Old Face", Font.PLAIN, 15));
		lblDevelopedBy.setBounds(320, 300, 284, 25);
		contentPane.add(lblDevelopedBy);
		
		JTextArea developerlist = new JTextArea();
		developerlist.setFont(new Font("Baskerville Old Face", Font.PLAIN, 16));
		developerlist.setOpaque(false);
		developerlist.setEditable(false);
		developerlist.setText("Alejandro, John Michael C.\r\nArabaca, Henry III D.\r\nOliveros, John Alfren\r\nMurillo, Mary Amvihanahbelle M.");
		developerlist.setBounds(330, 332, 274, 68);
		contentPane.add(developerlist);
		
		JTextArea schoolUnifDescript = new JTextArea();
		schoolUnifDescript.setOpaque(false);
		schoolUnifDescript.setEditable(false);
		schoolUnifDescript.setFont(new Font("Baskerville Old Face", Font.PLAIN, 14));
		schoolUnifDescript.setText("School Uniform Monitoring System is a software \r\nmade by the students from Computer Engineering \r\nin Emilio Aguinaldo College. School Uniform \r\nMonitoring System is developed to promote the \r\nprescribed school uniform to students.");
		schoolUnifDescript.setBounds(320, 47, 284, 83);
		contentPane.add(schoolUnifDescript);
		
		JTextArea txtrViolationMonitoringDashboard = new JTextArea();
		txtrViolationMonitoringDashboard.setText("Violation Monitoring Dashboard is developed to \r\nmonitor the student violation records utilizing \r\nprogramming languages such as Java and MySQL. \r\nThis will serve as a digital violation log book data \r\nstorage. ");
		txtrViolationMonitoringDashboard.setOpaque(false);
		txtrViolationMonitoringDashboard.setFont(new Font("Baskerville Old Face", Font.PLAIN, 14));
		txtrViolationMonitoringDashboard.setEditable(false);
		txtrViolationMonitoringDashboard.setBounds(320, 196, 284, 83);
		contentPane.add(txtrViolationMonitoringDashboard);
		setTitle("About");
	}
}
