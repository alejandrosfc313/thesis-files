package WindowFrameClasses;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.Desktop;  
import java.io.*;  
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileNameExtensionFilter;

import com.itextpdf.text.Document;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import DatabaseClasses.DBConnection;
import DatabaseClasses.userAccountTracker;
import LoginClassComponents.splashScreen;
import it.sauronsoftware.junique.AlreadyLockedException;
import it.sauronsoftware.junique.JUnique;
import net.proteanit.sql.DbUtils;
import javax.swing.border.LineBorder;

public class SuspendedStudentsFrame extends JFrame {

	private JPanel contentPane;
	private JTextField textField_Sn;
	private JTable table_SuspendedStudents;
	private JScrollPane scrollPane;
	static SuspendedStudentsFrame framevr = new SuspendedStudentsFrame();
	
	DBConnection db = new DBConnection();
	private JButton btn_back;
	private JLabel lbl_outputProgram;
	private JLabel lbl_name;
	private JLabel lbl_program;
	private JLabel lbl_outputName;
	private JMenuItem mainFrameItem;
	private JMenuItem stdmgmtItem;
	private JMenuItem prefItem;
	private JButton btn_search;
	private JLabel lbl_outputstdNum;
	private JLabel lbl_vCount;
	private JLabel lbl_outputvCount;
	private JLabel stdLBL;
	private JMenuItem RestartItem;
	private JButton btn_clearViolation;
	public static String clearedSuspensionName;
	private String path;
	private JLabel lbl_SuspensionLogList;
	private JMenuItem violationRecordItem;
	private JFileChooser fileChooser;
	private JMenu helpMenu;
	private JMenuItem tutorialItem;
	private JMenuItem aboutItem;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				String appId = "myapplicationid";
				boolean alreadyRunning;
				try {
					JUnique.acquireLock(appId);
					alreadyRunning = false;
				} catch (AlreadyLockedException e) {
					alreadyRunning = true;
					JOptionPane.showMessageDialog(null, "Application is already running!");
				}
				if (!alreadyRunning) {
					// Start sequence here
					framevr.setVisible(true);
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public SuspendedStudentsFrame() {
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowOpened(WindowEvent e) {
				ShowSQLData();
			}
		});
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("Suspension Logs");
		setBounds(100, 100, 1270, 740);
		setLocationRelativeTo(null);
		setResizable(false);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		fileChooser = new JFileChooser();
		fileChooser.setFileFilter(new FileNameExtensionFilter("*.pdf", "pdf"));
		
		JButton btn_showAll = new JButton("SHOW ALL");
		btn_showAll.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btn_clearViolation.setEnabled(false);
				
				ShowSQLData();
				clearDisplay();
			}
		});
		
		btn_clearViolation = new JButton("");
		btn_clearViolation.setIcon(new ImageIcon(SuspendedStudentsFrame.class.getResource("/imgSource/clrBtn.png")));
		btn_clearViolation.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				btn_clearViolation.setIcon(new ImageIcon(SuspendedStudentsFrame.class.getResource("/imgSource/clrBtn.png")));
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btn_clearViolation.setIcon(new ImageIcon(SuspendedStudentsFrame.class.getResource("/imgSource/clrBtn.png")));
			}
			@Override
			public void mouseEntered(MouseEvent e) {
				btn_clearViolation.setIcon(new ImageIcon(SuspendedStudentsFrame.class.getResource("/imgSource/clrBtn_hover.png")));
			}
			@Override
			public void mousePressed(MouseEvent e) {
				btn_clearViolation.setIcon(new ImageIcon(SuspendedStudentsFrame.class.getResource("/imgSource/clrBtn_clicked.png")));
			}
		});
		btn_clearViolation.setBorder(new LineBorder(new Color(0, 0, 0)));
		btn_clearViolation.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int response = JOptionPane.showConfirmDialog(null, "Are you sure you want to clear the suspension record of "+ lbl_outputName.getText() +"? This cannot be undone.", "Warning!", JOptionPane.YES_OPTION, JOptionPane.NO_OPTION);
				if(response == JOptionPane.YES_OPTION) {
					ClearViolation();
					JOptionPane.showMessageDialog(null, "Data Cleared!");
					clearDisplay();
					btn_clearViolation.setEnabled(false);
				}
			}
		});
		
		lbl_SuspensionLogList = new JLabel("SUSPENDED STUDENTS");
		lbl_SuspensionLogList.setHorizontalAlignment(SwingConstants.CENTER);
		lbl_SuspensionLogList.setFont(new Font("Baskerville Old Face", Font.BOLD, 28));
		lbl_SuspensionLogList.setBounds(463, 58, 757, 34);
		contentPane.add(lbl_SuspensionLogList);
		btn_clearViolation.setEnabled(false);
		btn_clearViolation.setFont(new Font("Baskerville Old Face", Font.BOLD, 16));
		btn_clearViolation.setBounds(174, 498, 150, 34);
		contentPane.add(btn_clearViolation);
		
		JSeparator lineSeparator = new JSeparator();
		lineSeparator.setOrientation(SwingConstants.VERTICAL);
		lineSeparator.setForeground(Color.LIGHT_GRAY);
		lineSeparator.setBackground(Color.LIGHT_GRAY);
		lineSeparator.setBounds(447, 44, 4, 629);
		contentPane.add(lineSeparator);
		btn_showAll.setFont(new Font("Baskerville Old Face", Font.PLAIN, 16));
		btn_showAll.setBounds(158, 171, 120, 34);
		contentPane.add(btn_showAll);
		
		stdLBL = new JLabel("STUDENT INFORMATION");
		stdLBL.setHorizontalAlignment(SwingConstants.CENTER);
		stdLBL.setFont(new Font("Baskerville Old Face", Font.BOLD, 18));
		stdLBL.setBounds(10, 242, 423, 34);
		contentPane.add(stdLBL);
		
		lbl_outputvCount = new JLabel("");
		lbl_outputvCount.setOpaque(true);
		lbl_outputvCount.setHorizontalAlignment(SwingConstants.CENTER);
		lbl_outputvCount.setBackground(new Color(165, 42, 42));
		lbl_outputvCount.setForeground(Color.WHITE);
		lbl_outputvCount.setFont(new Font("Baskerville Old Face", Font.PLAIN, 18));
		lbl_outputvCount.setBounds(174, 438, 259, 34);
		contentPane.add(lbl_outputvCount);
		
		lbl_vCount = new JLabel("VIOLATION COUNT");
		lbl_vCount.setFont(new Font("Baskerville Old Face", Font.PLAIN, 14));
		lbl_vCount.setBounds(20, 438, 137, 34);
		contentPane.add(lbl_vCount);
		
		lbl_outputstdNum = new JLabel("");
		lbl_outputstdNum.setOpaque(true);
		lbl_outputstdNum.setHorizontalAlignment(SwingConstants.CENTER);
		lbl_outputstdNum.setBackground(new Color(165, 42, 42));
		lbl_outputstdNum.setForeground(Color.WHITE);
		lbl_outputstdNum.setFont(new Font("Baskerville Old Face", Font.PLAIN, 18));
		lbl_outputstdNum.setBounds(174, 303, 259, 34);
		contentPane.add(lbl_outputstdNum);
		
		JLabel lbl_stdNum = new JLabel("STUDENT NUMBER");
		lbl_stdNum.setFont(new Font("Baskerville Old Face", Font.PLAIN, 14));
		lbl_stdNum.setBounds(23, 303, 134, 34);
		contentPane.add(lbl_stdNum);
		
		JLabel lbl_Sn = new JLabel("Student No.");
		lbl_Sn.setFont(new Font("Baskerville Old Face", Font.PLAIN, 17));
		lbl_Sn.setBounds(45, 126, 82, 34);
		contentPane.add(lbl_Sn);
		
		textField_Sn = new JTextField();
		textField_Sn.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_ENTER) {
					btn_search.doClick();
				}
			}
		});
		textField_Sn.setBounds(137, 126, 255, 34);
		contentPane.add(textField_Sn);
		textField_Sn.setColumns(10);
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(464, 103, 756, 506);
		contentPane.add(scrollPane);
		
		table_SuspendedStudents = new JTable();
		table_SuspendedStudents.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table_SuspendedStudents.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_DOWN || e.getKeyCode() == KeyEvent.VK_UP) {
					dataTableLoader();
					btn_clearViolation.setEnabled(true);
				}
				else {
					//Do Nothing
				}
			}
		});
		table_SuspendedStudents.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				dataTableLoader();
				btn_clearViolation.setEnabled(true);
			}
		});
		table_SuspendedStudents.setDefaultEditor(Object.class, null);
		scrollPane.setViewportView(table_SuspendedStudents);
		
		JLabel lbl_searchField = new JLabel("SEARCH");
		lbl_searchField.setHorizontalAlignment(SwingConstants.CENTER);
		lbl_searchField.setFont(new Font("Baskerville Old Face", Font.BOLD, 28));
		lbl_searchField.setBounds(20, 58, 127, 34);
		contentPane.add(lbl_searchField);
		
		btn_back = new JButton("");
		btn_back.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				btn_back.setIcon(new ImageIcon(StudentManagementFrame.class.getResource("/imgSource/btnBack.png")));
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btn_back.setIcon(new ImageIcon(StudentManagementFrame.class.getResource("/imgSource/btnBack.png")));
			}
			@Override
			public void mouseEntered(MouseEvent e) {
				btn_back.setIcon(new ImageIcon(StudentManagementFrame.class.getResource("/imgSource/btnBack_hover.png")));
			}
			@Override
			public void mousePressed(MouseEvent e) {
				btn_back.setIcon(new ImageIcon(StudentManagementFrame.class.getResource("/imgSource/btnBack_clicked.png")));
			}
		});
		btn_back.setBorder(new LineBorder(new Color(0, 0, 0)));
		btn_back.setIcon(new ImageIcon(SuspendedStudentsFrame.class.getResource("/imgSource/btnBack.png")));
		btn_back.setFont(new Font("Baskerville Old Face", Font.PLAIN, 16));
		btn_back.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
				mainFrame frame = new mainFrame();
                frame.start();
                frame.setVisible(true);
				frame.showStats();
				frame.displayName();
			}
		});
		btn_back.setBounds(1139, 650, 105, 40);
		contentPane.add(btn_back);
		
		btn_search = new JButton("SEARCH");
		btn_search.setFont(new Font("Baskerville Old Face", Font.PLAIN, 16));
		btn_search.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String StudentNum = textField_Sn.getText();
				
				if(StudentNum.trim().length() == 0) {
					JOptionPane.showMessageDialog(null, "Search Bar cannot be empty!", "Error", JOptionPane.ERROR_MESSAGE);
				}
				else {
				searchSQLData();
				}
			}	
		
		});
		btn_search.setBounds(288, 171, 104, 34);
		contentPane.add(btn_search);
		
		lbl_name = new JLabel("NAME");
		lbl_name.setFont(new Font("Baskerville Old Face", Font.PLAIN, 14));
		lbl_name.setBounds(115, 348, 42, 34);
		contentPane.add(lbl_name);
		
		lbl_program = new JLabel("PROGRAM");
		lbl_program.setFont(new Font("Baskerville Old Face", Font.PLAIN, 14));
		lbl_program.setBounds(87, 393, 70, 34);
		contentPane.add(lbl_program);
		
		lbl_outputName = new JLabel("");
		lbl_outputName.setOpaque(true);
		lbl_outputName.setHorizontalAlignment(SwingConstants.CENTER);
		lbl_outputName.setBackground(new Color(165, 42, 42));
		lbl_outputName.setForeground(Color.WHITE);
		lbl_outputName.setFont(new Font("Baskerville Old Face", Font.PLAIN, 18));
		lbl_outputName.setBounds(174, 348, 259, 34);
		contentPane.add(lbl_outputName);
		
		lbl_outputProgram = new JLabel("");
		lbl_outputProgram.setOpaque(true);
		lbl_outputProgram.setHorizontalAlignment(SwingConstants.CENTER);
		lbl_outputProgram.setBackground(new Color(165, 42, 42));
		lbl_outputProgram.setForeground(Color.WHITE);
		lbl_outputProgram.setFont(new Font("Baskerville Old Face", Font.PLAIN, 18));
		lbl_outputProgram.setBounds(174, 393, 259, 34);
		contentPane.add(lbl_outputProgram);
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.setBounds(0, 0, 1254, 22);
		contentPane.add(menuBar);
		
		JMenu fileMenu = new JMenu("File");
		menuBar.add(fileMenu);
		
		JMenuItem logoutItem = new JMenuItem("Logout");
		logoutItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mainFrame frame = new mainFrame();
				frame.logoutFunction();
				dispose();
			}
		});
		
		JMenuItem generateReportItem = new JMenuItem("Generate Report");
		generateReportItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (fileChooser.showSaveDialog(SuspendedStudentsFrame.this) == JFileChooser.APPROVE_OPTION) {
					exportSuspendedStdData();
				}
			}
		});
		fileMenu.add(generateReportItem);
		fileMenu.add(logoutItem);
		
		JMenuItem exitItem = new JMenuItem("Exit");
		exitItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		
		RestartItem = new JMenuItem("Restart");
		RestartItem.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				dispose();
				
				splashScreen frame = new splashScreen();
				frame.setLocationRelativeTo(null);
				frame.setVisible(true);
			}
		});
		fileMenu.add(RestartItem);
		fileMenu.add(exitItem);
		
		JMenu editMenu = new JMenu("Edit");
		menuBar.add(editMenu);
		
		JMenuItem refreshtableItem = new JMenuItem("Refresh");
		refreshtableItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ShowSQLData();
			}
		});
		editMenu.add(refreshtableItem);
		
		JMenu navMenu = new JMenu("Navigate");
		menuBar.add(navMenu);
		
		mainFrameItem = new JMenuItem("Home");
		mainFrameItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
				mainFrame frame = new mainFrame();
				frame.start();
				frame.setVisible(true);
				frame.showStats();
				frame.displayName();
			}
		});
		navMenu.add(mainFrameItem);
		
		stdmgmtItem = new JMenuItem("Student Management");
		stdmgmtItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
				StudentManagementFrame frame = new StudentManagementFrame();
				frame.setVisible(true);
			}
		});
		
		prefItem = new JMenuItem("Preferences");
		prefItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
				PreferencesFrame frame = new PreferencesFrame();
				frame.setVisible(true);
				frame.ShowSQLDataActivityLog();
				
			}
		});
		navMenu.add(prefItem);
		navMenu.add(stdmgmtItem);
		
		violationRecordItem = new JMenuItem("Violation Record");
		violationRecordItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ViolationRecordFrame frame = new ViolationRecordFrame();
				frame.setVisible(true);
				
				dispose();
			}
		});
		navMenu.add(violationRecordItem);
		
		helpMenu = new JMenu("Help");
		menuBar.add(helpMenu);
		
		tutorialItem = new JMenuItem("Tutorial");
		tutorialItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mainFrame frame = new mainFrame();
				frame.loadDocu();
			}
		});
		helpMenu.add(tutorialItem);
		
		aboutItem = new JMenuItem("About");
		aboutItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				aboutFrame frame = new aboutFrame();
				frame.setLocationRelativeTo(null);
				frame.setVisible(true);
			}
		});
		helpMenu.add(aboutItem);
		
		JLabel Background = new JLabel("");
		Background.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				btn_clearViolation.setEnabled(false);
				table_SuspendedStudents.clearSelection();
			}
		});
		Background.setIcon(new ImageIcon(ViolationRecordFrame.class.getResource("/imgSource/Background.jpg")));
		Background.setBounds(0, 0, 1254, 701);
		contentPane.add(Background);
	}
	
	public void clearDisplay() {
		textField_Sn.setText("");
		lbl_outputstdNum.setText("");
		lbl_outputName.setText("");
		lbl_outputProgram.setText("");
		lbl_outputvCount.setText("");
	}
	
	private void ShowSQLData() {
		try {
			String query = "SELECT `Student Number`, `Last Name`, `First Name`, `Last Name`, `Suffix`, `Middle Initial`, `Program` FROM studentinfo.studentdb WHERE `Violation Count` >= '3';";
			PreparedStatement pst = db.con().prepareStatement(query);
			ResultSet rs = pst.executeQuery(query);
			table_SuspendedStudents.setModel(DbUtils.resultSetToTableModel(rs));
			
			db.con().close();
		} catch (Exception e1) {
		}
	}
	
	
	public void dataTableLoader() {
		int row = table_SuspendedStudents.getSelectedRow();
		
	//get the table row value
		String stdSelect = (table_SuspendedStudents.getModel().getValueAt(row, 0).toString());
		String query = "SELECT * FROM studentinfo.studentdb WHERE `Student Number` = '"+ stdSelect +"';";			//Students DB Table
		try {
			PreparedStatement pst = db.con().prepareStatement(query);
			ResultSet rs = pst.executeQuery(query);
			
			if(rs.next()) {
				String stdno = rs.getString(2);
				String lname = rs.getString(3);
				String fname = rs.getString(4);
				String suffix = rs.getString(5);
				String mname = rs.getString(6);
				String pG = rs.getString(7);
				String vCount = rs.getString(8);
				
				
				if(suffix.equals("")) {//If no suffix but have middle initial
					lbl_outputstdNum.setText(stdno);
					lbl_outputName.setText(fname + " " + mname + ". " + lname);
					lbl_outputProgram.setText(pG);
					lbl_outputvCount.setText(vCount);
				}
				if(mname.equals("")) {//If no middle Initial and suffix
					lbl_outputstdNum.setText(stdno);
					lbl_outputName.setText(fname + " " + lname);
					lbl_outputProgram.setText(pG);
					lbl_outputvCount.setText(vCount);
				}
				else{//Complete Name
					lbl_outputstdNum.setText(stdno);
					lbl_outputName.setText(fname + " " + mname + ". " + lname + " " + suffix);
					lbl_outputProgram.setText(pG);
					lbl_outputvCount.setText(vCount);
				}
			}
			db.con().close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void searchSQLData() {
		String inputStnNo = textField_Sn.getText();
		try {
			String query = "SELECT `Student Number`, `Last Name`, `First Name`, `Last Name`, `Suffix`, `Middle Initial`, `Program` FROM studentinfo.studentdb WHERE `Violation Count` >= '3' and `Student Number` = '"+inputStnNo+"';";
			String query2 = "SELECT * FROM studentinfo.studentdb WHERE `Violation Count` >= '3' AND `Student Number` = '"+inputStnNo+"';";
			
			
			PreparedStatement pst = db.con().prepareStatement(query);
			PreparedStatement pst2 = db.con().prepareStatement(query2);
			ResultSet rs = pst.executeQuery(query);
			ResultSet rs2 = pst2.executeQuery(query2);
			table_SuspendedStudents.setModel(DbUtils.resultSetToTableModel(rs));
			
			if(rs2.next()) {
				String stdno = rs2.getString(2);
				String lname = rs2.getString(3);
				String fname = rs2.getString(4);
				String suffix = rs2.getString(5);
				String mname = rs2.getString(6);
				String pG = rs2.getString(7);
				String vCount = rs2.getString(8);
				
				if(suffix.equals("")) {//If no suffix but have middle initial
					lbl_outputstdNum.setText(stdno);
					lbl_outputName.setText(fname + " " + mname + ". " + lname);
					lbl_outputProgram.setText(pG);
					lbl_outputvCount.setText(vCount);
				}
				if(mname.equals("")) {//If no middle Initial and suffix
					lbl_outputstdNum.setText(stdno);
					lbl_outputName.setText(fname + " " + lname);
					lbl_outputProgram.setText(pG);
					lbl_outputvCount.setText(vCount);
				}
				else{//Complete Name
					lbl_outputstdNum.setText(stdno);
					lbl_outputName.setText(fname + " " + mname + ". " + lname + " " + suffix);
					lbl_outputProgram.setText(pG);
					lbl_outputvCount.setText(vCount);
				}
			}
			else {
				JOptionPane.showMessageDialog(null, "No data found", "Attention", JOptionPane.INFORMATION_MESSAGE);
			}
			db.con().close();
		} catch (Exception e1) {
		}
	}

	private void OrganizeAutoIncrement() {
		try {
			String query = "ALTER TABLE studentinfo.violationdb DROP COLUMN `#`;";
			PreparedStatement pst = db.con().prepareStatement(query);
			pst.execute();
			
			db.con().close();
		}catch (Exception e){
			JOptionPane.showMessageDialog(null, "Error: " + e);
		}
	
		try {
			String query = "ALTER TABLE studentinfo.violationdb ADD `#` INT PRIMARY KEY NOT NULL AUTO_INCREMENT FIRST;";
			PreparedStatement pst = db.con().prepareStatement(query);
			pst.execute();
			
			db.con().close();
		}catch (Exception e){
			JOptionPane.showMessageDialog(null, "Error: " + e);
		}
	
		try {
			String query = "SELECT * FROM studentinfo.violationdb;";
			PreparedStatement pst = db.con().prepareStatement(query);
			pst.execute();
			
			db.con().close();
			ShowSQLData();
		}catch (Exception e){
			JOptionPane.showMessageDialog(null, "Error: " + e);
		}
	}
	
	public void ClearViolation() {
		userAccountTracker uAT = new userAccountTracker();
		
		int row = table_SuspendedStudents.getSelectedRow();
		String id = (table_SuspendedStudents.getModel().getValueAt(row, 0).toString());
		try {			
			String query = "DELETE FROM `studentinfo`.`violationdb` WHERE (`Student Number` = '"+ id +"');";
			PreparedStatement pst = db.con().prepareStatement(query);
			pst.execute();
			
			db.con().close();
			OrganizeAutoIncrement();
			ShowSQLData();
		}catch (Exception e2){
		}
			
		try {
			String query = "UPDATE `studentinfo`.`studentdb` SET `Violation Count` = '0' WHERE (`Student Number` = '"+id+"');";
			PreparedStatement pst2 = db.con().prepareStatement(query);
			pst2.execute();
			
			db.con().close();
			ShowSQLData();
		}catch (SQLException e1) {
		}
		
		clearedSuspensionName = lbl_outputName.getText();
		uAT.suspensionClearing();
	}
	
	public void exportSuspendedStdData() {		
		try {
		Document generatedFile = new Document();
		PdfWriter.getInstance(generatedFile, new FileOutputStream(fileChooser.getSelectedFile().toString()+".pdf"));
		
		generatedFile.open();
		
		//Header Components
		Paragraph header = new Paragraph("School Uniform Monitoring System");
		header.setAlignment(1);
		
		Paragraph subheader = new Paragraph("Suspended Students");
		subheader.setAlignment(1);
		
		generatedFile.add(header);
		generatedFile.add(subheader);
		generatedFile.add(new Paragraph(" "));
		//
		
		//Table Components - Columns
		PdfPTable table = new PdfPTable(4);
		PdfPCell tableColumns = new PdfPCell(new Phrase("Student Number"));
		table.addCell(tableColumns);
		
		tableColumns = new PdfPCell(new Phrase("Name"));
		table.addCell(tableColumns);
		
		tableColumns = new PdfPCell(new Phrase("Program"));
		table.addCell(tableColumns);
		
		tableColumns = new PdfPCell(new Phrase("Violation Count"));
		table.addCell(tableColumns);
		table.setHeaderRows(1);
		
		//Database Connection
		String query = "SELECT `Student Number`, `Last Name`, `First Name`, `Suffix`, `Middle Initial`, `Program`, `Violation Count` from studentinfo.studentdb WHERE `Violation Count`>=3;";
		PreparedStatement pst = db.con().prepareStatement(query);
		ResultSet rs = pst.executeQuery();
		
		while(rs.next()) {
			String stdNum = rs.getString(1);
			String lName = rs.getString(2);
			String fName = rs.getString(3);
			String sFix = rs.getString(4);
			String mInitial = rs.getString(5);
			String program = rs.getString(6);
			String vCount = rs.getString(7);
			
			String fullName = (lName +", "+fName+" "+sFix+" "+mInitial+". ");
		

			table.addCell(stdNum);
			table.addCell(fullName);
			table.addCell(program);
			table.addCell(vCount);
		}	
		
		//Add Table to File
		generatedFile.add(table);
		
		//Close Connection and FileWriter
		db.con().close();
		generatedFile.close();
		
		JOptionPane.showMessageDialog(null, "Report File Generated", "Completed", JOptionPane.INFORMATION_MESSAGE);
		}
		catch (Exception e) {
			System.out.println(e);
		}
	}
}