package WindowFrameClasses;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.Desktop;  
import java.io.*;  
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileNameExtensionFilter;

import com.itextpdf.text.Document;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import DatabaseClasses.DBConnection;
import DatabaseClasses.userAccountTracker;
import LoginClassComponents.LoginFrame;
import LoginClassComponents.splashScreen;
import it.sauronsoftware.junique.AlreadyLockedException;
import it.sauronsoftware.junique.JUnique;
import net.proteanit.sql.DbUtils;
import javax.swing.border.LineBorder;

public class ViolationRecordFrame extends JFrame {

	private JPanel contentPane;
	private JTextField textField_Sn;
	private JTable table_ViolationRecord;
	private JScrollPane scrollPane;
	static ViolationRecordFrame framevr = new ViolationRecordFrame();
	
	DBConnection db = new DBConnection();
	private JButton btn_back;
	private JLabel lbl_outputProgram;
	private JLabel lbl_name;
	private JLabel lbl_program;
	private JLabel lbl_outputName;
	private JMenuItem mainFrameItem;
	private JMenuItem stdmgmtItem;
	private JMenuItem prefItem;
	private JButton btn_search;
	private JLabel lbl_outputstdNum;
	private JLabel lbl_vCount;
	private JLabel lbl_outputvCount;
	private JLabel stdLBL;
	private JMenuItem RestartItem;
	private JButton btn_deleteViolation;
	private JButton btn_clearViolation;
	private JButton btn_showEvidence;
	public static String deletedViolationName;
	public static String clearedViolationName;
	private String path;
	private JMenuItem suspensionLogItem;
	private JFileChooser fileChooser;
	private JMenuItem generateReportItem;
	private JMenu helpMenu;
	private JMenuItem tutorialItem;
	private JMenuItem aboutItem;
	private JButton btn_showAll;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				String appId = "myapplicationid";
				boolean alreadyRunning;
				try {
					JUnique.acquireLock(appId);
					alreadyRunning = false;
				} catch (AlreadyLockedException e) {
					alreadyRunning = true;
					JOptionPane.showMessageDialog(null, "Application is already running!");
				}
				if (!alreadyRunning) {
					// Start sequence here
					framevr.setVisible(true);
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ViolationRecordFrame() {
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowOpened(WindowEvent e) {
				ShowSQLData();
			}
		});
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("Violation Record");
		setBounds(100, 100, 1270, 740);
		setLocationRelativeTo(null);
		setResizable(false);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		fileChooser = new JFileChooser();
		fileChooser.setFileFilter(new FileNameExtensionFilter("*.pdf", "pdf"));
		
		btn_showAll = new JButton("");
		btn_showAll.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				btn_showAll.setIcon(new ImageIcon(ViolationRecordFrame.class.getResource("/imgSource/ShowAllBtn.png")));
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btn_showAll.setIcon(new ImageIcon(ViolationRecordFrame.class.getResource("/imgSource/ShowAllBtn.png")));
			}
			@Override
			public void mouseEntered(MouseEvent e) {
				btn_showAll.setIcon(new ImageIcon(ViolationRecordFrame.class.getResource("/imgSource/ShowAllBtn_hover.png")));
			}
			@Override
			public void mousePressed(MouseEvent e) {
				btn_showAll.setIcon(new ImageIcon(ViolationRecordFrame.class.getResource("/imgSource/ShowAllBtn_clicked.png")));
			}
		});
		btn_showAll.setEnabled(false);
		btn_showAll.setIcon(new ImageIcon(ViolationRecordFrame.class.getResource("/imgSource/ShowAllBtn.png")));
		btn_showAll.setBorder(new LineBorder(new Color(0, 0, 0)));
		btn_showAll.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btn_showEvidence.setEnabled(false);
				btn_clearViolation.setEnabled(false);
				btn_deleteViolation.setEnabled(false);
				
				ShowSQLData();
				clearDisplay();
				btn_showAll.setEnabled(false);
			}
		});
		
		btn_showEvidence = new JButton("");
		btn_showEvidence.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				btn_showEvidence.setIcon(new ImageIcon(ViolationRecordFrame.class.getResource("/imgSource/VImageBtn.png")));
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btn_showEvidence.setIcon(new ImageIcon(ViolationRecordFrame.class.getResource("/imgSource/VImageBtn.png")));
			}
			@Override
			public void mouseEntered(MouseEvent e) {
				btn_showEvidence.setIcon(new ImageIcon(ViolationRecordFrame.class.getResource("/imgSource/VImageBtn_hover.png")));
			}
			@Override
			public void mousePressed(MouseEvent e) {
				btn_showEvidence.setIcon(new ImageIcon(ViolationRecordFrame.class.getResource("/imgSource/VImageBtn_clicked.png")));
			}
		});
		btn_showEvidence.setIcon(new ImageIcon(ViolationRecordFrame.class.getResource("/imgSource/VImageBtn.png")));
		btn_showEvidence.setBorder(new LineBorder(new Color(0, 0, 0)));
		btn_showEvidence.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				retrieveImage();
			}
		});
		
		JLabel lbl_ViolationLogs = new JLabel("VIOLATION LOGS");
		lbl_ViolationLogs.setHorizontalAlignment(SwingConstants.CENTER);
		lbl_ViolationLogs.setFont(new Font("Baskerville Old Face", Font.BOLD, 28));
		lbl_ViolationLogs.setBounds(464, 64, 756, 34);
		contentPane.add(lbl_ViolationLogs);
		btn_showEvidence.setFont(new Font("Baskerville Old Face", Font.BOLD, 18));
		btn_showEvidence.setEnabled(false);
		btn_showEvidence.setBounds(148, 505, 130, 34);
		contentPane.add(btn_showEvidence);
		
		btn_clearViolation = new JButton("");
		btn_clearViolation.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				btn_clearViolation.setIcon(new ImageIcon(ViolationRecordFrame.class.getResource("/imgSource/clearBtnSmall.png")));
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btn_clearViolation.setIcon(new ImageIcon(ViolationRecordFrame.class.getResource("/imgSource/clearBtnSmall.png")));
			}
			@Override
			public void mouseEntered(MouseEvent e) {
				btn_clearViolation.setIcon(new ImageIcon(ViolationRecordFrame.class.getResource("/imgSource/clearBtnSmall_hover.png")));
			}
			@Override
			public void mousePressed(MouseEvent e) {
				btn_clearViolation.setIcon(new ImageIcon(ViolationRecordFrame.class.getResource("/imgSource/clearBtnSmall_clicked.png")));
			}
		});
		btn_clearViolation.setIcon(new ImageIcon(ViolationRecordFrame.class.getResource("/imgSource/clearBtnSmall.png")));
		btn_clearViolation.setBorder(new LineBorder(new Color(0, 0, 0)));
		btn_clearViolation.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int response = JOptionPane.showConfirmDialog(null, "Are you sure you want to clear the violation history of "+ lbl_outputName.getText() +"? This cannot be undone.", "Warning!", JOptionPane.YES_OPTION, JOptionPane.NO_OPTION);
				if(response == JOptionPane.YES_OPTION) {
					ClearViolation();
					clearDisplay();
					JOptionPane.showMessageDialog(null, "Violation Records Cleared!");
					
					btn_deleteViolation.setEnabled(false);
					btn_clearViolation.setEnabled(false);
					btn_showEvidence.setEnabled(false);
					table_ViolationRecord.clearSelection();
				}
			}
		});
		btn_clearViolation.setEnabled(false);
		btn_clearViolation.setFont(new Font("Baskerville Old Face", Font.BOLD, 16));
		btn_clearViolation.setBounds(174, 600, 104, 34);
		contentPane.add(btn_clearViolation);
		
		btn_deleteViolation = new JButton("");
		btn_deleteViolation.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				btn_deleteViolation.setIcon(new ImageIcon(ViolationRecordFrame.class.getResource("/imgSource/smallDelBtn.png")));
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btn_deleteViolation.setIcon(new ImageIcon(ViolationRecordFrame.class.getResource("/imgSource/smallDelBtn.png")));
			}
			@Override
			public void mouseEntered(MouseEvent e) {
				btn_deleteViolation.setIcon(new ImageIcon(ViolationRecordFrame.class.getResource("/imgSource/smallDelBtn_hover.png")));
			}
			@Override
			public void mousePressed(MouseEvent e) {
				btn_deleteViolation.setIcon(new ImageIcon(ViolationRecordFrame.class.getResource("/imgSource/smallDelBtn_clicked.png")));
			}
		});
		btn_deleteViolation.setIcon(new ImageIcon(ViolationRecordFrame.class.getResource("/imgSource/smallDelBtn.png")));
		btn_deleteViolation.setBorder(new LineBorder(new Color(0, 0, 0)));
		btn_deleteViolation.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int response = JOptionPane.showConfirmDialog(null, "Are you sure you want to delete the selected violation record of "+ lbl_outputName.getText() +"? This cannot be undone.", "Warning!", JOptionPane.YES_OPTION, JOptionPane.NO_OPTION);
				if(response == JOptionPane.YES_OPTION) {
					DeleteViolation();
					clearDisplay();
					JOptionPane.showMessageDialog(null, "Violation Record deleted!");

					btn_deleteViolation.setEnabled(false);
					btn_clearViolation.setEnabled(false);
					btn_showEvidence.setEnabled(false);
					table_ViolationRecord.clearSelection();
				}
			}
		});
		//btn_deleteViolation.setEnabled(false);
		btn_deleteViolation.setFont(new Font("Baskerville Old Face", Font.BOLD, 16));
		btn_deleteViolation.setBounds(288, 600, 104, 34);
		contentPane.add(btn_deleteViolation);
		
		JSeparator lineSeparator = new JSeparator();
		lineSeparator.setOrientation(SwingConstants.VERTICAL);
		lineSeparator.setForeground(Color.LIGHT_GRAY);
		lineSeparator.setBackground(Color.LIGHT_GRAY);
		lineSeparator.setBounds(443, 43, 4, 629);
		contentPane.add(lineSeparator);
		btn_showAll.setFont(new Font("Baskerville Old Face", Font.PLAIN, 16));
		btn_showAll.setBounds(158, 171, 120, 34);
		contentPane.add(btn_showAll);
		
		stdLBL = new JLabel("STUDENT INFORMATION");
		stdLBL.setHorizontalAlignment(SwingConstants.CENTER);
		stdLBL.setFont(new Font("Baskerville Old Face", Font.BOLD, 18));
		stdLBL.setBounds(10, 242, 423, 34);
		contentPane.add(stdLBL);
		
		lbl_outputvCount = new JLabel("");
		lbl_outputvCount.setOpaque(true);
		lbl_outputvCount.setHorizontalAlignment(SwingConstants.CENTER);
		lbl_outputvCount.setBackground(new Color(165, 42, 42));
		lbl_outputvCount.setForeground(Color.WHITE);
		lbl_outputvCount.setFont(new Font("Baskerville Old Face", Font.PLAIN, 18));
		lbl_outputvCount.setBounds(174, 438, 259, 34);
		contentPane.add(lbl_outputvCount);
		
		lbl_vCount = new JLabel("VIOLATION COUNT");
		lbl_vCount.setFont(new Font("Baskerville Old Face", Font.PLAIN, 14));
		lbl_vCount.setBounds(20, 438, 137, 34);
		contentPane.add(lbl_vCount);
		
		lbl_outputstdNum = new JLabel("");
		lbl_outputstdNum.setOpaque(true);
		lbl_outputstdNum.setHorizontalAlignment(SwingConstants.CENTER);
		lbl_outputstdNum.setBackground(new Color(165, 42, 42));
		lbl_outputstdNum.setForeground(Color.WHITE);
		lbl_outputstdNum.setFont(new Font("Baskerville Old Face", Font.PLAIN, 18));
		lbl_outputstdNum.setBounds(174, 303, 259, 34);
		contentPane.add(lbl_outputstdNum);
		
		JLabel lbl_stdNum = new JLabel("STUDENT NUMBER");
		lbl_stdNum.setFont(new Font("Baskerville Old Face", Font.PLAIN, 14));
		lbl_stdNum.setBounds(23, 303, 134, 34);
		contentPane.add(lbl_stdNum);
		
		JLabel lbl_Sn = new JLabel("Student No.");
		lbl_Sn.setFont(new Font("Baskerville Old Face", Font.PLAIN, 17));
		lbl_Sn.setBounds(45, 126, 82, 34);
		contentPane.add(lbl_Sn);
		
		textField_Sn = new JTextField();
		textField_Sn.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_ENTER) {
					btn_search.doClick();
				}
			}
		});
		textField_Sn.setBounds(137, 126, 255, 34);
		contentPane.add(textField_Sn);
		textField_Sn.setColumns(10);
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(464, 109, 756, 500);
		contentPane.add(scrollPane);
		
		table_ViolationRecord = new JTable();
		table_ViolationRecord.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table_ViolationRecord.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_DOWN || e.getKeyCode() == KeyEvent.VK_UP) {
					dataTableLoader();
					btn_deleteViolation.setEnabled(true);
					btn_clearViolation.setEnabled(true);
					btn_showEvidence.setEnabled(true);
				}
				else {
					//Do Nothing
				}
			}
		});
		table_ViolationRecord.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				dataTableLoader();
				btn_deleteViolation.setEnabled(true);
				btn_clearViolation.setEnabled(true);
				btn_showEvidence.setEnabled(true);
			}
		});
		table_ViolationRecord.setDefaultEditor(Object.class, null);
		scrollPane.setViewportView(table_ViolationRecord);
		
		JLabel lbl_searchField = new JLabel("SEARCH");
		lbl_searchField.setHorizontalAlignment(SwingConstants.CENTER);
		lbl_searchField.setFont(new Font("Baskerville Old Face", Font.BOLD, 28));
		lbl_searchField.setBounds(20, 58, 127, 34);
		contentPane.add(lbl_searchField);
		
		btn_back = new JButton("");
		btn_back.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				btn_back.setIcon(new ImageIcon(StudentManagementFrame.class.getResource("/imgSource/btnBack.png")));
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btn_back.setIcon(new ImageIcon(StudentManagementFrame.class.getResource("/imgSource/btnBack.png")));
			}
			@Override
			public void mouseEntered(MouseEvent e) {
				btn_back.setIcon(new ImageIcon(StudentManagementFrame.class.getResource("/imgSource/btnBack_hover.png")));
			}
			@Override
			public void mousePressed(MouseEvent e) {
				btn_back.setIcon(new ImageIcon(StudentManagementFrame.class.getResource("/imgSource/btnBack_clicked.png")));
			}
		});
		btn_back.setIcon(new ImageIcon(StudentManagementFrame.class.getResource("/imgSource/btnBack.png")));
		btn_back.setBorder(new LineBorder(new Color(0, 0, 0)));
		btn_back.setFont(new Font("Baskerville Old Face", Font.PLAIN, 16));
		btn_back.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
				mainFrame frame = new mainFrame();
                frame.start();
                frame.setVisible(true);
				frame.showStats();
				frame.displayName();
			}
		});
		btn_back.setBounds(1139, 650, 105, 40);
		contentPane.add(btn_back);
		
		btn_search = new JButton("");
		btn_search.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				btn_search.setIcon(new ImageIcon(ViolationRecordFrame.class.getResource("/imgSource/searchBtn.png")));
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btn_search.setIcon(new ImageIcon(ViolationRecordFrame.class.getResource("/imgSource/searchBtn.png")));
			}
			@Override
			public void mouseEntered(MouseEvent e) {
				btn_search.setIcon(new ImageIcon(ViolationRecordFrame.class.getResource("/imgSource/searchBtn_hover.png")));
			}
			@Override
			public void mousePressed(MouseEvent e) {
				btn_search.setIcon(new ImageIcon(ViolationRecordFrame.class.getResource("/imgSource/searchBtn_clicked.png")));
			}
		});
		btn_search.setIcon(new ImageIcon(ViolationRecordFrame.class.getResource("/imgSource/searchBtn.png")));
		btn_search.setBorder(new LineBorder(new Color(0, 0, 0)));
		btn_search.setFont(new Font("Baskerville Old Face", Font.PLAIN, 16));
		btn_search.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String StudentNum = textField_Sn.getText();
				
				if(StudentNum.trim().length() == 0) {
					JOptionPane.showMessageDialog(null, "The field is empty!", "Error", JOptionPane.ERROR_MESSAGE);
				}
				else {
				searchSQLData();
				btn_showAll.setEnabled(true);
				
				}
			}	
		
		});
		btn_search.setBounds(288, 171, 104, 34);
		contentPane.add(btn_search);
		
		lbl_name = new JLabel("NAME");
		lbl_name.setFont(new Font("Baskerville Old Face", Font.PLAIN, 14));
		lbl_name.setBounds(115, 348, 42, 34);
		contentPane.add(lbl_name);
		
		lbl_program = new JLabel("PROGRAM");
		lbl_program.setFont(new Font("Baskerville Old Face", Font.PLAIN, 14));
		lbl_program.setBounds(87, 393, 70, 34);
		contentPane.add(lbl_program);
		
		lbl_outputName = new JLabel("");
		lbl_outputName.setOpaque(true);
		lbl_outputName.setHorizontalAlignment(SwingConstants.CENTER);
		lbl_outputName.setBackground(new Color(165, 42, 42));
		lbl_outputName.setForeground(Color.WHITE);
		lbl_outputName.setFont(new Font("Baskerville Old Face", Font.PLAIN, 18));
		lbl_outputName.setBounds(174, 348, 259, 34);
		contentPane.add(lbl_outputName);
		
		lbl_outputProgram = new JLabel("");
		lbl_outputProgram.setOpaque(true);
		lbl_outputProgram.setHorizontalAlignment(SwingConstants.CENTER);
		lbl_outputProgram.setBackground(new Color(165, 42, 42));
		lbl_outputProgram.setForeground(Color.WHITE);
		lbl_outputProgram.setFont(new Font("Baskerville Old Face", Font.PLAIN, 18));
		lbl_outputProgram.setBounds(174, 393, 259, 34);
		contentPane.add(lbl_outputProgram);
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.setBounds(0, 0, 1254, 22);
		contentPane.add(menuBar);
		
		JMenu fileMenu = new JMenu("File");
		menuBar.add(fileMenu);
		
		JMenuItem logoutItem = new JMenuItem("Logout");
		logoutItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mainFrame frame = new mainFrame();
				frame.logoutFunction();
				dispose();
			}
		});
		
		generateReportItem = new JMenuItem("Generate Report");
		generateReportItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (fileChooser.showSaveDialog(ViolationRecordFrame.this) == JFileChooser.APPROVE_OPTION) {
					exportViolationRecordData();
				}
			}
		});
		fileMenu.add(generateReportItem);
		fileMenu.add(logoutItem);
		
		JMenuItem exitItem = new JMenuItem("Exit");
		exitItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		
		RestartItem = new JMenuItem("Restart");
		RestartItem.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				dispose();
				
				splashScreen frame = new splashScreen();
				frame.setLocationRelativeTo(null);
				frame.setVisible(true);
			}
		});
		fileMenu.add(RestartItem);
		fileMenu.add(exitItem);
		
		JMenu editMenu = new JMenu("Edit");
		menuBar.add(editMenu);
		
		JMenuItem refreshtableItem = new JMenuItem("Refresh");
		refreshtableItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ShowSQLData();
			}
		});
		editMenu.add(refreshtableItem);
		
		JMenu navMenu = new JMenu("Navigate");
		menuBar.add(navMenu);
		
		mainFrameItem = new JMenuItem("Home");
		mainFrameItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
				mainFrame frame = new mainFrame();
				frame.start();
				frame.setVisible(true);
				frame.showStats();
				frame.displayName();
			}
		});
		navMenu.add(mainFrameItem);
		
		stdmgmtItem = new JMenuItem("Student Management");
		stdmgmtItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
				StudentManagementFrame frame = new StudentManagementFrame();
				frame.setVisible(true);
			}
		});
		navMenu.add(stdmgmtItem);
		
		prefItem = new JMenuItem("Preferences");
		prefItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
				PreferencesFrame frame = new PreferencesFrame();
				frame.setVisible(true);
				frame.ShowSQLDataActivityLog();
				
			}
		});
		
		suspensionLogItem = new JMenuItem("Suspension Logs");
		suspensionLogItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SuspendedStudentsFrame frame = new SuspendedStudentsFrame();
				frame.setVisible(true);
				
				dispose();
			}
		});
		navMenu.add(suspensionLogItem);
		navMenu.add(prefItem);
		
		helpMenu = new JMenu("Help");
		menuBar.add(helpMenu);
		
		tutorialItem = new JMenuItem("Tutorial");
		tutorialItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mainFrame frame = new mainFrame();
				frame.loadDocu();
			}
		});
		helpMenu.add(tutorialItem);
		
		aboutItem = new JMenuItem("About");
		aboutItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				aboutFrame frame = new aboutFrame();
				frame.setLocationRelativeTo(null);
				frame.setVisible(true);
			}
		});
		helpMenu.add(aboutItem);
		
		JLabel Background = new JLabel("");
		Background.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				btn_deleteViolation.setEnabled(false);
				btn_clearViolation.setEnabled(false);
				btn_showEvidence.setEnabled(false);
				table_ViolationRecord.clearSelection();
				clearDisplay();
			}
		});
		Background.setIcon(new ImageIcon(ViolationRecordFrame.class.getResource("/imgSource/Background.jpg")));
		Background.setBounds(0, 0, 1254, 701);
		contentPane.add(Background);
	}
	
	public void clearDisplay() {
		textField_Sn.setText("");
		lbl_outputstdNum.setText("");
		lbl_outputName.setText("");
		lbl_outputProgram.setText("");
		lbl_outputvCount.setText("");
	}
	
	private void ShowSQLData() {
		try {
			String query = "SELECT `Student Number`, `Violation` FROM studentinfo.violationdb;";
			PreparedStatement pst = db.con().prepareStatement(query);
			ResultSet rs = pst.executeQuery(query);
			table_ViolationRecord.setModel(DbUtils.resultSetToTableModel(rs));
			
			db.con().close();
		} catch (Exception e1) {
		}
	}
	
	
	public void dataTableLoader() {
		int row = table_ViolationRecord.getSelectedRow();
		
	//get the table row value
		String stdSelect = (table_ViolationRecord.getModel().getValueAt(row, 0).toString());
		String query = "SELECT * FROM studentinfo.studentdb WHERE `Student Number` = '"+ stdSelect +"';";			//Students DB Table
		try {
			PreparedStatement pst = db.con().prepareStatement(query);
			ResultSet rs = pst.executeQuery(query);
			
			if(rs.next()) {
				String stdno = rs.getString(2);
				String lname = rs.getString(3);
				String fname = rs.getString(4);
				String suffix = rs.getString(5);
				String mname = rs.getString(6);
				String pG = rs.getString(7);
				String vCount = rs.getString(8);
				
				
				if(suffix.equals("")) {//If no suffix but have middle initial
					lbl_outputstdNum.setText(stdno);
					lbl_outputName.setText(fname + " " + mname + ". " + lname);
					lbl_outputProgram.setText(pG);
					lbl_outputvCount.setText(vCount);
				}
				if(mname.equals("")) {//If no middle Initial and suffix
					lbl_outputstdNum.setText(stdno);
					lbl_outputName.setText(fname + " " + lname);
					lbl_outputProgram.setText(pG);
					lbl_outputvCount.setText(vCount);
				}
				else{//Complete Name
					lbl_outputstdNum.setText(stdno);
					lbl_outputName.setText(fname + " " + mname + ". " + lname + " " + suffix);
					lbl_outputProgram.setText(pG);
					lbl_outputvCount.setText(vCount);
				}
			}
			db.con().close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void searchSQLData() {

		String stdNum = textField_Sn.getText();
		
		try {
			String query = "SELECT `Student Number`, `Violation` FROM studentinfo.violationdb WHERE `Student Number` = '"+ stdNum +"';";		//Violations DB Table
			String query2 = "SELECT * FROM studentinfo.studentdb WHERE `Student Number` = '"+ stdNum +"';";			//Students DB Table
			PreparedStatement pst = db.con().prepareStatement(query);
			PreparedStatement pst2 = db.con().prepareStatement(query2);
			ResultSet rs = pst.executeQuery(query);
			ResultSet rs2 = pst2.executeQuery(query2);
			table_ViolationRecord.setModel(DbUtils.resultSetToTableModel(rs));
			
			if(rs2.next()) {
				String stdno = rs2.getString(2);
				String lname = rs2.getString(3);
				String fname = rs2.getString(4);
				String suffix = rs2.getString(5);
				String mname = rs2.getString(6);
				String pG = rs2.getString(7);
				String vCount = rs2.getString(8);
				
				
				if(suffix.equals("")) {//If no suffix but have middle initial
					lbl_outputstdNum.setText(stdno);
					lbl_outputName.setText(fname + " " + mname + ". " + lname);
					lbl_outputProgram.setText(pG);
					lbl_outputvCount.setText(vCount);
				}
				if(mname.equals("")) {//If no middle Initial and suffix
					lbl_outputstdNum.setText(stdno);
					lbl_outputName.setText(fname + " " + lname);
					lbl_outputProgram.setText(pG);
					lbl_outputvCount.setText(vCount);
				}
				else{//Complete Name
					lbl_outputstdNum.setText(stdno);
					lbl_outputName.setText(fname + " " + mname + ". " + lname + " " + suffix);
					lbl_outputProgram.setText(pG);
					lbl_outputvCount.setText(vCount);
				}
			}
			else {
				JOptionPane.showMessageDialog(null, "No Violation Records found", "Attention", JOptionPane.INFORMATION_MESSAGE);
			}
			db.con().close();
		} catch (Exception e1) {
		}
	
	}
	
	public void retrieveImage() {
		int row = table_ViolationRecord.getSelectedRow();
		String ViolationTime = (table_ViolationRecord.getModel().getValueAt(row, 1).toString());
		
		try {
			String query = "SELECT `Violation Image` FROM studentinfo.violationdb WHERE `Violation` = '"+ ViolationTime +"';";
			PreparedStatement pst = db.con().prepareStatement(query);
			ResultSet rs = pst.executeQuery(query);
			
			File IMGTitle = new File("TempIMGCapture.jpg");
			path = IMGTitle.getCanonicalPath();
			FileOutputStream IMGFile = new FileOutputStream(IMGTitle);
			
			if(rs.next()) {
				InputStream input = rs.getBinaryStream("Violation Image");
				
				byte[] buffer = new byte[1024];
				while (input.read(buffer) > 0) {
					IMGFile.write(buffer);
				}	
			}
			IMGFile.close();
			//Insert run command code
			OpenImageFile();
			
			db.con().close();
		} catch (Exception e1) {
		}
	}
	
	private void OpenImageFile() {
		try {  
			//constructor of file class having file as argument  
			File file = new File(path);   
			
			if(!Desktop.isDesktopSupported()) {//check if Desktop is supported by Platform or not  
				System.out.println("not supported");  
				return;  
			}  
			
			Desktop desktop = Desktop.getDesktop();  
			if(file.exists())         //checks file exists or not  
				desktop.open(file);              //opens the specified file  
			  
		}catch(Exception e)  {  
			e.printStackTrace();  
		}  
	}

	private void OrganizeAutoIncrement() {
		try {
			String query = "ALTER TABLE studentinfo.violationdb DROP COLUMN `#`;";
			PreparedStatement pst = db.con().prepareStatement(query);
			pst.execute();
			
			db.con().close();
		}catch (Exception e){
			JOptionPane.showMessageDialog(null, "Error: " + e);
		}
	
		try {
			String query = "ALTER TABLE studentinfo.violationdb ADD `#` INT PRIMARY KEY NOT NULL AUTO_INCREMENT FIRST;";
			PreparedStatement pst = db.con().prepareStatement(query);
			pst.execute();
			
			db.con().close();
		}catch (Exception e){
			JOptionPane.showMessageDialog(null, "Error: " + e);
		}
	
		try {
			String query = "SELECT * FROM studentinfo.violationdb;";
			PreparedStatement pst = db.con().prepareStatement(query);
			pst.execute();
			
			db.con().close();
			ShowSQLData();
		}catch (Exception e){
			JOptionPane.showMessageDialog(null, "Error: " + e);
		}
	}
	
	public void DeleteViolation() {
		int row = table_ViolationRecord.getSelectedRow();
		String id = (table_ViolationRecord.getModel().getValueAt(row, 0).toString());
		String violationTime = (table_ViolationRecord.getModel().getValueAt(row, 1).toString());
		
		try {			
			String query = "DELETE FROM `studentinfo`.`violationdb` WHERE `Violation` = '"+ violationTime +"';";
			PreparedStatement pst1 = db.con().prepareStatement(query);
			pst1.execute();
			
			db.con().close();
			OrganizeAutoIncrement();
			ShowSQLData();
		}catch (Exception e2){
			JOptionPane.showMessageDialog(null, "Error: " + e2);
		}
		
		//DECREMENT VIOLATION COUNT
		try {
			String sql = "UPDATE `studentinfo`.`studentdb` SET `Violation Count` = `Violation Count` - 1 WHERE `Student Number` = '"+ id +"'";
			PreparedStatement pst2 = db.con().prepareStatement(sql);
			pst2.execute();
			
			db.con().close();
			ShowSQLData();
		}
		catch (SQLException e1) {
		}
		
		deletedViolationName = lbl_outputName.getText();
		userAccountTracker uAT = new userAccountTracker();
		uAT.violationDeleting();
	}
	
	public void ClearViolation() {
		userAccountTracker frame = new userAccountTracker();
		
		int row = table_ViolationRecord.getSelectedRow();
		String id = (table_ViolationRecord.getModel().getValueAt(row, 0).toString());
		try {			
			String query = "DELETE FROM `studentinfo`.`violationdb` WHERE (`Student Number` = '"+ id +"');";
			PreparedStatement pst = db.con().prepareStatement(query);
			pst.execute();
			
			db.con().close();
			OrganizeAutoIncrement();
			ShowSQLData();
		}catch (Exception e2){
		}
			
		try {
			String query = "UPDATE `studentinfo`.`studentdb` SET `Violation Count` = '0' WHERE (`Student Number` = '"+id+"');";
			PreparedStatement pst2 = db.con().prepareStatement(query);
			pst2.execute();
			
			db.con().close();
			ShowSQLData();
		}catch (SQLException e1) {
		}
		
		clearedViolationName = lbl_outputName.getText();
		frame.violationClearing();
	}
	
	public void exportViolationRecordData() {		
		try {
		Document generatedFile = new Document();
		PdfWriter.getInstance(generatedFile, new FileOutputStream(fileChooser.getSelectedFile().toString()+".pdf"));
		
		generatedFile.open();
		
		//Header Components
		Paragraph header = new Paragraph("School Uniform Monitoring System");
		header.setAlignment(1);
		
		Paragraph subheader = new Paragraph("Violation List");
		subheader.setAlignment(1);
		
		generatedFile.add(header);
		generatedFile.add(subheader);
		generatedFile.add(new Paragraph(" "));
		//
		
		//Table Components - Columns
		PdfPTable table = new PdfPTable(4);
		PdfPCell tableColumns = new PdfPCell(new Phrase("Student Number"));
		table.addCell(tableColumns);
		
		tableColumns = new PdfPCell(new Phrase("Name"));
		table.addCell(tableColumns);

		tableColumns = new PdfPCell(new Phrase("Program"));
		table.addCell(tableColumns);
		
		tableColumns = new PdfPCell(new Phrase("Violation Date"));
		table.addCell(tableColumns);
		table.setHeaderRows(1);
		
		//Database Connection
		String queryVio = "SELECT studentinfo.violationdb.`Student Number`, studentinfo.studentdb.`Last Name`, studentinfo.studentdb.`First Name`, studentinfo.studentdb.`Suffix`, studentinfo.studentdb.`Middle Initial`, studentinfo.studentdb.`Program`,studentinfo.violationdb.`Violation` "
				+ "FROM studentinfo.studentdb "
				+ "INNER JOIN studentinfo.violationdb "
				+ "ON studentinfo.studentdb.`Student Number` = studentinfo.violationdb.`Student Number`;";
		PreparedStatement pst = db.con().prepareStatement(queryVio);
		ResultSet rs = pst.executeQuery();
		
		while(rs.next()) {
			String stdNum = rs.getString(1);
			String lName = rs.getString(2);
			String fName = rs.getString(3);
			String suffix = rs.getString(4);
			String mInitial = rs.getString(5);
			String pG = rs.getString(6);
			String vio = rs.getString(7);
	
			
			String fullName = (lName + ", " + fName + ", " + suffix + " " + mInitial + ".");

			table.addCell(stdNum);
			table.addCell(fullName);
			table.addCell(pG);
			table.addCell(vio);
		}	
		
		//Add Table to File
		generatedFile.add(table);
		
		//Close Connection and FileWriter
		db.con().close();
		generatedFile.close();
		
		JOptionPane.showMessageDialog(null, "Report File Generated", "Completed", JOptionPane.INFORMATION_MESSAGE);
		}
		catch (Exception e) {
			System.out.println(e);
		}
	}
	
}