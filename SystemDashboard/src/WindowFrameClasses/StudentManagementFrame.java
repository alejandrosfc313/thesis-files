package WindowFrameClasses;
import java.awt.Color;
import java.awt.Desktop;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileNameExtensionFilter;

import com.itextpdf.text.Document;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPTableFooter;
import com.itextpdf.text.pdf.PdfWriter;

import DatabaseClasses.DBConnection;
import DatabaseClasses.userAccountTracker;
import LoginClassComponents.LoginFrame;
import LoginClassComponents.splashScreen;
import it.sauronsoftware.junique.AlreadyLockedException;
import it.sauronsoftware.junique.JUnique;
import net.proteanit.sql.DbUtils;
import javax.swing.JFileChooser;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.border.LineBorder;

public class StudentManagementFrame extends JFrame {

	private JPanel contentPane;
	private JTextField textField_stdNo;
	public JTextField textField_lastName;
	public JTextField textField_firstName;
	public JTextField textField_suffix;
	public JTextField textField_mInitial;
	public JTable table_dbStudentInfo;
	static StudentManagementFrame framesm = new StudentManagementFrame();
	private JTextField textField_no;
	private JLabel lbl_outputID;
	public JLabel lbl_outputName;
	private JLabel lbl_outputProgram;
	private JButton btn_update;
	public JButton btn_delete;
	private JComboBox<?> programSelector;
	DBConnection db = new DBConnection();
	private JButton btn_add;
	private JLabel lbl_outputvCount;
	private JLabel textfield_FormTitle;
	userAccountTracker uAT = new userAccountTracker();
	private JComboBox comboBox_ProgramCat;
	private JFileChooser genReportChooser;
	private JFileChooser exportToCSVchooser;
	private JFileChooser importFromCSVchooser;
	public static String deletedStudentName, createdStudentName, updatedStudentName;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				String appId = "myapplicationid";
				boolean alreadyRunning;
				try {
					JUnique.acquireLock(appId);
					alreadyRunning = false;
				} catch (AlreadyLockedException e) {
					alreadyRunning = true;
					JOptionPane.showMessageDialog(null, "Application is already running!");
				}
				if (!alreadyRunning) {
					// Start sequence here
					framesm.setVisible(true);
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public StudentManagementFrame() {
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowOpened(WindowEvent e) {
				ShowSQLData();
			}
		});
		
		setTitle("Violation Monitoring Dashboard - Student Management");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1270, 740);
		setLocationRelativeTo(null);
		setUndecorated(false);
		setResizable(false);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		genReportChooser = new JFileChooser();
		genReportChooser.setFileFilter(new FileNameExtensionFilter("*.pdf", "pdf"));
		
		exportToCSVchooser = new JFileChooser();
		exportToCSVchooser.setFileFilter(new FileNameExtensionFilter("*.csv", "csv"));
		
		importFromCSVchooser = new JFileChooser();
		importFromCSVchooser.setFileFilter(new FileNameExtensionFilter("*.csv", "csv"));
		

//Add Student Button
		btn_add = new JButton("");
		btn_add.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				btn_add.setIcon(new ImageIcon(StudentManagementFrame.class.getResource("/imgSource/AddBtn.png")));
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btn_add.setIcon(new ImageIcon(StudentManagementFrame.class.getResource("/imgSource/AddBtn.png")));
			}
			@Override
			public void mouseEntered(MouseEvent e) {
				btn_add.setIcon(new ImageIcon(StudentManagementFrame.class.getResource("/imgSource/AddBtn_hover.png")));
			}
			@Override
			public void mousePressed(MouseEvent e) {
				btn_add.setIcon(new ImageIcon(StudentManagementFrame.class.getResource("/imgSource/AddBtn_clicked.png")));
			}
		});
		btn_add.setIcon(new ImageIcon(StudentManagementFrame.class.getResource("/imgSource/AddBtn.png")));
		btn_add.setBorder(new LineBorder(new Color(0, 0, 0)));
		btn_add.setFont(new Font("Baskerville Old Face", Font.PLAIN, 18));
		btn_add.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AddStudentFunction();
			}
		});
		
		comboBox_ProgramCat = new JComboBox();
		comboBox_ProgramCat.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				clearEntry();
				ShowSQLData();
			}
		});
		comboBox_ProgramCat.setModel(new DefaultComboBoxModel(new String[] {"All", "Computer Engineering", "Dentistry", "Financial Management", "Information Technology", "Mechanical Engineering", "Medical Technology"}));
		comboBox_ProgramCat.setFont(new Font("Segoe UI Semilight", Font.PLAIN, 14));
		comboBox_ProgramCat.setBounds(1074, 65, 170, 25);
		contentPane.add(comboBox_ProgramCat);
		
		JLabel lbl_category = new JLabel("Program:");
		lbl_category.setFont(new Font("Baskerville Old Face", Font.PLAIN, 18));
		lbl_category.setBounds(994, 67, 70, 22);
		contentPane.add(lbl_category);
		
		textfield_FormTitle = new JLabel("Add Form");
		textfield_FormTitle.setFont(new Font("Baskerville Old Face", Font.PLAIN, 25));
		textfield_FormTitle.setBounds(27, 41, 328, 34);
		contentPane.add(textfield_FormTitle);
		
		JSeparator lineSeparator = new JSeparator();
		lineSeparator.setOrientation(SwingConstants.VERTICAL);
		lineSeparator.setForeground(Color.LIGHT_GRAY);
		lineSeparator.setBackground(Color.LIGHT_GRAY);
		lineSeparator.setBounds(377, 41, 4, 629);
		contentPane.add(lineSeparator);
		
		JLabel lbl_StudentInformation = new JLabel("STUDENT INFORMATION");
		lbl_StudentInformation.setFont(new Font("Baskerville Old Face", Font.BOLD, 18));
		lbl_StudentInformation.setBounds(690, 440, 233, 34);
		contentPane.add(lbl_StudentInformation);
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.setBackground(Color.WHITE);
		menuBar.setBounds(0, 0, 1254, 22);
		contentPane.add(menuBar);
		
		JMenu fileMenu = new JMenu("File");
		menuBar.add(fileMenu);
		
		JMenuItem exitItem = new JMenuItem("Exit");
		exitItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		
		JMenuItem logoutItem = new JMenuItem("Logout");
		logoutItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mainFrame frame = new mainFrame();
				frame.logoutFunction();
				dispose();
			}
		});
		
		JMenuItem generateReportItem = new JMenuItem("Generate Report");
		generateReportItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (genReportChooser.showSaveDialog(StudentManagementFrame.this) == JFileChooser.APPROVE_OPTION) {
					exportStudentMgmtData();
				}
			}
		});
		fileMenu.add(generateReportItem);
		
		JMenuItem exportDataItem = new JMenuItem("Export Data");
		exportDataItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (exportToCSVchooser.showSaveDialog(StudentManagementFrame.this) == JFileChooser.APPROVE_OPTION) {
					exportToCSVFile();
				}
			}
		});
		
		JMenuItem importDataItem = new JMenuItem("Import Data");
		importDataItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(importFromCSVchooser.showOpenDialog(StudentManagementFrame.this) == JFileChooser.APPROVE_OPTION) {
					importFromCSVFile();
				}
			}
		});
		fileMenu.add(importDataItem);
		fileMenu.add(exportDataItem);
		
		JMenuItem spacer = new JMenuItem("-------------------");
		spacer.setEnabled(false);
		fileMenu.add(spacer);
		fileMenu.add(logoutItem);
		
		JMenuItem mntmNewMenuItem = new JMenuItem("Restart");
		mntmNewMenuItem.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				dispose();
				splashScreen frame = new splashScreen();
				frame.setLocationRelativeTo(null);
				frame.setVisible(true);
			}
		});
		fileMenu.add(mntmNewMenuItem);
		fileMenu.add(exitItem);
		
		JMenu editMenu = new JMenu("Edit");
		menuBar.add(editMenu);
		
		JMenuItem refreshItem = new JMenuItem("Refresh");
		refreshItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ShowSQLData();
				
				textField_stdNo.setText("");
				textField_lastName.setText("");
				textField_firstName.setText("");
				textField_suffix.setText("");
				textField_mInitial.setText("");
				programSelector.setSelectedItem("-SELECT PROGRAM-");
				textField_no.setText("");
				
				lbl_outputID.setText("");
				lbl_outputName.setText("");
				lbl_outputProgram.setText("");
				lbl_outputvCount.setText("");
			}
		});
		editMenu.add(refreshItem);
		
		JMenuItem clearEntryItem = new JMenuItem("Clear Entries");
		clearEntryItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				clearEntry();
			}
		});
		editMenu.add(clearEntryItem);
		
		JMenu navMenu = new JMenu("Navigate");
		menuBar.add(navMenu);
		
		JMenuItem mainFrameItem = new JMenuItem("Home");
		mainFrameItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
				mainFrame frame = new mainFrame();
				frame.start();
				frame.setVisible(true);
				frame.showStats();
				frame.displayName();
			}
		});
		navMenu.add(mainFrameItem);
		
		JMenuItem violationRecordItem = new JMenuItem("Violation Record");
		violationRecordItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
				ViolationRecordFrame frame = new ViolationRecordFrame();
				frame.setVisible(true);
			}
		});
		
		JMenuItem prefItem = new JMenuItem("Preferences");
		navMenu.add(prefItem);
		prefItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
				PreferencesFrame frame = new PreferencesFrame();
				frame.setVisible(true);
				frame.ShowSQLDataActivityLog();
			}
		});
		
		JMenuItem suspensionLogItem = new JMenuItem("Suspension Logs");
		suspensionLogItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SuspendedStudentsFrame frame = new SuspendedStudentsFrame();
				frame.setVisible(true);
				dispose();
			}
		});
		navMenu.add(suspensionLogItem);
		navMenu.add(violationRecordItem);
		
		JMenu helpMenu = new JMenu("Help");
		menuBar.add(helpMenu);
		
		JMenuItem tutorialItem = new JMenuItem("Tutorial");
		tutorialItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mainFrame frame = new mainFrame();
				frame.loadDocu();
			}
		});
		helpMenu.add(tutorialItem);
		
		JMenuItem aboutItem = new JMenuItem("About");
		aboutItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				aboutFrame frame = new aboutFrame();
				frame.setLocationRelativeTo(null);
				frame.setVisible(true);
			}
		});
		helpMenu.add(aboutItem);
		
		programSelector = new JComboBox();
		programSelector.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_ENTER) {
					AddStudentFunction();
				}
			}
		});
		programSelector.setModel(new DefaultComboBoxModel(new String[] {"-SELECT PROGRAM-", "Mechanical Engineering", "Computer Engineering", "Computer Science", "Information Technology", "Financial Management"}));
		programSelector.setBounds(176, 373, 179, 34);
		contentPane.add(programSelector);
		
		btn_add.setBounds(76, 445, 279, 63);
		contentPane.add(btn_add);
		
		JLabel lbl_stdNumber = new JLabel("STUDENT NUMBER");
		lbl_stdNumber.setFont(new Font("Baskerville Old Face", Font.PLAIN, 15));
		lbl_stdNumber.setHorizontalAlignment(SwingConstants.LEFT);
		lbl_stdNumber.setBounds(27, 148, 139, 34);
		contentPane.add(lbl_stdNumber);
		
		JLabel lbl_ln = new JLabel("LAST NAME");
		lbl_ln.setFont(new Font("Baskerville Old Face", Font.PLAIN, 15));
		lbl_ln.setBounds(79, 193, 87, 34);
		contentPane.add(lbl_ln);
		
		JLabel lbl_fn = new JLabel("FIRST NAME");
		lbl_fn.setFont(new Font("Baskerville Old Face", Font.PLAIN, 15));
		lbl_fn.setBounds(76, 238, 90, 34);
		contentPane.add(lbl_fn);
		
		JLabel lbl_suffix = new JLabel("SUFFIX");
		lbl_suffix.setFont(new Font("Baskerville Old Face", Font.PLAIN, 15));
		lbl_suffix.setBounds(115, 328, 51, 34);
		contentPane.add(lbl_suffix);
		
		JLabel lbl_mI = new JLabel("MIDDLE INITIAL");
		lbl_mI.setFont(new Font("Baskerville Old Face", Font.PLAIN, 15));
		lbl_mI.setBounds(46, 283, 120, 34);
		contentPane.add(lbl_mI);
		
		JLabel lbl_pG = new JLabel("PROGRAM");
		lbl_pG.setFont(new Font("Baskerville Old Face", Font.PLAIN, 15));
		lbl_pG.setBounds(91, 373, 75, 34);
		contentPane.add(lbl_pG);
		
		textField_stdNo = new JTextField();
		textField_stdNo.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
			//Auto Delete Additional Inputs of exceeding 10 char
				if(textField_stdNo.getText().length() > 9) {
					textField_stdNo.setText(textField_stdNo.getText().substring(0, textField_stdNo.getText ().length() - 1));					
				}
			}
		});
		textField_stdNo.setBounds(176, 148, 179, 34);
		contentPane.add(textField_stdNo);
		textField_stdNo.setColumns(10);
		
		textField_lastName = new JTextField();
		textField_lastName.setColumns(10);
		textField_lastName.setBounds(176, 193, 179, 34);
		contentPane.add(textField_lastName);
		
		textField_firstName = new JTextField();
		textField_firstName.setColumns(10);
		textField_firstName.setBounds(176, 238, 179, 34);
		contentPane.add(textField_firstName);
		
		textField_suffix = new JTextField();
		textField_suffix.setColumns(10);
		textField_suffix.setBounds(176, 328, 179, 34);
		contentPane.add(textField_suffix);
		
		textField_mInitial = new JTextField();
		textField_mInitial.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				//Auto Delete Additional Inputs of exceeding 1 char
				if(textField_mInitial.getText().length() > 0) {
					textField_mInitial.setText(textField_mInitial.getText().substring(0, textField_mInitial.getText ().length() - 1));					
				}
			}
		});
		textField_mInitial.setColumns(10);
		textField_mInitial.setBounds(176, 283, 179, 34);
		contentPane.add(textField_mInitial);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(404, 101, 840, 322);
		contentPane.add(scrollPane);
		
//Table
		table_dbStudentInfo = new JTable();
		table_dbStudentInfo.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_DOWN || e.getKeyCode() == KeyEvent.VK_UP) {
					dataTableLoader();
				}
				else {
					//Do Nothing
				}
			}
			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_DELETE) {
					int response = JOptionPane.showConfirmDialog(null, "Are you sure you want to delete the selected student's information?", "Warning!", JOptionPane.YES_OPTION, JOptionPane.NO_OPTION);
					if(response == JOptionPane.YES_OPTION) {
						DeleteSQLData();
						ClearViolation();
						ShowSQLData();
						
						JOptionPane.showMessageDialog(null, "Deleted!");
						clearEntry();
						
					}else if(response == JOptionPane.NO_OPTION) {
				}
				}
			}
		});
		table_dbStudentInfo.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table_dbStudentInfo.setFont(new Font("Segoe UI Semilight", Font.PLAIN, 12));		
		table_dbStudentInfo.setDefaultEditor(Object.class, null);
		table_dbStudentInfo.addMouseListener(new MouseAdapter() {
	
	//Table Mouse Press Event
			@Override			
			public void mousePressed(MouseEvent e) {
				dataTableLoader();
			}
		});		
		scrollPane.setViewportView(table_dbStudentInfo);
		
//Update Student Button
		btn_update = new JButton("");
		btn_update.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				btn_update.setIcon(new ImageIcon(StudentManagementFrame.class.getResource("/imgSource/UpdBtn.png")));
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btn_update.setIcon(new ImageIcon(StudentManagementFrame.class.getResource("/imgSource/UpdBtn.png")));
			}
			@Override
			public void mouseEntered(MouseEvent e) {
				btn_update.setIcon(new ImageIcon(StudentManagementFrame.class.getResource("/imgSource/UpdBtn_hover.png")));
			}
			@Override
			public void mousePressed(MouseEvent e) {
				btn_update.setIcon(new ImageIcon(StudentManagementFrame.class.getResource("/imgSource/UpdBtn_clicked.png")));
			}
		});
		btn_update.setIcon(new ImageIcon(StudentManagementFrame.class.getResource("/imgSource/UpdBtn.png")));
		btn_update.setBorder(new LineBorder(new Color(0, 0, 0)));
		btn_update.setFont(new Font("Baskerville Old Face", Font.PLAIN, 18));
		btn_update.setEnabled(false);
		btn_update.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				UpdateSQLData();
			}
				});
		btn_update.setBounds(76, 593, 279, 63);
		contentPane.add(btn_update);
		
		btn_delete = new JButton("");
		btn_delete.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				btn_delete.setIcon(new ImageIcon(StudentManagementFrame.class.getResource("/imgSource/DelBtn.png")));
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btn_delete.setIcon(new ImageIcon(StudentManagementFrame.class.getResource("/imgSource/DelBtn.png")));
			}
			@Override
			public void mouseEntered(MouseEvent e) {
				btn_delete.setIcon(new ImageIcon(StudentManagementFrame.class.getResource("/imgSource/DelBtn_hover.png")));
			}
			@Override
			public void mousePressed(MouseEvent e) {
				btn_delete.setIcon(new ImageIcon(StudentManagementFrame.class.getResource("/imgSource/DelBtn_clicked.png")));
			}
		});
		btn_delete.setIcon(new ImageIcon(StudentManagementFrame.class.getResource("/imgSource/DelBtn.png")));
		btn_delete.setBorder(new LineBorder(new Color(0, 0, 0)));
		btn_delete.setFont(new Font("Baskerville Old Face", Font.PLAIN, 18));
		btn_delete.setEnabled(false);
		btn_delete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int response = JOptionPane.showConfirmDialog(null, "Are you sure you want to delete the selected student's information?", "Warning!", JOptionPane.YES_OPTION, JOptionPane.NO_OPTION);
				if(response == JOptionPane.YES_OPTION) {
					DeleteSQLData();
					ClearViolation();
					ShowSQLData();
					
					JOptionPane.showMessageDialog(null, "Deleted!");
					clearEntry();
					
				}else if(response == JOptionPane.NO_OPTION) {
			}
		}
		});
		btn_delete.setBounds(76, 519, 279, 63);
		contentPane.add(btn_delete);
		
		textField_no = new JTextField();
		textField_no.setEditable(false);
		textField_no.setColumns(10);
		textField_no.setBounds(176, 103, 179, 34);
		contentPane.add(textField_no);
		
		JLabel lbl_no = new JLabel("NO.");
		lbl_no.setFont(new Font("Baskerville Old Face", Font.PLAIN, 15));
		lbl_no.setBounds(139, 103, 27, 34);
		contentPane.add(lbl_no);
		
		JButton btn_back = new JButton("");
		btn_back.setBorder(new LineBorder(new Color(0, 0, 0)));
		btn_back.setIcon(new ImageIcon(StudentManagementFrame.class.getResource("/imgSource/btnBack.png")));
		btn_back.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				btn_back.setIcon(new ImageIcon(StudentManagementFrame.class.getResource("/imgSource/btnBack.png")));
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btn_back.setIcon(new ImageIcon(StudentManagementFrame.class.getResource("/imgSource/btnBack.png")));
			}
			@Override
			public void mouseEntered(MouseEvent e) {
				btn_back.setIcon(new ImageIcon(StudentManagementFrame.class.getResource("/imgSource/btnBack_hover.png")));
			}
			@Override
			public void mousePressed(MouseEvent e) {
				btn_back.setIcon(new ImageIcon(StudentManagementFrame.class.getResource("/imgSource/btnBack_clicked.png")));
			}
		});
		btn_back.setFont(new Font("Baskerville Old Face", Font.PLAIN, 16));
		btn_back.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
				mainFrame frame = new mainFrame();
                frame.start();
				frame.setVisible(true);
                frame.displayName();
                frame.showStats();
			}
		});
		btn_back.setBounds(1139, 650, 105, 40);
		contentPane.add(btn_back);
		
		JLabel lbl_displayID = new JLabel("STUDENT ID NUMBER");
		lbl_displayID.setHorizontalAlignment(SwingConstants.RIGHT);
		lbl_displayID.setFont(new Font("Baskerville Old Face", Font.PLAIN, 15));
		lbl_displayID.setBounds(404, 502, 171, 27);
		contentPane.add(lbl_displayID);
		
		JLabel lbl_displayName = new JLabel("NAME");
		lbl_displayName.setHorizontalAlignment(SwingConstants.RIGHT);
		lbl_displayName.setFont(new Font("Baskerville Old Face", Font.PLAIN, 15));
		lbl_displayName.setBounds(404, 543, 171, 27);
		contentPane.add(lbl_displayName);
		
		JLabel lbl_displayProgram = new JLabel("PROGRAM");
		lbl_displayProgram.setHorizontalAlignment(SwingConstants.RIGHT);
		lbl_displayProgram.setFont(new Font("Baskerville Old Face", Font.PLAIN, 15));
		lbl_displayProgram.setBounds(404, 580, 171, 27);
		contentPane.add(lbl_displayProgram);
		
		lbl_outputID = new JLabel("");
		lbl_outputID.setForeground(Color.WHITE);
		lbl_outputID.setBackground(new Color(165, 42, 42));
		lbl_outputID.setOpaque(true);
		lbl_outputID.setHorizontalAlignment(SwingConstants.CENTER);
		lbl_outputID.setHorizontalTextPosition(SwingConstants.CENTER);
		lbl_outputID.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 16));
		lbl_outputID.setBounds(585, 496, 440, 34);
		contentPane.add(lbl_outputID);
		
		lbl_outputName = new JLabel("");
		lbl_outputName.setForeground(Color.WHITE);
		lbl_outputName.setBackground(new Color(165, 42, 42));
		lbl_outputName.setHorizontalAlignment(SwingConstants.CENTER);
		lbl_outputName.setOpaque(true);
		lbl_outputName.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 16));
		lbl_outputName.setBounds(585, 533, 440, 36);
		contentPane.add(lbl_outputName);
		
		lbl_outputProgram = new JLabel("");
		lbl_outputProgram.setForeground(Color.WHITE);
		lbl_outputProgram.setBackground(new Color(165, 42, 42));
		lbl_outputProgram.setHorizontalAlignment(SwingConstants.CENTER);
		lbl_outputProgram.setOpaque(true);
		lbl_outputProgram.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 16));
		lbl_outputProgram.setBounds(585, 572, 440, 40);
		contentPane.add(lbl_outputProgram);
		
		JLabel lbl_displayVCount = new JLabel("VIOLATION COUNT");
		lbl_displayVCount.setHorizontalAlignment(SwingConstants.RIGHT);
		lbl_displayVCount.setFont(new Font("Baskerville Old Face", Font.PLAIN, 15));
		lbl_displayVCount.setBounds(404, 623, 171, 27);
		contentPane.add(lbl_displayVCount);
		
		lbl_outputvCount = new JLabel("");
		lbl_outputvCount.setForeground(Color.WHITE);
		lbl_outputvCount.setBackground(new Color(165, 42, 42));
		lbl_outputvCount.setHorizontalAlignment(SwingConstants.CENTER);
		lbl_outputvCount.setOpaque(true);
		lbl_outputvCount.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 16));
		lbl_outputvCount.setBounds(585, 615, 440, 34);
		contentPane.add(lbl_outputvCount);
		
		JLabel Background = new JLabel("");
		Background.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				clearEntry();
			}
		});
		Background.setIcon(new ImageIcon(StudentManagementFrame.class.getResource("/imgSource/Background.jpg")));
		Background.setBounds(0, 2, 1254, 700);
		contentPane.add(Background);	
				
	}
	
	private void AddAndUpdate() {
		try {
			String query = "INSERT INTO `studentinfo`.`studentdb` "
					+ "(`Student Number`, `Last Name`, `First Name`, `Suffix`, `Middle Initial`, `Program`) "
					+ "VALUES (?, ?, ?, ?, ?, ?);";
					
			PreparedStatement pst = db.con().prepareStatement(query);
			pst.setString(1, textField_stdNo.getText());
			pst.setString(2, textField_lastName.getText());
			pst.setString(3, textField_firstName.getText());
			pst.setString(4, textField_suffix.getText());
			pst.setString(5, textField_mInitial.getText());
			pst.setString(6, programSelector.getSelectedItem().toString());
			pst.execute();
			OrganizeAutoIncrement();
			JOptionPane.showMessageDialog(null, "Added and Updated!");
			userAccountTracker frame = new userAccountTracker();
			
			String lName = textField_lastName.getText();
			String fName = textField_firstName.getText();
			String mName = textField_mInitial.getText();
			String sName = textField_suffix.getText();
			createdStudentName = (fName + " " + mName + ". " + lName + " " + sName);
			
			frame.accountCreating();
			db.con().close();
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
		}
	}
	
	private void ShowSQLData() {
		String ProgramCat = comboBox_ProgramCat.getSelectedItem().toString();
		if(ProgramCat.equals("All")) {
			try {
				String query = "SELECT * FROM studentinfo.studentdb;";
				PreparedStatement pst = db.con().prepareStatement(query);
				ResultSet rs = pst.executeQuery(query);
				table_dbStudentInfo.setModel(DbUtils.resultSetToTableModel(rs));
				
				db.con().close();
			} catch (Exception e1) {
				JOptionPane.showMessageDialog(null, "Error: " + e1);
			}
		}
		else {
			try {
				String query = "SELECT * FROM studentinfo.studentdb WHERE Program ='"+ProgramCat+"';";
				PreparedStatement pst = db.con().prepareStatement(query);
				ResultSet rs = pst.executeQuery(query);
				table_dbStudentInfo.setModel(DbUtils.resultSetToTableModel(rs));
				
				db.con().close();
			} catch (Exception e1) {
				JOptionPane.showMessageDialog(null, "Error: " + e1);
			}
		}
	}
	
	private void DeleteSQLData() {
		try {			
			String query = "DELETE FROM `studentinfo`.`studentdb` WHERE (`#` = "+ textField_no.getText() +");";
			PreparedStatement pst = db.con().prepareStatement(query);
			pst.execute();
			
			db.con().close();
			deletedStudentName = lbl_outputName.getText();
			userAccountTracker uAT = new userAccountTracker();
			uAT.accountDeleting();
			ShowSQLData();
		}catch (Exception e){
			JOptionPane.showMessageDialog(null, "Error: " + e);
		}
		OrganizeAutoIncrement();
	}
	
	public void ClearViolation() {		
		try {			
			String query = "DELETE FROM `studentinfo`.`violationdb` WHERE (`Student Number` = '"+ textField_stdNo.getText() +"');";
			PreparedStatement pst = db.con().prepareStatement(query);
			pst.execute();
			
			db.con().close();
			OrganizeAutoIncrement();
			ShowSQLData();
		}catch (Exception e2){
		}
	}
	
	public void UpdateSQLData() {
		try {
			if(table_dbStudentInfo.getSelectedRowCount() == 1) {
				String num = textField_no.getText();
				String stdno = textField_stdNo.getText();
				String lname = textField_lastName.getText();
				String fname = textField_firstName.getText();
				String suffix = textField_suffix.getText();
				String mname = textField_mInitial.getText();
				String pG = programSelector.getSelectedItem().toString();
				updatedStudentName = (fname + " " + mname + ". " + lname + " " + suffix);
			
			String sql = "UPDATE `studentinfo`.`studentdb` SET `Student Number` = '"+ stdno +"', `Last Name` = '"+ lname +"', `First Name` = '"+ fname +"', "
					+ "`Suffix` = '"+ suffix +"', `Middle Initial` = '"+ mname +"', `Program` = '"+ pG +"' WHERE `#` = '"+ num +"'";
			PreparedStatement pst = db.con().prepareStatement(sql);
			pst.execute();
			uAT.accountUpdating();
			
			JOptionPane.showMessageDialog(null, "Updated!");
			
		//Clear Textboxes
			clearEntry();
			db.con().close();
		}
		else if(table_dbStudentInfo.getSelectedRowCount() == 0) {
			JOptionPane.showMessageDialog(null, "Please select row in the table for data to be display for verification.");
		}
	ShowSQLData();
			
	} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		
	}
	
	private void OrganizeAutoIncrement() {
			try {
				String query = "ALTER TABLE studentinfo.studentdb DROP COLUMN `#`;";
				PreparedStatement pst = db.con().prepareStatement(query);
				pst.execute();
				
				db.con().close();
			}catch (Exception e){
				JOptionPane.showMessageDialog(null, "Error: " + e);
			}
		
			try {
				String query = "ALTER TABLE studentinfo.studentdb ADD `#` INT PRIMARY KEY NOT NULL AUTO_INCREMENT FIRST;";
				PreparedStatement pst = db.con().prepareStatement(query);
				pst.execute();
				
				db.con().close();
			}catch (Exception e){
				JOptionPane.showMessageDialog(null, "Error: " + e);
			}
			try {
				String query = "SELECT * FROM studentinfo.studentdb;";
				PreparedStatement pst = db.con().prepareStatement(query);
				pst.execute();
				
				db.con().close();
				ShowSQLData();
			}catch (Exception e){
				JOptionPane.showMessageDialog(null, "Error: " + e);
			}
	}
	
//Display Table Data into TextFields
	public void dataTableLoader() {
		
		textfield_FormTitle.setText("Edit Form");
		String id, stdno, lname, fname, suffix, mname, pG, vCount;
		int row = table_dbStudentInfo.getSelectedRow();
		
	//get the table row value
		id = (table_dbStudentInfo.getModel().getValueAt(row, 0).toString());
		stdno = (table_dbStudentInfo.getModel().getValueAt(row, 1).toString());
		lname = (table_dbStudentInfo.getModel().getValueAt(row, 2).toString());
		fname = (table_dbStudentInfo.getModel().getValueAt(row, 3).toString());
		suffix = (table_dbStudentInfo.getModel().getValueAt(row, 4).toString());
		mname = (table_dbStudentInfo.getModel().getValueAt(row, 5).toString());
		pG = (table_dbStudentInfo.getModel().getValueAt(row, 6).toString());
		vCount = (table_dbStudentInfo.getModel().getValueAt(row, 7).toString());
		
	//display the table row value
		textField_no.setText(id);
		textField_stdNo.setText(stdno);
		textField_lastName.setText(lname);
		textField_firstName.setText(fname);
		textField_suffix.setText(suffix);
		textField_mInitial.setText(mname);
		programSelector.setSelectedItem(pG);
		
	
	//Enable and Disable Buttons
		btn_add.setEnabled(false);
		btn_update.setEnabled(true);
		btn_delete.setEnabled(true);
		
	//Student Info Display
		if(suffix.equals("")) {//If no suffix but have middle initial
			lbl_outputID.setText(stdno);
			lbl_outputName.setText(fname + " " + mname + ". " + lname);
			lbl_outputProgram.setText(pG);
			lbl_outputvCount.setText(vCount);
		}
		if(mname.equals("")) {//If no middle Initial and suffix
			lbl_outputID.setText(stdno);
			lbl_outputName.setText(fname + " " + lname);
			lbl_outputProgram.setText(pG);
			lbl_outputvCount.setText(vCount);
		}
		else{//Complete Name
			lbl_outputID.setText(stdno);
			lbl_outputName.setText(fname + " " + mname + ". " + lname + " " + suffix);
			lbl_outputProgram.setText(pG);
			lbl_outputvCount.setText(vCount);
		}
		
	}
	
	public void clearEntry() {
		textfield_FormTitle.setText("Add Form");
		ShowSQLData();
		
		btn_add.setEnabled(true);
		btn_update.setEnabled(false);
		btn_delete.setEnabled(false);
		
		textField_stdNo.setText("");
		textField_lastName.setText("");
		textField_firstName.setText("");
		textField_suffix.setText("");
		textField_mInitial.setText("");
		programSelector.setSelectedItem("-SELECT PROGRAM-");
		textField_no.setText("");
		
		lbl_outputID.setText("");
		lbl_outputName.setText("");
		lbl_outputProgram.setText("");
		lbl_outputvCount.setText("");
	}
	
	public void AddStudentFunction() {

		String studentNum = textField_stdNo.getText();
		String lastName = textField_lastName.getText();
		String firstName = textField_firstName.getText();
		String proG = programSelector.getSelectedItem().toString();
		
		//If Primary Info is null
		if(studentNum.equalsIgnoreCase("") || lastName.equalsIgnoreCase("") || firstName.equalsIgnoreCase("") || proG.equalsIgnoreCase("-SELECT PROGRAM-")) {
			JOptionPane.showMessageDialog(null, "Incomplete Details!", "Warning!", JOptionPane.WARNING_MESSAGE);
		}				
		//Add Student Info
		else {
			AddAndUpdate();		
			clearEntry();
		}
		
	}
	
	public void exportStudentMgmtData() {		
		try {
		Document generatedFile = new Document();
		PdfWriter.getInstance(generatedFile, new FileOutputStream(genReportChooser.getSelectedFile().toString()+".pdf"));
		
		generatedFile.open();
		
		//Header Components
		Paragraph header = new Paragraph("School Uniform Monitoring System");
		header.setAlignment(1);
		
		Paragraph subheader = new Paragraph("Student List");
		subheader.setAlignment(1);
		
		generatedFile.add(header);
		generatedFile.add(subheader);
		generatedFile.add(new Paragraph(" "));
		//
		
		//Table Components - Columns
		PdfPTable table = new PdfPTable(4);
		PdfPCell tableColumns = new PdfPCell(new Phrase("Student Number"));
		table.addCell(tableColumns);
		
		tableColumns = new PdfPCell(new Phrase("Name"));
		table.addCell(tableColumns);
		
		tableColumns = new PdfPCell(new Phrase("Program"));
		table.addCell(tableColumns);
		
		tableColumns = new PdfPCell(new Phrase("Violation Count"));
		table.addCell(tableColumns);
		table.setHeaderRows(1);
		
		//Database Connection
		String query = "SELECT `Student Number`, `Last Name`, `First Name`, `Suffix`, `Middle Initial`, `Program`, `Violation Count` from studentinfo.studentdb;";
		PreparedStatement pst = db.con().prepareStatement(query);
		ResultSet rs = pst.executeQuery();
		
		while(rs.next()) {
			String stdNum = rs.getString(1);
			String lName = rs.getString(2);
			String fName = rs.getString(3);
			String sFix = rs.getString(4);
			String mInitial = rs.getString(5);
			String program = rs.getString(6);
			String vCount = rs.getString(7);
			
			String fullName = (lName +", "+fName+" "+sFix+" "+mInitial+". ");
		

			table.addCell(stdNum);
			table.addCell(fullName);
			table.addCell(program);
			table.addCell(vCount);
		}	
		
		//Add Table to File
		generatedFile.add(table);
		
		//Close Connection and FileWriter
		db.con().close();
		generatedFile.close();
		
		JOptionPane.showMessageDialog(null, "Report File Generated", "Completed", JOptionPane.INFORMATION_MESSAGE);
		
		userAccountTracker uAT = new userAccountTracker();
		uAT.generateReport();
		}
		catch (Exception e) {
			System.out.println(e);
		}
	}
	
	public void exportToCSVFile() {
		try {
			String query = "SELECT * from studentinfo.studentdb;";
			PreparedStatement pst = db.con().prepareStatement(query);
	        FileWriter fw = new FileWriter(exportToCSVchooser.getSelectedFile().toString() + ".csv");
	        ResultSet rs = pst.executeQuery(query);

	        int cols = rs.getMetaData().getColumnCount();

	        for(int i = 1; i <= cols; i ++){
	           fw.append(rs.getMetaData().getColumnLabel(i));
	           if(i < cols) fw.append(',');
	           else fw.append('\n');
	        }

	        while (rs.next()) {

	           for(int i = 1; i <= cols; i ++){
	               fw.append(rs.getString(i));
	               if(i < cols) fw.append(',');
	           }
	           fw.append('\n');
	        }
	        fw.flush();
	        fw.close();
	        
	        db.con().close();
	        
	        JOptionPane.showMessageDialog(null, "CSV File Exported Successfully");
	        userAccountTracker uAT = new userAccountTracker();
	        uAT.dataExport();
	        
	    } catch (Exception e) {
	   }
	}
	
	public void importFromCSVFile() {
		int batchSize = 20;
		
		try {
            String sql = "INSERT INTO `studentinfo`.`studentdb` (`Student Number`, `Last Name`, `First Name`, `Suffix`, `Middle Initial`, `Program`) VALUES (?, ?, ?, ?, ?, ?);";
            PreparedStatement statement = db.con().prepareStatement(sql);
 
            BufferedReader lineReader = new BufferedReader(new FileReader(importFromCSVchooser.getSelectedFile()));
            String lineText = null;
 
            int count = 0;
 
            lineReader.readLine(); // skip header line
 
            while ((lineText = lineReader.readLine()) != null) {
                String[] data = lineText.split(",");
                String numOrder = data[0];
                String studentNumber = data[1];
                String lName = data[2];
                String fName = data[3];
                String sFix = data[4];
                String mInitial = data[5];
                String program = data[6];
                String vCount = data[7];
 
                statement.setString(1, studentNumber);
                statement.setString(2, lName); 
                statement.setString(3, fName);
                statement.setString(4, sFix);
                statement.setString(5, mInitial);
                statement.setString(6, program);
 
                statement.addBatch();
 
                if (count % batchSize == 0) {
                    statement.executeBatch();
                }
            }
 
            lineReader.close();
 
            // execute the remaining queries
            statement.executeBatch();
 
            //db.con().commit();
            db.con().close();
            JOptionPane.showMessageDialog(null, "CSV Data import successfully");
            
            userAccountTracker uAT = new userAccountTracker();
            uAT.dataImport();
            ShowSQLData();
 
        } catch (IOException ex) {
            System.err.println(ex);
        } catch (SQLException ex) {
            ex.printStackTrace();
 
            try {
                db.con().rollback();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
	}
}