package WindowFrameClasses;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import DatabaseClasses.DBConnection;
import DatabaseClasses.userAccountTracker;
import LoginClassComponents.splashScreen;
import accountManagementClasses.CreateAccountFrame;
import accountManagementClasses.EditUserFrame;
import it.sauronsoftware.junique.AlreadyLockedException;
import it.sauronsoftware.junique.JUnique;
import net.proteanit.sql.DbUtils;
import javax.swing.JToggleButton;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class PreferencesFrame extends JFrame {

	private JPanel contentPane;
	static PreferencesFrame framesettings = new PreferencesFrame();
	userAccountTracker uAT = new userAccountTracker();
	private JTextField Field_IP;
	private JTextField Field_uName;
	private JPasswordField Field_pWord;
	public JTable accountTable;
	DBConnection db = new DBConnection();
	private JButton btn_EditUser;
	JButton btn_DeleteUser;
	public JPanel userMgmt;
	public JTabbedPane settingTabs;
	private JTable actionLogTable;
	private JButton btn_DeleteHistory;
	private JPanel unifRecog;
	private JToggleButton tglbtn_workingMode;
	private int statusSet;
	private String IPAddress;
	private String UserName;
	private String PassWord;
	private JLabel lbl_Status;
	public static String id;
	private JTextField userField;
	private JPasswordField passField;
	private JPanel authenticatorUserMgmt;
	private JPanel userManagement;
	private JTextField userTextField;
	private JPasswordField passwordField;
	private JPanel authenticatorReset;
	private JButton btn_Reset;
	private JPanel actlogOptions;
	private JTextField txtField_uName;
	private JPasswordField pf_password;
	private JPanel authenticatorActlog;
	private JLabel lbl_statusIndicator;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				String appId = "myapplicationid";
				boolean alreadyRunning;
				try {
					JUnique.acquireLock(appId);
					alreadyRunning = false;
				} catch (AlreadyLockedException e) {
					alreadyRunning = true;
					JOptionPane.showMessageDialog(null, "Application is already running!");
				}
				if (!alreadyRunning) {
					// Start sequence here
					framesettings.setVisible(true);
					framesettings.ShowSQLDataActivityLog();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public PreferencesFrame() {		
		setTitle("Preferences");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1270, 740);
		setLocationRelativeTo(null);
		setUndecorated(false);
		setResizable(false);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		ShowSQLDataActivityLog();
		dbconfigLoad();
		
		JButton btn_back = new JButton("Back");
		btn_back.setFont(new Font("Baskerville Old Face", Font.PLAIN, 16));
		btn_back.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
				mainFrame frame = new mainFrame();
				frame.start();
				frame.setVisible(true);
				frame.displayName();
				frame.showStats();
			}
		});
		
		settingTabs = new JTabbedPane(JTabbedPane.TOP);
		settingTabs.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				if(settingTabs.getSelectedIndex() == 0) {
					ShowSQLDataActivityLog();					
				}
				if(settingTabs.getSelectedIndex() == 2) {
					loadStatus();
				}
			}
		});
		settingTabs.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 12));
		settingTabs.setBounds(189, 99, 880, 490);
		contentPane.add(settingTabs);
		
		userMgmt = new JPanel();
		userMgmt.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				ShowSQLDataUserMgmt();
				btn_EditUser.setEnabled(false);
				btn_DeleteUser.setEnabled(false);
			}
		});
		
		JPanel actLog = new JPanel();
		actLog.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				btn_DeleteHistory.setEnabled(false);
				ShowSQLDataActivityLog();
			}
		});
		settingTabs.addTab("Activity Log", null, actLog, null);
		actLog.setLayout(null);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(271, 180, 581, 257);
		actLog.add(scrollPane_1);
		
		actionLogTable = new JTable();
		actionLogTable.setDefaultEditor(Object.class, null);
		actionLogTable.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				btn_DeleteHistory.setEnabled(true);
			}
		});
		scrollPane_1.setViewportView(actionLogTable);
		
		JLabel lbl_ActionHistory = new JLabel("Activity Log");
		lbl_ActionHistory.setFont(new Font("Baskerville Old Face", Font.PLAIN, 23));
		lbl_ActionHistory.setBounds(10, 11, 168, 40);
		actLog.add(lbl_ActionHistory);
		
		JTextArea lbl_descriptionActLog1 = new JTextArea();
		lbl_descriptionActLog1.setText("Actions performed within the program are digitally stored intended for reporting and auditing purposes. Activity log list all \r\nthe information of who, what and when the action happened.");
		lbl_descriptionActLog1.setOpaque(false);
		lbl_descriptionActLog1.setFont(new Font("Baskerville Old Face", Font.PLAIN, 16));
		lbl_descriptionActLog1.setEditable(false);
		lbl_descriptionActLog1.setBounds(47, 62, 785, 40);
		actLog.add(lbl_descriptionActLog1);
		
		actlogOptions = new JPanel();
		actlogOptions.setVisible(false);
		
		authenticatorActlog = new JPanel();
		authenticatorActlog.setBounds(10, 144, 251, 293);
		actLog.add(authenticatorActlog);
		authenticatorActlog.setLayout(null);
		
		JLabel lblNewLabel_uName = new JLabel("Username:");
		lblNewLabel_uName.setFont(new Font("Baskerville Old Face", Font.PLAIN, 16));
		lblNewLabel_uName.setBounds(10, 122, 70, 14);
		authenticatorActlog.add(lblNewLabel_uName);
		
		JLabel lblPassword = new JLabel("Password:");
		lblPassword.setFont(new Font("Baskerville Old Face", Font.PLAIN, 16));
		lblPassword.setBounds(16, 162, 64, 14);
		authenticatorActlog.add(lblPassword);
		
		txtField_uName = new JTextField();
		txtField_uName.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_ENTER) {
					authActLog();
				}
			}
		});
		txtField_uName.setBounds(90, 109, 151, 30);
		authenticatorActlog.add(txtField_uName);
		txtField_uName.setColumns(10);
		
		pf_password = new JPasswordField();
		pf_password.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_ENTER) {
					authActLog();
				}
			}
		});
		pf_password.setBounds(90, 150, 151, 29);
		authenticatorActlog.add(pf_password);
		
		JButton actlogAuth = new JButton("Authenticate");
		actlogAuth.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				authActLog();
			}
		});
		actlogAuth.setFont(new Font("Baskerville Old Face", Font.PLAIN, 14));
		actlogAuth.setBounds(76, 209, 105, 30);
		authenticatorActlog.add(actlogAuth);
		
		JLabel lblNewLabel = new JLabel("Authentication Required");
		lblNewLabel.setFont(new Font("Baskerville Old Face", Font.PLAIN, 18));
		lblNewLabel.setBounds(10, 11, 231, 21);
		authenticatorActlog.add(lblNewLabel);
		
		JTextArea actLogAuth = new JTextArea();
		actLogAuth.setOpaque(false);
		actLogAuth.setFont(new Font("Baskerville Old Face", Font.PLAIN, 16));
		actLogAuth.setText("Sign in with root credentials for\r\nmodification privileges.");
		actLogAuth.setBounds(20, 32, 221, 45);
		authenticatorActlog.add(actLogAuth);
		actlogOptions.setBounds(10, 144, 251, 293);
		actLog.add(actlogOptions);
		actlogOptions.setLayout(null);
		
		JLabel lbl_accountOptions_1 = new JLabel("Available Options");
		lbl_accountOptions_1.setBounds(30, 46, 138, 40);
		actlogOptions.add(lbl_accountOptions_1);
		lbl_accountOptions_1.setFont(new Font("Baskerville Old Face", Font.PLAIN, 19));
		
		btn_DeleteHistory = new JButton("Delete");
		btn_DeleteHistory.setBounds(67, 97, 152, 40);
		actlogOptions.add(btn_DeleteHistory);
		btn_DeleteHistory.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {			
			int response = JOptionPane.showConfirmDialog(null, "Are you sure you want to delete this action history? This cannot be undone", "Confirmation", JOptionPane.YES_OPTION, JOptionPane.NO_OPTION);
				if(response == JOptionPane.YES_OPTION) {
					DeleteSQLDataActivityLog();
					OrganizeAutoIncrement();
					
				}
			}
		});
		btn_DeleteHistory.setFont(new Font("Baskerville Old Face", Font.PLAIN, 17));
		btn_DeleteHistory.setEnabled(false);
		
		JButton btn_ClearHistory_ = new JButton("Clear");
		btn_ClearHistory_.setBounds(67, 148, 152, 40);
		actlogOptions.add(btn_ClearHistory_);
		btn_ClearHistory_.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			int response = JOptionPane.showConfirmDialog(null, "Are you sure you want to clear the action history? This cannot be undone", "Confirmation", JOptionPane.YES_OPTION, JOptionPane.NO_OPTION);
				if(response == JOptionPane.YES_OPTION) {
					ClearSQLDataActivityLog();
					OrganizeAutoIncrement();
				}
			}
		});
		btn_ClearHistory_.setFont(new Font("Baskerville Old Face", Font.PLAIN, 17));
		
		lbl_statusIndicator = new JLabel("Read-Only Mode");
		lbl_statusIndicator.setHorizontalAlignment(SwingConstants.CENTER);
		lbl_statusIndicator.setFont(new Font("Baskerville Old Face", Font.PLAIN, 16));
		lbl_statusIndicator.setBounds(271, 144, 581, 25);
		actLog.add(lbl_statusIndicator);
		settingTabs.addTab("User Management", null, userMgmt, null);
		userMgmt.setLayout(null);
		
		JLabel lbl_userManagement = new JLabel("User Management");
		lbl_userManagement.setFont(new Font("Baskerville Old Face", Font.PLAIN, 23));
		lbl_userManagement.setBounds(10, 11, 168, 40);
		userMgmt.add(lbl_userManagement);
		
		JTextArea lblDescription_userManagement = new JTextArea();
		lblDescription_userManagement.setText("The User Management Settings allows you to add, change and remove users that can Log In into the system.");
		lblDescription_userManagement.setOpaque(false);
		lblDescription_userManagement.setFont(new Font("Baskerville Old Face", Font.PLAIN, 16));
		lblDescription_userManagement.setEditable(false);
		lblDescription_userManagement.setBounds(47, 62, 785, 40);
		userMgmt.add(lblDescription_userManagement);
		
		userManagement = new JPanel();
		userManagement.setVisible(false);
		
		authenticatorUserMgmt = new JPanel();
		authenticatorUserMgmt.setBounds(0, 92, 875, 368);
		userMgmt.add(authenticatorUserMgmt);
		authenticatorUserMgmt.setLayout(null);
		
		JLabel lbl_userName = new JLabel("Username:");
		lbl_userName.setFont(new Font("Baskerville Old Face", Font.PLAIN, 18));
		lbl_userName.setBounds(275, 120, 78, 33);
		authenticatorUserMgmt.add(lbl_userName);
		
		JLabel lbl_passWord = new JLabel("Password:");
		lbl_passWord.setFont(new Font("Baskerville Old Face", Font.PLAIN, 18));
		lbl_passWord.setBounds(284, 173, 69, 33);
		authenticatorUserMgmt.add(lbl_passWord);
		
		userField = new JTextField();
		userField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_ENTER) {
					userMgmtAuth();
				}
			}
		});
		userField.setBounds(363, 120, 210, 33);
		authenticatorUserMgmt.add(userField);
		userField.setColumns(10);
		
		passField = new JPasswordField();
		passField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_ENTER) {
					userMgmtAuth();
				}
			}
		});
		passField.setBounds(363, 174, 210, 33);
		authenticatorUserMgmt.add(passField);
		
		JButton btnNewButton = new JButton("Authenticate");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				userMgmtAuth();
			}
		});
		btnNewButton.setFont(new Font("Baskerville Old Face", Font.PLAIN, 16));
		btnNewButton.setBounds(379, 245, 125, 45);
		authenticatorUserMgmt.add(btnNewButton);
		
		JLabel lblNewLabel_1 = new JLabel("Authentication Required");
		lblNewLabel_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_1.setFont(new Font("Baskerville Old Face", Font.PLAIN, 20));
		lblNewLabel_1.setBounds(10, 28, 855, 20);
		authenticatorUserMgmt.add(lblNewLabel_1);
		
		JLabel lblNewLabel_1_1 = new JLabel("Sign in with root credentials in order to view user management settings.");
		lblNewLabel_1_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_1_1.setFont(new Font("Baskerville Old Face", Font.PLAIN, 16));
		lblNewLabel_1_1.setBounds(10, 50, 855, 20);
		authenticatorUserMgmt.add(lblNewLabel_1_1);
		userManagement.setBounds(0, 92, 875, 368);
		userMgmt.add(userManagement);
		userManagement.setLayout(null);
		
		JLabel lbl_accountOptions = new JLabel("Account Options");
		lbl_accountOptions.setBounds(10, 28, 123, 40);
		userManagement.add(lbl_accountOptions);
		lbl_accountOptions.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 16));
		
		JButton btn_AddUser = new JButton("Add");
		btn_AddUser.setBounds(47, 96, 152, 40);
		userManagement.add(btn_AddUser);
		btn_AddUser.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				accountTable.clearSelection();
				
				CreateAccountFrame frame = new CreateAccountFrame();
				frame.setVisible(true);
				
				dispose();
			}
		});
		btn_AddUser.setFont(new Font("Baskerville Old Face", Font.PLAIN, 17));
		
		btn_EditUser = new JButton("Edit");
		btn_EditUser.setBounds(47, 147, 152, 40);
		userManagement.add(btn_EditUser);
		btn_EditUser.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				hide();
				EditUserFrame frame = new EditUserFrame();
				frame.setVisible(true);
			}
		});
		btn_EditUser.setEnabled(false);
		btn_EditUser.setFont(new Font("Baskerville Old Face", Font.PLAIN, 17));
		
		btn_DeleteUser = new JButton("Delete");
		btn_DeleteUser.setBounds(47, 198, 152, 40);
		userManagement.add(btn_DeleteUser);
		btn_DeleteUser.setEnabled(false);
		btn_DeleteUser.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int response = JOptionPane.showConfirmDialog(null, "Are you sure you want to delete the selected student's information?", "Warning!", JOptionPane.YES_OPTION, JOptionPane.NO_OPTION);
				if(response == JOptionPane.YES_OPTION) {
					DeleteSQLData();
					ShowSQLDataUserMgmt();
					JOptionPane.showMessageDialog(null, "Deleted!");
				}
			}
		});
		btn_DeleteUser.setFont(new Font("Baskerville Old Face", Font.PLAIN, 17));
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(252, 21, 580, 315);
		userManagement.add(scrollPane);
		
		accountTable = new JTable();
		accountTable.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				int row = accountTable.getSelectedRow();
				if (accountTable.getSelectedRowCount() == 1) {
					id = (accountTable.getModel().getValueAt(row, 0).toString());
					btn_EditUser.setEnabled(true);
					btn_DeleteUser.setEnabled(true);
				}
			}
		});
		accountTable.setDefaultEditor(Object.class, null);
		scrollPane.setViewportView(accountTable);		
		
		JSeparator lineSeparator = new JSeparator();
		lineSeparator.setBounds(238, 21, 4, 325);
		userManagement.add(lineSeparator);
		lineSeparator.setOrientation(SwingConstants.VERTICAL);
		lineSeparator.setForeground(Color.LIGHT_GRAY);
		lineSeparator.setBackground(Color.LIGHT_GRAY);
		
		unifRecog = new JPanel();
		settingTabs.addTab("Uniform Recognition", null, unifRecog, null);
		unifRecog.setLayout(null);
		
		JLabel lbl_unifRecog = new JLabel("Uniform Recognition");
		lbl_unifRecog.setFont(new Font("Baskerville Old Face", Font.PLAIN, 23));
		lbl_unifRecog.setBounds(10, 11, 195, 40);
		unifRecog.add(lbl_unifRecog);
		
		JTextArea lbl_descriptionActLog1_1 = new JTextArea();
		lbl_descriptionActLog1_1.setText("This settings allows the Uniform Recognition System to be actively monitored and override the detection mode of the \r\nUniform Recognition System.");
		lbl_descriptionActLog1_1.setOpaque(false);
		lbl_descriptionActLog1_1.setFont(new Font("Baskerville Old Face", Font.PLAIN, 16));
		lbl_descriptionActLog1_1.setEditable(false);
		lbl_descriptionActLog1_1.setBounds(47, 62, 785, 40);
		unifRecog.add(lbl_descriptionActLog1_1);
		
		JSeparator lineSeparator_1 = new JSeparator();
		lineSeparator_1.setOrientation(SwingConstants.VERTICAL);
		lineSeparator_1.setForeground(Color.LIGHT_GRAY);
		lineSeparator_1.setBackground(Color.LIGHT_GRAY);
		lineSeparator_1.setBounds(406, 117, 4, 316);
		unifRecog.add(lineSeparator_1);
		
		tglbtn_workingMode = new JToggleButton("Detection Mode");
		tglbtn_workingMode.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(tglbtn_workingMode.isSelected()) {
					DetectionMode();
					lbl_Status.setIcon(new ImageIcon(PreferencesFrame.class.getResource("/imgSource/DetectionMode.png")));
				}
				else {
					IdleMode();
					lbl_Status.setIcon(new ImageIcon(PreferencesFrame.class.getResource("/imgSource/IdleMode.png")));
				}
			}
		});
		tglbtn_workingMode.setFont(new Font("Baskerville Old Face", Font.PLAIN, 16));
		tglbtn_workingMode.setBounds(145, 265, 140, 45);
		unifRecog.add(tglbtn_workingMode);
		
		JLabel lbl_manualOverride = new JLabel("Manual Override");
		lbl_manualOverride.setFont(new Font("Baskerville Old Face", Font.PLAIN, 19));
		lbl_manualOverride.setBounds(10, 117, 135, 40);
		unifRecog.add(lbl_manualOverride);
		
		JTextArea lbl_descriptionActLog1_1_1 = new JTextArea();
		lbl_descriptionActLog1_1_1.setText("This settings allows the Uniform Recognition System to be set \r\non Detection/Idle mode.");
		lbl_descriptionActLog1_1_1.setOpaque(false);
		lbl_descriptionActLog1_1_1.setFont(new Font("Baskerville Old Face", Font.PLAIN, 15));
		lbl_descriptionActLog1_1_1.setEditable(false);
		lbl_descriptionActLog1_1_1.setBounds(20, 155, 376, 58);
		unifRecog.add(lbl_descriptionActLog1_1_1);
		
		JLabel lbl_currentStatus = new JLabel("Status");
		lbl_currentStatus.setFont(new Font("Baskerville Old Face", Font.PLAIN, 19));
		lbl_currentStatus.setBounds(420, 117, 45, 40);
		unifRecog.add(lbl_currentStatus);
		
		JTextArea lblDescription_Warning_1 = new JTextArea();
		lblDescription_Warning_1.setText("Warning: This setting controls the operating protocol of the Object \r\nRecognition System.");
		lblDescription_Warning_1.setOpaque(false);
		lblDescription_Warning_1.setFont(new Font("Segoe UI Light", Font.PLAIN, 12));
		lblDescription_Warning_1.setEditable(false);
		lblDescription_Warning_1.setBounds(20, 393, 376, 40);
		unifRecog.add(lblDescription_Warning_1);
		
		lbl_Status = new JLabel("");
		lbl_Status.setHorizontalAlignment(SwingConstants.CENTER);
		lbl_Status.setBounds(420, 155, 445, 281);
		unifRecog.add(lbl_Status);
		
		JPanel Connectivity = new JPanel();
		settingTabs.addTab("Connectivity", null, Connectivity, null);
		Connectivity.setLayout(null);
		
		JLabel lbl_mysqlDB = new JLabel("MySQL Database Connectivity");
		lbl_mysqlDB.setFont(new Font("Baskerville Old Face", Font.PLAIN, 23));
		lbl_mysqlDB.setBounds(10, 11, 287, 40);
		Connectivity.add(lbl_mysqlDB);
		
		JLabel hostAddLBL = new JLabel("Host Address");
		hostAddLBL.setFont(new Font("Baskerville Old Face", Font.PLAIN, 18));
		hostAddLBL.setBounds(219, 152, 96, 33);
		Connectivity.add(hostAddLBL);
		
		JLabel uNameLBL = new JLabel("Username");
		uNameLBL.setFont(new Font("Baskerville Old Face", Font.PLAIN, 18));
		uNameLBL.setBounds(242, 206, 73, 33);
		Connectivity.add(uNameLBL);
		
		JLabel passwordLBL = new JLabel("Password");
		passwordLBL.setFont(new Font("Baskerville Old Face", Font.PLAIN, 18));
		passwordLBL.setBounds(247, 258, 68, 33);
		Connectivity.add(passwordLBL);
		
		Field_IP = new JTextField(IPAddress);
		Field_IP.setBounds(325, 152, 275, 29);
		Connectivity.add(Field_IP);
		Field_IP.setColumns(10);
		
		Field_uName = new JTextField(UserName);
		Field_uName.setColumns(10);
		Field_uName.setBounds(325, 210, 275, 29);
		Connectivity.add(Field_uName);
		
		Field_pWord = new JPasswordField(PassWord);
		Field_pWord.setBounds(325, 258, 275, 33);
		Connectivity.add(Field_pWord);
		
		JButton saveBTN = new JButton("Save");
		saveBTN.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				String IPAddress = Field_IP.getText();
				String UserName = Field_uName.getText();
				String PassWord = Field_pWord.getText();
				
				if(IPAddress.equals("") || UserName.equals("") || PassWord.equals("")) {
					JOptionPane.showMessageDialog(null, "Incomplete Credentials","Invalid Input",JOptionPane.WARNING_MESSAGE);
				}
				else {
					try {
						FileWriter fstream = new FileWriter("dbconfig.cfg");
						BufferedWriter info = new BufferedWriter(fstream);
			            	info.write(IPAddress);
			            	info.newLine();
			            	info.write(UserName);
			            	info.newLine();
			            	info.write(PassWord);
			            	info.close();
			            	JOptionPane.showMessageDialog(null, "Configuration Updated!");
					} 
					catch (Exception e1) {
						System.out.println(e1);
						System.out.println("A write error has occurred");
					}
				}
			}
		});
		saveBTN.setFont(new Font("Baskerville Old Face", Font.PLAIN, 17));
		saveBTN.setBounds(325, 340, 120, 33);
		Connectivity.add(saveBTN);
		
		JButton cancelBTN = new JButton("Cancel");
		cancelBTN.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Field_IP.setText("");
				Field_uName.setText("");
				Field_pWord.setText("");
			}
		});
		cancelBTN.setFont(new Font("Baskerville Old Face", Font.PLAIN, 17));
		cancelBTN.setBounds(480, 340, 120, 33);
		Connectivity.add(cancelBTN);
		
		JTextArea lblDescription_mysqlDB = new JTextArea();
		lblDescription_mysqlDB.setEditable(false);
		lblDescription_mysqlDB.setOpaque(false);
		lblDescription_mysqlDB.setFont(new Font("Baskerville Old Face", Font.PLAIN, 16));
		lblDescription_mysqlDB.setText("The configuration of host IP Address as well as the Username and Password credentials ensures that the system is always \r\nconnected to the source Database.");
		lblDescription_mysqlDB.setBounds(47, 62, 785, 40);
		Connectivity.add(lblDescription_mysqlDB);
		
		JTextArea lblDescription_Warning = new JTextArea();
		lblDescription_Warning.setText("Warning: Altering this section without the any copy/backup of the credentials will result in inaccessibility to the database.");
		lblDescription_Warning.setOpaque(false);
		lblDescription_Warning.setFont(new Font("Segoe UI Light", Font.PLAIN, 12));
		lblDescription_Warning.setEditable(false);
		lblDescription_Warning.setBounds(270, 429, 595, 20);
		Connectivity.add(lblDescription_Warning);
		
		JPanel factoryReset = new JPanel();
		settingTabs.addTab("Factory Reset", null, factoryReset, null);
		factoryReset.setLayout(null);
		
		JTextArea lbl_WarningReset_1 = new JTextArea();
		lbl_WarningReset_1.setText("Warning: Resetting the systems mean deleting all data including credentials, records and history logs.");
		lbl_WarningReset_1.setOpaque(false);
		lbl_WarningReset_1.setFont(new Font("Segoe UI Light", Font.PLAIN, 12));
		lbl_WarningReset_1.setEditable(false);
		lbl_WarningReset_1.setBounds(359, 430, 506, 20);
		factoryReset.add(lbl_WarningReset_1);
		
		JLabel lbl_factoryReset = new JLabel("Factory Reset");
		lbl_factoryReset.setFont(new Font("Baskerville Old Face", Font.PLAIN, 23));
		lbl_factoryReset.setBounds(10, 11, 124, 40);
		factoryReset.add(lbl_factoryReset);
		
		JTextArea lblDescription_mysqlDB_1 = new JTextArea();
		lblDescription_mysqlDB_1.setText("Delete all database contents (this includes student information, violation records and user accounts.) It is intended for \r\nsemestral/academic year reset.");
		lblDescription_mysqlDB_1.setOpaque(false);
		lblDescription_mysqlDB_1.setFont(new Font("Baskerville Old Face", Font.PLAIN, 16));
		lblDescription_mysqlDB_1.setEditable(false);
		lblDescription_mysqlDB_1.setBounds(47, 62, 785, 40);
		factoryReset.add(lblDescription_mysqlDB_1);
		
		btn_Reset = new JButton("Reset");
		btn_Reset.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				authenticatorReset.setVisible(true);
				btn_Reset.setVisible(false);
			}
		});
		btn_Reset.setFont(new Font("Baskerville Old Face", Font.PLAIN, 17));
		btn_Reset.setBounds(335, 202, 177, 57);
		factoryReset.add(btn_Reset);
		
		authenticatorReset = new JPanel();
		authenticatorReset.setVisible(false);
		authenticatorReset.setLayout(null);
		authenticatorReset.setBounds(0, 103, 875, 358);
		factoryReset.add(authenticatorReset);
		
		JLabel lblNewLabel_2 = new JLabel("Username:");
		lblNewLabel_2.setFont(new Font("Baskerville Old Face", Font.PLAIN, 18));
		lblNewLabel_2.setBounds(275, 120, 78, 33);
		authenticatorReset.add(lblNewLabel_2);
		
		JLabel lblPassword_1 = new JLabel("Password:");
		lblPassword_1.setFont(new Font("Baskerville Old Face", Font.PLAIN, 18));
		lblPassword_1.setBounds(284, 173, 69, 33);
		authenticatorReset.add(lblPassword_1);
		
		userTextField = new JTextField();
		userTextField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_ENTER) {
					resetAuth();
				}
			}
		});
		userTextField.setColumns(10);
		userTextField.setBounds(363, 120, 210, 33);
		authenticatorReset.add(userTextField);
		
		passwordField = new JPasswordField();
		passwordField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_ENTER) {
					resetAuth();
				}
			}
		});
		passwordField.setBounds(363, 174, 210, 33);
		authenticatorReset.add(passwordField);
		
		JButton btnNewButton_1 = new JButton("Authenticate");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				resetAuth();
			}
		});
		btnNewButton_1.setFont(new Font("Baskerville Old Face", Font.PLAIN, 16));
		btnNewButton_1.setBounds(379, 245, 125, 45);
		authenticatorReset.add(btnNewButton_1);
		
		JLabel lblNewLabel_1_2 = new JLabel("Authentication Required");
		lblNewLabel_1_2.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_1_2.setFont(new Font("Baskerville Old Face", Font.PLAIN, 20));
		lblNewLabel_1_2.setBounds(10, 28, 855, 20);
		authenticatorReset.add(lblNewLabel_1_2);
		
		JLabel lblNewLabel_1_1_1 = new JLabel("Sign in with root credentials in order to reset the system.");
		lblNewLabel_1_1_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_1_1_1.setFont(new Font("Baskerville Old Face", Font.PLAIN, 16));
		lblNewLabel_1_1_1.setBounds(10, 50, 855, 20);
		authenticatorReset.add(lblNewLabel_1_1_1);
		
		JTextArea lbl_WarningReset = new JTextArea();
		lbl_WarningReset.setText("Warning: Resetting the systems mean deleting all data including credentials, records and history logs.");
		lbl_WarningReset.setOpaque(false);
		lbl_WarningReset.setFont(new Font("Segoe UI Light", Font.PLAIN, 12));
		lbl_WarningReset.setEditable(false);
		lbl_WarningReset.setBounds(359, 327, 506, 20);
		authenticatorReset.add(lbl_WarningReset);
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.setBounds(0, 0, 1254, 22);
		contentPane.add(menuBar);
		
		JMenu fileMenu = new JMenu("File");
		menuBar.add(fileMenu);
		
		JMenuItem logoutItem = new JMenuItem("Logout");
		logoutItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mainFrame frame = new mainFrame();
				frame.logoutFunction();
				dispose();
			}
		});
		fileMenu.add(logoutItem);
		
		JMenuItem exitItem = new JMenuItem("Exit");
		exitItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		
		JMenuItem RestartItem = new JMenuItem("Restart");
		RestartItem.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				dispose();
				splashScreen frame = new splashScreen();
				frame.setLocationRelativeTo(null);
				frame.setVisible(true);
			}
		});
		fileMenu.add(RestartItem);
		fileMenu.add(exitItem);
		
		JMenu navMenu = new JMenu("Navigate");
		menuBar.add(navMenu);
		
		JMenuItem mainFrameItem = new JMenuItem("Home");
		mainFrameItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
				mainFrame frame = new mainFrame();
				frame.start();
				frame.setVisible(true);
				frame.showStats();
				frame.displayName();
			}
		});
		navMenu.add(mainFrameItem);
		
		JMenuItem stdmgmtItem = new JMenuItem("Student Management");
		stdmgmtItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
				StudentManagementFrame frame = new StudentManagementFrame();
				frame.setVisible(true);
			}
		});
		navMenu.add(stdmgmtItem);
		
		JMenuItem violationRecordItem = new JMenuItem("Violation Record");
		violationRecordItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
				ViolationRecordFrame frame = new ViolationRecordFrame();
				frame.setVisible(true);
			}
		});
		
		JMenuItem suspensionLogItem = new JMenuItem("Suspension Logs");
		suspensionLogItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SuspendedStudentsFrame frame = new SuspendedStudentsFrame();
				frame.setVisible(true);
				dispose();
			}
		});
		navMenu.add(suspensionLogItem);
		navMenu.add(violationRecordItem);
		
		JMenu helpMenu = new JMenu("Help");
		menuBar.add(helpMenu);
		
		JMenuItem tutorialItem = new JMenuItem("Tutorials");
		tutorialItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mainFrame frame = new mainFrame();
				frame.loadDocu();
			}
		});
		helpMenu.add(tutorialItem);
		
		JMenuItem aboutItem = new JMenuItem("About");
		aboutItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				aboutFrame frame = new aboutFrame();
				frame.setLocationRelativeTo(null);
				frame.setVisible(true);
			}
		});
		helpMenu.add(aboutItem);
		btn_back.setBounds(1114, 652, 130, 38);
		contentPane.add(btn_back);
		
		JLabel Background = new JLabel("");
		Background.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				accountTable.clearSelection();
				btn_EditUser.setEnabled(false);
				btn_DeleteUser.setEnabled(false);
				btn_DeleteHistory.setEnabled(false);
				actionLogTable.clearSelection();
			}
		});
		Background.setIcon(new ImageIcon(PreferencesFrame.class.getResource("/imgSource/Background.jpg")));
		Background.setBounds(0, 0, 1254, 701);
		contentPane.add(Background);
	}
	
	public void ShowSQLDataUserMgmt() {
		try {
			String query = "SELECT `id`, `Username`, `name` FROM studentinfo.credentialsdb;";
			PreparedStatement pst = db.con().prepareStatement(query);
			ResultSet rs = pst.executeQuery(query);
			accountTable.setModel(DbUtils.resultSetToTableModel(rs));
			
			db.con().close();
			} catch (Exception e1) {
		}
	}
	
	public void ShowSQLDataActivityLog() {
		try {
			String query = "SELECT `Name`, `Action`, `Details`, `Date and Time` FROM studentinfo.activitylogdb;";
			PreparedStatement pst = db.con().prepareStatement(query);
			ResultSet rs = pst.executeQuery(query);
			actionLogTable.setModel(DbUtils.resultSetToTableModel(rs));
			
			db.con().close();
			} catch (Exception e1) {
		}
	}
	
	public void DeleteSQLDataActivityLog() {
		int row = actionLogTable.getSelectedRow();
		String DateSelect = (actionLogTable.getModel().getValueAt(row, 3).toString());
		
			try {			
				String query = "DELETE FROM `studentinfo`.`activitylogdb` WHERE `Date and Time` = '"+ DateSelect +"';";
				PreparedStatement pst = db.con().prepareStatement(query);
				pst.execute();
				
				db.con().close();
				JOptionPane.showMessageDialog(null, "Action Log Deleted Successfully!", "Process Completed", JOptionPane.INFORMATION_MESSAGE);
				btn_DeleteHistory.setEnabled(false);
				ShowSQLDataActivityLog();
			}
			catch (Exception e){
		}
	}
	
	public void ClearSQLDataActivityLog() {
		
		try {			
			String query = "DELETE FROM `studentinfo`.`activitylogdb`;";
			PreparedStatement pst = db.con().prepareStatement(query);
			pst.execute();
			
			db.con().close();
			JOptionPane.showMessageDialog(null, "Action Log Cleared Successfully!", "Process Completed", JOptionPane.INFORMATION_MESSAGE);
			ShowSQLDataActivityLog();
		}
		catch (Exception e){
		}
	}
	
	public void DeleteSQLData() {
		try {			
			String query = "DELETE FROM `studentinfo`.`credentialsdb` WHERE `id` = '"+ id +"';";
			PreparedStatement pst = db.con().prepareStatement(query);
			pst.execute();
			
			db.con().close();
			ShowSQLDataUserMgmt();
		}
		catch (Exception e){
		}
	}
	
	public void DetectionMode() {
		try {
            String query = "UPDATE `studentinfo`.`status_set` SET `status` = '1' WHERE (`status` = '0')  or (`status` = '2');";
            PreparedStatement pst = db.con().prepareStatement(query);
            pst.executeUpdate();
            
            db.con().close();
			tglbtn_workingMode.setText("Detection Mode");
			uAT.detectMode();
        } catch (Exception e) {
            // TODO: handle exception
        }
	}
	
	public void IdleMode() {
		try {
			String query = "UPDATE `studentinfo`.`status_set` SET `status` = '0' WHERE (`status` = '1') or (`status` = '2');";
            PreparedStatement pst = db.con().prepareStatement(query);
            pst.executeUpdate();
            
            db.con().close();
			tglbtn_workingMode.setText("Idle Mode");
			uAT.idleMode();
        } catch (Exception e) {
            // TODO: handle exception
        }
	}
	
	public void loadStatus() {
		try {
            String query = "SELECT * from studentinfo.status_set";
            PreparedStatement pst = db.con().prepareStatement(query);
            ResultSet rs = pst.executeQuery();
            
            if (rs.next()) {
                statusSet = rs.getInt(1);
                if(statusSet == 1) {
                    tglbtn_workingMode.setSelected(true);
                    tglbtn_workingMode.setText("Detection Mode");
            		lbl_Status.setIcon(new ImageIcon(PreferencesFrame.class.getResource("/imgSource/DetectionMode.png")));
                    
                }
                if(statusSet == 0) {
                    tglbtn_workingMode.setSelected(false);
                	tglbtn_workingMode.setText("Idle Mode");
            		lbl_Status.setIcon(new ImageIcon(PreferencesFrame.class.getResource("/imgSource/IdleMode.png")));
                }
                if(statusSet == 2) {
                	tglbtn_workingMode.setSelected(false);
                	tglbtn_workingMode.setText("Camera Error");
            		lbl_Status.setIcon(new ImageIcon(PreferencesFrame.class.getResource("/imgSource/CamError.png")));
                }
            }
            
            db.con().close();
        } catch (Exception e) {
            // TODO: handle exception
        }
	}
	
	public void dbconfigLoad() {
		//Load Current DBConfig//
		IPAddress = "";
		UserName = "";
		PassWord = "";
		
		try {
	        FileReader fstream = new FileReader("dbconfig.cfg");
	        BufferedReader info = new BufferedReader(fstream);
	           IPAddress = info.readLine();
	           UserName = info.readLine();
	           PassWord = info.readLine();
	        info.close();
	    } 
		catch (Exception e1) {
			System.out.println(e1);
	        System.out.println("An error has occurred");
	    }
	}
	
	public void FactoryReset() {
		try {
            String delQuery1 = "DELETE FROM `studentinfo`.`activitylogdb`;";
            String delQuery2 = "DELETE FROM `studentinfo`.`credentialsdb`;";
            String delQuery3 = "DELETE FROM `studentinfo`.`studentdb`;";
            String delQuery4 = "DELETE FROM `studentinfo`.`violationdb`;";
            PreparedStatement pst1 = db.con().prepareStatement(delQuery1);
            PreparedStatement pst2 = db.con().prepareStatement(delQuery2);
            PreparedStatement pst3 = db.con().prepareStatement(delQuery3);
            PreparedStatement pst4 = db.con().prepareStatement(delQuery4);
            pst1.execute();
            pst2.execute();
            pst3.execute();
            pst4.execute();
            JOptionPane.showMessageDialog(null, "Factory Reset Completed!", "Process Completed", JOptionPane.INFORMATION_MESSAGE);
            
            db.con().close();
        } catch (Exception e) {
            System.out.println(e);
        }
	}
	
	public void userMgmtAuth() {
		String uName = userField.getText();
		String passW = passField.getText();
		
		if((uName.equals("")) || (passW.equals(""))) {
			JOptionPane.showMessageDialog(null, "Incomplete credentials!", "Warning", JOptionPane.WARNING_MESSAGE);
		}
		else if (uName.equals(uAT.rootName()) && (passW.equals(uAT.rootPass()))) {
			JOptionPane.showMessageDialog(null, "Access Granted");
			authenticatorUserMgmt.setVisible(false);
			userManagement.setVisible(true);
			ShowSQLDataUserMgmt();
			
		}
		else {
			JOptionPane.showMessageDialog(null, "Invalid credentials!", "Error", JOptionPane.ERROR_MESSAGE);
		}
	}
	
	public void resetAuth() {

		String uName = userTextField.getText();
		String passW = passwordField.getText();
		
		if((uName.equals("")) || (passW.equals(""))) {
			JOptionPane.showMessageDialog(null, "Incomplete credentials!", "Warning", JOptionPane.WARNING_MESSAGE);
		}
		else if (uName.equals(uAT.rootName()) && (passW.equals(uAT.rootPass()))) {
			int response = JOptionPane.showConfirmDialog(null, "Are you sure you want to erase all the data within the system? This cannot be undone", "Warning!", JOptionPane.YES_OPTION, JOptionPane.NO_OPTION);
			if(response == JOptionPane.YES_OPTION) {
				
				FactoryReset();
				
				JOptionPane.showMessageDialog(null, "Process Complete! The system will now restart");
				dispose();
				
				splashScreen frame = new splashScreen();
				frame.setVisible(true);
				
			}
			else if(response == JOptionPane.NO_OPTION) {
				userTextField.setText("");
				passwordField.setText("");
			}
			
		}
		else {
			JOptionPane.showMessageDialog(null, "Invalid credentials!", "Error", JOptionPane.ERROR_MESSAGE);
		}
	}
	
	public void authActLog() {

		String uName = txtField_uName.getText();
		String passW = pf_password.getText();
		
		if((uName.equals("")) || (passW.equals(""))) {
			JOptionPane.showMessageDialog(null, "Incomplete credentials!", "Warning", JOptionPane.WARNING_MESSAGE);
		}
		else if (uName.equals(uAT.rootName()) && (passW.equals(uAT.rootPass()))) {
			JOptionPane.showMessageDialog(null, "Access Granted");
			authenticatorActlog.setVisible(false);
			actlogOptions.setVisible(true);
			lbl_statusIndicator.setText("Editor Mode");
			
		}
		else {
			JOptionPane.showMessageDialog(null, "Invalid credentials!", "Error", JOptionPane.ERROR_MESSAGE);
		}
	}
	
	private void OrganizeAutoIncrement() {
		try {
			String query = "ALTER TABLE studentinfo.activitylogdb DROP COLUMN `#`;";
			PreparedStatement pst = db.con().prepareStatement(query);
			pst.execute();
			
			db.con().close();
		}catch (Exception e3){
			JOptionPane.showMessageDialog(null, "Error: " + e3);
		}
	
		try {
			String query = "ALTER TABLE studentinfo.activitylogdb ADD `#` INT PRIMARY KEY NOT NULL AUTO_INCREMENT FIRST;";
			PreparedStatement pst = db.con().prepareStatement(query);
			pst.execute();
			
			db.con().close();
		}catch (Exception e1){
			JOptionPane.showMessageDialog(null, "Error: " + e1);
		}
	
		try {
			String query = "SELECT * FROM studentinfo.activitylogdb;";
			PreparedStatement pst = db.con().prepareStatement(query);
			pst.execute();
			
			db.con().close();
			ShowSQLDataActivityLog();
		}catch (Exception e2){
			JOptionPane.showMessageDialog(null, "Error: " + e2);
		}
		
	}
}