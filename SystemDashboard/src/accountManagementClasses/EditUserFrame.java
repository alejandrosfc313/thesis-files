package accountManagementClasses;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import DatabaseClasses.DBConnection;
import WindowFrameClasses.PreferencesFrame;
import it.sauronsoftware.junique.AlreadyLockedException;
import it.sauronsoftware.junique.JUnique;
import javax.swing.border.LineBorder;
import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class EditUserFrame extends JFrame {

	PreferencesFrame frame = new PreferencesFrame();
	DBConnection db = new DBConnection();
	private JPanel contentPane;
	private JTextField textField_employeeID;
	private JTextField textField_name;
	private JTextField textField_userName;
	private JPasswordField passwordField;
	private JPasswordField passwordField_verify;
	private JButton btn_Edit;
	private JTextField securityAnswer1;
	private JTextField securityAnswer2;
	private JComboBox securityQuestion1;
	private JComboBox securityQuestion2;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				String appId = "myapplicationid";
				boolean alreadyRunning;
				try {
					JUnique.acquireLock(appId);
					alreadyRunning = false;
				} catch (AlreadyLockedException e) {
					alreadyRunning = true;
					JOptionPane.showMessageDialog(null, "Application is already running!");
				}
				if (!alreadyRunning) {
					// Start sequence here
					EditUserFrame frame = new EditUserFrame();
					frame.setVisible(true);
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public EditUserFrame() {
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowActivated(WindowEvent e) {
				dataGrabber();
			}
		});
		setResizable(false);
		setTitle("Create an Account");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 610, 600);
		setLocationRelativeTo(null);
		setUndecorated(false);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		btn_Edit = new JButton("");
		btn_Edit.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				btn_Edit.setIcon(new ImageIcon(EditUserFrame.class.getResource("/imgSource/SaveBtn.png")));
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btn_Edit.setIcon(new ImageIcon(EditUserFrame.class.getResource("/imgSource/SaveBtn.png")));
			}
			@Override
			public void mouseEntered(MouseEvent e) {
				btn_Edit.setIcon(new ImageIcon(EditUserFrame.class.getResource("/imgSource/SaveBtn_hover.png")));
			}
			@Override
			public void mousePressed(MouseEvent e) {
				btn_Edit.setIcon(new ImageIcon(EditUserFrame.class.getResource("/imgSource/SaveBtn_clicked.png")));
			}
		});
		btn_Edit.setIcon(new ImageIcon(EditUserFrame.class.getResource("/imgSource/SaveBtn.png")));
		btn_Edit.setBorder(new LineBorder(new Color(0, 0, 0)));
		btn_Edit.setFont(new Font("Baskerville Old Face", Font.PLAIN, 18));
		btn_Edit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				String empID = textField_employeeID.getText();
				String name = textField_name.getText();
				String uName = textField_userName.getText();
				String firstPass = passwordField.getText();
				String confirmPass = passwordField_verify.getText();
				String securityQ1 = securityQuestion1.getSelectedItem().toString();
				String securityA1 = securityAnswer1.getText();
				String securityQ2 = securityQuestion2.getSelectedItem().toString();
				String securityA2 = securityAnswer2.getText();
					
				if (securityQ1.equals("-SELECT SECURITY QUESTION-") || securityQ2.equals("-SELECT SECURITY QUESTION-")) {
					JOptionPane.showMessageDialog(null, "Please select a question", "Error", JOptionPane.WARNING_MESSAGE);
				}
				else if (firstPass.equals("") || confirmPass.equals("")) {
					JOptionPane.showMessageDialog(null, "Incomplete Password!", "Error", JOptionPane.ERROR_MESSAGE);
				}
				else if (!firstPass.equals(confirmPass)) {
					JOptionPane.showMessageDialog(null, "Password Mismatch!", "Error", JOptionPane.ERROR_MESSAGE);
				}
				else if (securityA1.equals("") || securityA2.equals("")) {
					JOptionPane.showMessageDialog(null, "Answer the following questions", "Error", JOptionPane.ERROR_MESSAGE);
				}
				else {
					try {						
						String query = "UPDATE `studentinfo`.`credentialsdb` SET `Username` =?, `Password` =?, `id` =?, `name` =?, `security question 1` =?, "
								+ "`security answer 1` =?, `security question 2` =?, `security answer 2` =?;";
						
						PreparedStatement pst = db.con().prepareStatement(query);
						pst.setString(1, uName);
						pst.setString(2, confirmPass);
						pst.setString(3, empID);
						pst.setString(4, name);
						pst.setString(5, securityQ1);
						pst.setString(6, securityA1);
						pst.setString(7, securityQ2);
						pst.setString(8, securityA2);
						pst.executeUpdate();
						
						JOptionPane.showMessageDialog(null, "Credentials Updated Successfully!");
						dispose();
						
						frame.setVisible(true);
						frame.ShowSQLDataUserMgmt();
						frame.settingTabs.setSelectedIndex(1);
						
						dispose();
						db.con().close();
					}catch (Exception e1) {
						JOptionPane.showMessageDialog(null, e1, "Connection Error", JOptionPane.WARNING_MESSAGE);
					}
				}
			}
		});
		
		JButton btn_back = new JButton("");
		btn_back.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				btn_back.setIcon(new ImageIcon(EditUserFrame.class.getResource("/imgSource/BackBtn.png")));
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btn_back.setIcon(new ImageIcon(EditUserFrame.class.getResource("/imgSource/BackBtn.png")));
			}
			@Override
			public void mouseEntered(MouseEvent e) {
				btn_back.setIcon(new ImageIcon(EditUserFrame.class.getResource("/imgSource/BackBtn_hover.png")));
			}
			@Override
			public void mousePressed(MouseEvent e) {
				btn_back.setIcon(new ImageIcon(EditUserFrame.class.getResource("/imgSource/BackBtn_clicked.png")));
			}
		});
		btn_back.setIcon(new ImageIcon(EditUserFrame.class.getResource("/imgSource/BackBtn.png")));
		btn_back.setBorder(new LineBorder(new Color(0, 0, 0)));
		btn_back.setFont(new Font("Baskerville Old Face", Font.PLAIN, 18));
		btn_back.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
				PreferencesFrame frame = new PreferencesFrame();
				frame.setVisible(true);
				frame.ShowSQLDataUserMgmt();
				frame.settingTabs.setSelectedIndex(1);
			}
		});
		btn_back.setBounds(304, 489, 150, 45);
		contentPane.add(btn_back);
		
		securityAnswer1 = new JTextField();
		securityAnswer1.setBounds(183, 338, 338, 30);
		contentPane.add(securityAnswer1);
		securityAnswer1.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_ENTER) {
					btn_Edit.doClick();
				}
			}
		});
		securityAnswer1.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 11));
		securityAnswer1.setColumns(10);
		
		securityAnswer2 = new JTextField();
		securityAnswer2.setBounds(183, 421, 338, 30);
		contentPane.add(securityAnswer2);
		securityAnswer2.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_ENTER) {
					btn_Edit.doClick();
				}
			}
		});
		securityAnswer2.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 11));
		securityAnswer2.setColumns(10);
		
		JLabel lbl_createAccount = new JLabel("Edit Account Information");
		lbl_createAccount.setHorizontalAlignment(SwingConstants.CENTER);
		lbl_createAccount.setFont(new Font("Baskerville Old Face", Font.PLAIN, 35));
		lbl_createAccount.setBounds(0, 11, 594, 45);
		contentPane.add(lbl_createAccount);
		
		JLabel lbl_securityQuestion2 = new JLabel("Security Question 2");
		lbl_securityQuestion2.setBounds(41, 390, 132, 21);
		contentPane.add(lbl_securityQuestion2);
		lbl_securityQuestion2.setFont(new Font("Baskerville Old Face", Font.PLAIN, 16));
		
		JLabel lbl_securityQuestion1 = new JLabel("Security Question 1");
		lbl_securityQuestion1.setBounds(43, 306, 130, 21);
		contentPane.add(lbl_securityQuestion1);
		lbl_securityQuestion1.setFont(new Font("Baskerville Old Face", Font.PLAIN, 16));
		
		JLabel lbl_securityAnswer1 = new JLabel("Security Answer 1");
		lbl_securityAnswer1.setBounds(53, 347, 120, 21);
		contentPane.add(lbl_securityAnswer1);
		lbl_securityAnswer1.setFont(new Font("Baskerville Old Face", Font.PLAIN, 16));
		
		JLabel lbl_securityAnswer2 = new JLabel("Security Answer 2");
		lbl_securityAnswer2.setBounds(51, 430, 122, 21);
		contentPane.add(lbl_securityAnswer2);
		lbl_securityAnswer2.setFont(new Font("Baskerville Old Face", Font.PLAIN, 16));
		
		securityQuestion1 = new JComboBox();
		securityQuestion1.setBounds(183, 297, 338, 30);
		contentPane.add(securityQuestion1);
		securityQuestion1.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 11));
		securityQuestion1.setModel(new DefaultComboBoxModel(new String[] {"-SELECT SECURITY QUESTION-", "What is the name of the first school you attended?", "What is you mother's maiden name?", "What is your first pet's name?", "What city were you born in?", "What is your favorite movie?"}));
		
		securityQuestion2 = new JComboBox();
		securityQuestion2.setBounds(183, 379, 338, 31);
		contentPane.add(securityQuestion2);
		securityQuestion2.setModel(new DefaultComboBoxModel(new String[] {"-SELECT SECURITY QUESTION-", "In what city or town did your mother and father meet?", "What is the name of your childhood friend?", "In what city or town was your first job?", "What was your childhood nickname?", "What is your favorite song?"}));
		securityQuestion2.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 11));
		btn_Edit.setBounds(145, 489, 150, 45);
		contentPane.add(btn_Edit);
		
		JLabel lbl_confirmPassword = new JLabel("Confirm Password");
		lbl_confirmPassword.setBounds(50, 259, 123, 21);
		contentPane.add(lbl_confirmPassword);
		lbl_confirmPassword.setFont(new Font("Baskerville Old Face", Font.PLAIN, 16));
		
		passwordField_verify = new JPasswordField();
		passwordField_verify.setBounds(183, 256, 338, 30);
		contentPane.add(passwordField_verify);
		passwordField_verify.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 11));
		
		passwordField = new JPasswordField();
		passwordField.setBounds(183, 215, 338, 30);
		contentPane.add(passwordField);
		passwordField.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 11));
		
		JLabel lbl_Password = new JLabel("Password");
		lbl_Password.setBounds(109, 218, 64, 21);
		contentPane.add(lbl_Password);
		lbl_Password.setFont(new Font("Baskerville Old Face", Font.PLAIN, 16));
		
		textField_userName = new JTextField();
		textField_userName.setBounds(183, 174, 338, 30);
		contentPane.add(textField_userName);
		textField_userName.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 11));
		textField_userName.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_ENTER) {
					btn_Edit.doClick();
				}
			}
		});
		textField_userName.setColumns(10);
		
		JLabel lbl_userName = new JLabel("Username");
		lbl_userName.setBounds(104, 177, 69, 21);
		contentPane.add(lbl_userName);
		lbl_userName.setFont(new Font("Baskerville Old Face", Font.PLAIN, 16));
		
		textField_name = new JTextField();
		textField_name.setBounds(183, 133, 338, 30);
		contentPane.add(textField_name);
		textField_name.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 11));
		textField_name.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_ENTER) {
					btn_Edit.doClick();
				}
			}
		});
		textField_name.setToolTipText("e.g. Juan A. Dela Cruz Jr.");
		textField_name.setColumns(10);
		
		JLabel lbl_name = new JLabel("Name");
		lbl_name.setBounds(132, 136, 41, 21);
		contentPane.add(lbl_name);
		lbl_name.setFont(new Font("Baskerville Old Face", Font.PLAIN, 16));
		
		textField_employeeID = new JTextField();
		textField_employeeID.setBounds(183, 92, 338, 30);
		contentPane.add(textField_employeeID);
		textField_employeeID.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 11));
		textField_employeeID.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_ENTER) {
					btn_Edit.doClick();
				}
			}
		});
		textField_employeeID.setColumns(10);
		
		JLabel lbl_id = new JLabel("Employee ID");
		lbl_id.setBounds(87, 95, 86, 21);
		contentPane.add(lbl_id);
		lbl_id.setFont(new Font("Baskerville Old Face", Font.PLAIN, 16));
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon(EditUserFrame.class.getResource("/imgSource/EditAccountBG.jpg")));
		lblNewLabel.setBounds(0, 0, 594, 561);
		contentPane.add(lblNewLabel);
		passwordField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_ENTER) {
					btn_Edit.doClick();
				}
			}
		});
		passwordField_verify.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_ENTER) {
					btn_Edit.doClick();
				}
			}
		});
	}
	
	public void dataGrabber(){
		try {
			PreferencesFrame frame = new PreferencesFrame();
			textField_employeeID.setText(frame.id);
			String query = "SELECT * FROM studentinfo.credentialsdb WHERE `id` = '"+ textField_employeeID.getText() +"';";
			PreparedStatement pst = db.con().prepareStatement(query);
			ResultSet rs = pst.executeQuery(query);
				
			while(rs.next()) {
				textField_userName.setText(rs.getString(1));
				textField_employeeID.setText(rs.getString(3));
				textField_name.setText(rs.getString(4));
				securityQuestion1.setSelectedItem(rs.getString(5));
				securityAnswer1.setText(rs.getString(6));
				securityQuestion2.setSelectedItem(rs.getString(7));
				securityAnswer2.setText(rs.getString(8));
			}
			db.con().close();
		} catch (Exception e1) {
			JOptionPane.showMessageDialog(null, "Error: " + e1);
		}
	}
}
