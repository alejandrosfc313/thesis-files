package accountManagementClasses;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.PreparedStatement;

import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import DatabaseClasses.DBConnection;
import WindowFrameClasses.PreferencesFrame;
import it.sauronsoftware.junique.AlreadyLockedException;
import it.sauronsoftware.junique.JUnique;
import javax.swing.border.LineBorder;
import java.awt.Color;

public class CreateAccountFrame extends JFrame {


	PreferencesFrame frame = new PreferencesFrame();
	DBConnection db = new DBConnection();
	private JPanel contentPane;
	private JTextField textField_employeeID;
	private JTextField textField_name;
	private JTextField textField_userName;
	private JPasswordField passwordField;
	private JPasswordField passwordField_verify;
	private JButton btn_Register;
	private JTextField securityAnswer1;
	private JTextField securityAnswer2;
	private JComboBox securityQuestion1;
	private JComboBox securityQuestion2;
	private JPanel panel_securityQ;
	private JPanel panel_infoForm;
	private JButton btn_Next;
	private JButton btn_Back;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				String appId = "myapplicationid";
				boolean alreadyRunning;
				try {
					JUnique.acquireLock(appId);
					alreadyRunning = false;
				} catch (AlreadyLockedException e) {
					alreadyRunning = true;
					JOptionPane.showMessageDialog(null, "Application is already running!");
				}
				if (!alreadyRunning) {
					// Start sequence here
					CreateAccountFrame frame = new CreateAccountFrame();
					frame.setVisible(true);
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public CreateAccountFrame() {
		setResizable(false);
		setTitle("Violation Monitoring Dashboard - Create an Account");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 540, 500);
		setLocationRelativeTo(null);
		setUndecorated(false);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		panel_securityQ = new JPanel();
		panel_securityQ.setOpaque(false);
		panel_securityQ.setVisible(false);
		
		btn_Next = new JButton("");
		btn_Next.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseExited(MouseEvent e) {
				btn_Next.setIcon(new ImageIcon(CreateAccountFrame.class.getResource("/imgSource/NextBtn.png")));
			}
			@Override
			public void mouseReleased(MouseEvent e) {
				btn_Next.setIcon(new ImageIcon(CreateAccountFrame.class.getResource("/imgSource/NextBtn.png")));
			}
			@Override
			public void mouseEntered(MouseEvent e) {
				btn_Next.setIcon(new ImageIcon(CreateAccountFrame.class.getResource("/imgSource/NextBtn_hover.png")));
			}
			@Override
			public void mousePressed(MouseEvent e) {
				btn_Next.setIcon(new ImageIcon(CreateAccountFrame.class.getResource("/imgSource/NextBtn_clicked.png")));
			}
		});
		btn_Next.setBorder(new LineBorder(new Color(0, 0, 0)));
		btn_Next.setIcon(new ImageIcon(CreateAccountFrame.class.getResource("/imgSource/NextBtn.png")));
		btn_Next.setFont(new Font("Baskerville Old Face", Font.PLAIN, 18));
		btn_Next.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent e) {
				
				String empID = textField_employeeID.getText();
				String name = textField_name.getText();
				String uName = textField_userName.getText();
				String firstPass = passwordField.getText();
				String confirmPass = passwordField_verify.getText();
				
				if(empID.equalsIgnoreCase("") || name.equalsIgnoreCase("") || uName.equalsIgnoreCase("") || firstPass.equalsIgnoreCase("")) {
					JOptionPane.showMessageDialog(null, "Incomplete credentials!", "Error", JOptionPane.ERROR_MESSAGE);
				}
				else {
					if(!firstPass.equals(confirmPass)) {
						JOptionPane.showMessageDialog(null, "Password Mismatch", "Error", JOptionPane.ERROR_MESSAGE);
				}
				else {
					panel_infoForm.setVisible(false);
					panel_securityQ.setVisible(true);
					
					btn_Next.setVisible(false);
					btn_Back.setVisible(true);
					btn_Register.setVisible(true);
				}
			}
		}
		});
		
		btn_Register = new JButton("");
		btn_Register.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseExited(MouseEvent e) {
				btn_Register.setIcon(new ImageIcon(CreateAccountFrame.class.getResource("/imgSource/RegisterBtn.png")));
			}
			@Override
			public void mouseReleased(MouseEvent e) {
				btn_Register.setIcon(new ImageIcon(CreateAccountFrame.class.getResource("/imgSource/RegisterBtn.png")));
			}
			@Override
			public void mousePressed(MouseEvent e) {
				btn_Register.setIcon(new ImageIcon(CreateAccountFrame.class.getResource("/imgSource/RegisterBtn_clicked.png")));
			}
			@Override
			public void mouseEntered(MouseEvent e) {
				btn_Register.setIcon(new ImageIcon(CreateAccountFrame.class.getResource("/imgSource/RegisterBtn_hover.png")));
			}
		});
		btn_Register.setBorder(new LineBorder(new Color(0, 0, 0)));
		btn_Register.setIcon(new ImageIcon(CreateAccountFrame.class.getResource("/imgSource/RegisterBtn.png")));
		btn_Register.setFont(new Font("Baskerville Old Face", Font.PLAIN, 18));
		btn_Register.setVisible(false);
		btn_Register.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				String empID = textField_employeeID.getText();
				String name = textField_name.getText();
				String uName = textField_userName.getText();
				String firstPass = passwordField.getText();
				String confirmPass = passwordField_verify.getText();
				String securityQ1 = securityQuestion1.getSelectedItem().toString();
				String securityA1 = securityAnswer1.getText();
				String securityQ2 = securityQuestion2.getSelectedItem().toString();
				String securityA2 = securityAnswer2.getText();
					
				if (securityQ1.equals("-SELECT SECURITY QUESTION-") || securityQ2.equals("-SELECT SECURITY QUESTION-")) {
					JOptionPane.showMessageDialog(null, "Please select a question", "Error", JOptionPane.ERROR_MESSAGE);
				}
				else if (securityA1.equals("") || securityA2.equals("")) {
					JOptionPane.showMessageDialog(null, "Answer the following questions", "Error", JOptionPane.ERROR_MESSAGE);
				}
				else {
					try {
						
						String query = "INSERT INTO `studentinfo`.`credentialsdb` "	+ "(`Username`, `Password`, `id`, `name`, `security question 1`, `security answer 1`, `security question 2`, `security answer 2`) "	
						+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?);";
					
						PreparedStatement ps = db.con().prepareStatement(query);
						ps.setString(1, uName);
						ps.setString(2, confirmPass);
						ps.setString(3, empID);
						ps.setString(4, name);
						ps.setString(5, securityQ1);
						ps.setString(6, securityA1);
						ps.setString(7, securityQ2);
						ps.setString(8, securityA2);
						ps.execute();
						db.con().close();
						
						JOptionPane.showMessageDialog(null, "Registration Success!", "Registered", JOptionPane.PLAIN_MESSAGE);
						dispose();
						
						frame.setVisible(true);
						frame.ShowSQLDataUserMgmt();
						frame.settingTabs.setSelectedIndex(1);
						
						dispose();
						db.con().close();
					}catch (Exception e1) {
						JOptionPane.showMessageDialog(null, e1, "Connection Error", JOptionPane.WARNING_MESSAGE);
					}
				}
			}
		});
		
		btn_Back = new JButton("");
		btn_Back.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseExited(MouseEvent e) {
				btn_Back.setIcon(new ImageIcon(CreateAccountFrame.class.getResource("/imgSource/BackBtn.png")));
			}
			@Override
			public void mouseReleased(MouseEvent e) {
				btn_Back.setIcon(new ImageIcon(CreateAccountFrame.class.getResource("/imgSource/BackBtn.png")));
			}
			@Override
			public void mouseEntered(MouseEvent e) {
				btn_Back.setIcon(new ImageIcon(CreateAccountFrame.class.getResource("/imgSource/BackBtn_hover.png")));
			}
			@Override
			public void mousePressed(MouseEvent e) {
				btn_Back.setIcon(new ImageIcon(CreateAccountFrame.class.getResource("/imgSource/BackBtn_clicked.png")));
			}
		});
		btn_Back.setBorder(new LineBorder(new Color(0, 0, 0)));
		btn_Back.setIcon(new ImageIcon(CreateAccountFrame.class.getResource("/imgSource/BackBtn.png")));
		btn_Back.setFont(new Font("Baskerville Old Face", Font.PLAIN, 18));
		btn_Back.setVisible(false);
		btn_Back.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panel_infoForm.setVisible(true);
				panel_securityQ.setVisible(false);
				
				btn_Next.setVisible(true);
				btn_Back.setVisible(false);
				btn_Register.setVisible(false);
			}
		});
		btn_Back.setBounds(277, 375, 150, 45);
		contentPane.add(btn_Back);
		btn_Register.setBounds(117, 375, 150, 45);
		contentPane.add(btn_Register);
		btn_Next.setBounds(170, 375, 180, 45);
		contentPane.add(btn_Next);
		
		panel_infoForm = new JPanel();
		panel_infoForm.setOpaque(false);
		panel_infoForm.setBounds(10, 67, 500, 285);
		contentPane.add(panel_infoForm);
		panel_infoForm.setLayout(null);
		
		JLabel lbl_id = new JLabel("Employee ID");
		lbl_id.setBounds(109, 78, 86, 21);
		panel_infoForm.add(lbl_id);
		lbl_id.setFont(new Font("Baskerville Old Face", Font.PLAIN, 16));
		
		JLabel lbl_name = new JLabel("Name");
		lbl_name.setBounds(156, 119, 39, 21);
		panel_infoForm.add(lbl_name);
		lbl_name.setFont(new Font("Baskerville Old Face", Font.PLAIN, 16));
		
		textField_employeeID = new JTextField();
		textField_employeeID.setBounds(205, 69, 210, 30);
		panel_infoForm.add(textField_employeeID);
		textField_employeeID.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 11));
		textField_employeeID.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_ENTER) {
					btn_Next.doClick();
				}
			}
		});
		textField_employeeID.setColumns(10);
		
		JLabel lbl_userName = new JLabel("Username");
		lbl_userName.setBounds(129, 160, 66, 21);
		panel_infoForm.add(lbl_userName);
		lbl_userName.setFont(new Font("Baskerville Old Face", Font.PLAIN, 16));
		
		textField_userName = new JTextField();
		textField_userName.setBounds(205, 151, 210, 30);
		panel_infoForm.add(textField_userName);
		textField_userName.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 11));
		textField_userName.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_ENTER) {
					btn_Register.doClick();
				}
			}
		});
		textField_userName.setColumns(10);
		
		JLabel lbl_Password = new JLabel("Password");
		lbl_Password.setBounds(132, 201, 63, 21);
		panel_infoForm.add(lbl_Password);
		lbl_Password.setFont(new Font("Baskerville Old Face", Font.PLAIN, 16));
		
		passwordField = new JPasswordField();
		passwordField.setBounds(205, 192, 210, 30);
		panel_infoForm.add(passwordField);
		passwordField.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 11));
		
		JLabel lbl_confirmPassword = new JLabel("Confirm Password");
		lbl_confirmPassword.setBounds(77, 242, 118, 21);
		panel_infoForm.add(lbl_confirmPassword);
		lbl_confirmPassword.setFont(new Font("Baskerville Old Face", Font.PLAIN, 16));
		
		passwordField_verify = new JPasswordField();
		passwordField_verify.setBounds(205, 233, 210, 30);
		panel_infoForm.add(passwordField_verify);
		passwordField_verify.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 11));
		
		textField_name = new JTextField();
		textField_name.setBounds(205, 110, 210, 30);
		panel_infoForm.add(textField_name);
		textField_name.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 11));
		textField_name.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_ENTER) {
					btn_Next.doClick();
				}
			}
		});
		textField_name.setToolTipText("e.g. Juan A. Dela Cruz Jr.");
		textField_name.setColumns(10);
		
		JLabel lbl_description = new JLabel("Complete the following forms.");
		lbl_description.setFont(new Font("Baskerville Old Face", Font.PLAIN, 16));
		lbl_description.setBounds(10, 11, 480, 21);
		panel_infoForm.add(lbl_description);
		passwordField_verify.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_ENTER) {
					btn_Next.doClick();
				}
			}
		});
		passwordField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_ENTER) {
					btn_Next.doClick();
				}
			}
		});
		panel_securityQ.setBounds(10, 67, 500, 285);
		contentPane.add(panel_securityQ);
		panel_securityQ.setLayout(null);
		
		JLabel lbl_securityAnswer2 = new JLabel("Security Answer 2");
		lbl_securityAnswer2.setBounds(22, 216, 120, 21);
		panel_securityQ.add(lbl_securityAnswer2);
		lbl_securityAnswer2.setFont(new Font("Baskerville Old Face", Font.PLAIN, 16));
		
		JLabel lbl_securityQuestion2 = new JLabel("Security Question 2");
		lbl_securityQuestion2.setBounds(12, 176, 130, 21);
		panel_securityQ.add(lbl_securityQuestion2);
		lbl_securityQuestion2.setFont(new Font("Baskerville Old Face", Font.PLAIN, 16));
		
		JLabel lbl_securityAnswer1 = new JLabel("Security Answer 1");
		lbl_securityAnswer1.setBounds(22, 133, 120, 21);
		panel_securityQ.add(lbl_securityAnswer1);
		lbl_securityAnswer1.setFont(new Font("Baskerville Old Face", Font.PLAIN, 16));
		
		JLabel lbl_securityQuestion1 = new JLabel("Security Question 1");
		lbl_securityQuestion1.setBounds(12, 92, 130, 21);
		panel_securityQ.add(lbl_securityQuestion1);
		lbl_securityQuestion1.setFont(new Font("Baskerville Old Face", Font.PLAIN, 16));
		
		securityQuestion1 = new JComboBox();
		securityQuestion1.setBounds(152, 84, 338, 30);
		panel_securityQ.add(securityQuestion1);
		securityQuestion1.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 11));
		securityQuestion1.setModel(new DefaultComboBoxModel(new String[] {"-SELECT SECURITY QUESTION-", "What is the name of the first school you attended?", "What is you mother's maiden name?", "What is your first pet's name?", "What city were you born in?", "What is your favorite movie?"}));
		
		securityAnswer1 = new JTextField();
		securityAnswer1.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_ENTER) {
					btn_Register.doClick();
				}
			}
		});
		securityAnswer1.setBounds(152, 125, 338, 30);
		panel_securityQ.add(securityAnswer1);
		securityAnswer1.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 11));
		securityAnswer1.setColumns(10);
		
		securityQuestion2 = new JComboBox();
		securityQuestion2.setBounds(152, 166, 338, 31);
		panel_securityQ.add(securityQuestion2);
		securityQuestion2.setModel(new DefaultComboBoxModel(new String[] {"-SELECT SECURITY QUESTION-", "In what city or town did your mother and father meet?", "What is the name of your childhood friend?", "In what city or town was your first job?", "What was your childhood nickname?", "What is your favorite song?"}));
		securityQuestion2.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 11));
		
		securityAnswer2 = new JTextField();
		securityAnswer2.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_ENTER) {
					btn_Register.doClick();
				}
			}
		});
		securityAnswer2.setBounds(152, 208, 338, 30);
		panel_securityQ.add(securityAnswer2);
		securityAnswer2.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 11));
		securityAnswer2.setColumns(10);
		
		JLabel lbl_description_1 = new JLabel("Answer the following security questions.");
		lbl_description_1.setFont(new Font("Baskerville Old Face", Font.PLAIN, 16));
		lbl_description_1.setBounds(10, 11, 480, 21);
		panel_securityQ.add(lbl_description_1);
		
		JLabel lbl_createAccount = new JLabel("Create an Account");
		lbl_createAccount.setFont(new Font("Baskerville Old Face", Font.PLAIN, 34));
		lbl_createAccount.setBounds(130, 11, 269, 45);
		contentPane.add(lbl_createAccount);
		
		JLabel Background = new JLabel("");
		Background.setIcon(new ImageIcon(CreateAccountFrame.class.getResource("/imgSource/registerFrame BG.jpg")));
		Background.setBounds(0, 0, 524, 461);
		contentPane.add(Background);
	}
}
