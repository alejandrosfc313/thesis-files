package accountManagementClasses;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import DatabaseClasses.DBConnection;
import LoginClassComponents.LoginFrame;
import it.sauronsoftware.junique.AlreadyLockedException;
import it.sauronsoftware.junique.JUnique;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.ImageIcon;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.JPasswordField;
import javax.swing.border.LineBorder;
import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class AccountRecoveryFrame extends JFrame {

	private JPanel contentPane;
	DBConnection db = new DBConnection();
	private JTextField field_empID;
	private JTextField field_Name;
	private JTextField field_securityQ1;
	private JTextField field_securityA1;
	private JTextField field_securityQ2;
	private JTextField field_securityA2;
	private JButton btn_identify;
	private JPanel passwordResetPanel;
	private JPanel userVerificationPanel;
	private JPasswordField field_newPassword;
	private JPasswordField field_confirmPassword;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				String appId = "myapplicationid";
				boolean alreadyRunning;
				try {
					JUnique.acquireLock(appId);
					alreadyRunning = false;
				} catch (AlreadyLockedException e) {
					alreadyRunning = true;
					JOptionPane.showMessageDialog(null, "Application is already running!");
				}
				if (!alreadyRunning) {
					// Start sequence here
					AccountRecoveryFrame frame = new AccountRecoveryFrame();
					frame.setVisible(true);
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AccountRecoveryFrame() {
		setResizable(false);
		setTitle("Account Recovery");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 510, 510);
		setLocationRelativeTo(null);
		setUndecorated(false);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		userVerificationPanel = new JPanel();
		userVerificationPanel.setOpaque(false);
		userVerificationPanel.setVisible(false);
		
		JPanel userIdentificationPanel = new JPanel();
		userIdentificationPanel.setOpaque(false);
		userIdentificationPanel.setBounds(10, 11, 474, 449);
		contentPane.add(userIdentificationPanel);
		userIdentificationPanel.setLayout(null);
		
		JLabel lbl_title = new JLabel("We need to identify your account first");
		lbl_title.setHorizontalAlignment(SwingConstants.LEFT);
		lbl_title.setFont(new Font("Baskerville Old Face", Font.PLAIN, 28));
		lbl_title.setBounds(10, 28, 454, 44);
		userIdentificationPanel.add(lbl_title);
		
		JLabel lbl_subtitle = new JLabel("Enter your user ID and name.");
		lbl_subtitle.setFont(new Font("Baskerville Old Face", Font.PLAIN, 16));
		lbl_subtitle.setBounds(22, 72, 452, 28);
		userIdentificationPanel.add(lbl_subtitle);
		
		JLabel lbl_empID = new JLabel("Employee ID");
		lbl_empID.setFont(new Font("Baskerville Old Face", Font.PLAIN, 16));
		lbl_empID.setBounds(73, 184, 86, 28);
		userIdentificationPanel.add(lbl_empID);
		
		field_empID = new JTextField();
		field_empID.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_ENTER) {
					btn_identify.doClick();
				}
			}
		});
		field_empID.setFont(new Font("Segoe UI", Font.PLAIN, 12));
		field_empID.setBounds(169, 177, 215, 35);
		userIdentificationPanel.add(field_empID);
		field_empID.setColumns(10);
		
		field_Name = new JTextField();
		field_Name.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_ENTER) {
					btn_identify.doClick();
				}
			}
		});
		field_Name.setFont(new Font("Segoe UI", Font.PLAIN, 12));
		field_Name.setBounds(169, 223, 215, 35);
		userIdentificationPanel.add(field_Name);
		field_Name.setColumns(10);
		
		JLabel lbl_name = new JLabel("Name");
		lbl_name.setFont(new Font("Baskerville Old Face", Font.PLAIN, 16));
		lbl_name.setBounds(118, 230, 41, 28);
		userIdentificationPanel.add(lbl_name);
		
		btn_identify = new JButton("");
		btn_identify.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				btn_identify.setIcon(new ImageIcon(AccountRecoveryFrame.class.getResource("/imgSource/VerifyBtn.png")));
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btn_identify.setIcon(new ImageIcon(AccountRecoveryFrame.class.getResource("/imgSource/VerifyBtn.png")));
			}
			@Override
			public void mousePressed(MouseEvent e) {
				btn_identify.setIcon(new ImageIcon(AccountRecoveryFrame.class.getResource("/imgSource/VerifyBtn_clicked.png")));
			}
			@Override
			public void mouseEntered(MouseEvent e) {
				btn_identify.setIcon(new ImageIcon(AccountRecoveryFrame.class.getResource("/imgSource/VerifyBtn_hover.png")));
			}
		});
		btn_identify.setBorder(new LineBorder(new Color(0, 0, 0)));
		btn_identify.setIcon(new ImageIcon(AccountRecoveryFrame.class.getResource("/imgSource/VerifyBtn.png")));
		btn_identify.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String empID = field_empID.getText();
				String name = field_Name.getText();
				
				if(empID.equals("") || name.equals("")) {
					JOptionPane.showMessageDialog(null, "Incomplete Credentials","Error",JOptionPane.ERROR_MESSAGE);
				}
				else {
					
					try {                 																

	                    PreparedStatement pst = (PreparedStatement) db.con().prepareStatement("SELECT id, name from studentinfo.credentialsdb WHERE id=? and name=?");

	                    pst.setString(1, empID);
	                    pst.setString(2, name);
	                    ResultSet rs = pst.executeQuery();
	                    if (rs.next()) {
	    					userIdentificationPanel.setVisible(false);
	    					userVerificationPanel.setVisible(true);
	    					loadQuestions();
	    					
	                    } else {
	                        JOptionPane.showMessageDialog(null, "No related credentials found", "Error", JOptionPane.ERROR_MESSAGE);
	                    }
	                    db.con().close();
	                } catch (SQLException sqlException) {
	                    JOptionPane.showMessageDialog(null, sqlException);
	               }
				}
			}
		});
		btn_identify.setFont(new Font("Baskerville Old Face", Font.PLAIN, 18));
		btn_identify.setBounds(178, 326, 115, 45);
		userIdentificationPanel.add(btn_identify);
		userVerificationPanel.setBounds(10, 11, 474, 449);
		contentPane.add(userVerificationPanel);
		userVerificationPanel.setLayout(null);
		
		JLabel lbl_title_1 = new JLabel("We need to confirm you identity first");
		lbl_title_1.setHorizontalAlignment(SwingConstants.LEFT);
		lbl_title_1.setFont(new Font("Baskerville Old Face", Font.PLAIN, 28));
		lbl_title_1.setBounds(10, 28, 454, 44);
		userVerificationPanel.add(lbl_title_1);
		
		JLabel lbl_subtitle_1 = new JLabel("Answer the following questions.");
		lbl_subtitle_1.setFont(new Font("Baskerville Old Face", Font.PLAIN, 15));
		lbl_subtitle_1.setBounds(22, 73, 452, 28);
		userVerificationPanel.add(lbl_subtitle_1);
		
		JLabel lbl_securityQ1 = new JLabel("Security Question 1");
		lbl_securityQ1.setFont(new Font("Baskerville Old Face", Font.PLAIN, 16));
		lbl_securityQ1.setBounds(52, 160, 130, 26);
		userVerificationPanel.add(lbl_securityQ1);
		
		JLabel lbl_securityA1 = new JLabel("Security Answer 1");
		lbl_securityA1.setFont(new Font("Baskerville Old Face", Font.PLAIN, 16));
		lbl_securityA1.setBounds(62, 197, 121, 26);
		userVerificationPanel.add(lbl_securityA1);
		
		field_securityQ1 = new JTextField();
		field_securityQ1.setEditable(false);
		field_securityQ1.setFont(new Font("Segoe UI", Font.PLAIN, 12));
		field_securityQ1.setBounds(193, 160, 220, 26);
		userVerificationPanel.add(field_securityQ1);
		field_securityQ1.setColumns(10);
		
		field_securityA1 = new JTextField();
		field_securityA1.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_ENTER) {
					verifyAnswers();
				}
			}
		});
		field_securityA1.setFont(new Font("Segoe UI", Font.PLAIN, 12));
		field_securityA1.setColumns(10);
		field_securityA1.setBounds(194, 197, 220, 26);
		userVerificationPanel.add(field_securityA1);
		
		JLabel lbl_securityQ2 = new JLabel("Security Question 2");
		lbl_securityQ2.setFont(new Font("Baskerville Old Face", Font.PLAIN, 16));
		lbl_securityQ2.setBounds(50, 234, 132, 26);
		userVerificationPanel.add(lbl_securityQ2);
		
		field_securityQ2 = new JTextField();
		field_securityQ2.setEditable(false);
		field_securityQ2.setBounds(193, 234, 220, 26);
		userVerificationPanel.add(field_securityQ2);
		field_securityQ2.setColumns(10);
		
		field_securityA2 = new JTextField();
		field_securityA2.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_ENTER) {
					verifyAnswers();
				}
			}
		});
		field_securityA2.setFont(new Font("Segoe UI", Font.PLAIN, 12));
		field_securityA2.setColumns(10);
		field_securityA2.setBounds(193, 271, 220, 26);
		userVerificationPanel.add(field_securityA2);
		
		JLabel lbl_securityA1_1 = new JLabel("Security Answer 2");
		lbl_securityA1_1.setFont(new Font("Baskerville Old Face", Font.PLAIN, 16));
		lbl_securityA1_1.setBounds(61, 271, 121, 26);
		userVerificationPanel.add(lbl_securityA1_1);
		
		JButton btn_submit = new JButton("");
		btn_submit.setBorder(new LineBorder(new Color(0, 0, 0)));
		btn_submit.setIcon(new ImageIcon(AccountRecoveryFrame.class.getResource("/imgSource/SubmitBtn.png")));
		btn_submit.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseExited(MouseEvent e) {
				btn_submit.setIcon(new ImageIcon(AccountRecoveryFrame.class.getResource("/imgSource/SubmitBtn.png")));
			}
			@Override
			public void mouseReleased(MouseEvent e) {
				btn_submit.setIcon(new ImageIcon(AccountRecoveryFrame.class.getResource("/imgSource/SubmitBtn.png")));
			}
			@Override
			public void mouseEntered(MouseEvent e) {
				btn_submit.setIcon(new ImageIcon(AccountRecoveryFrame.class.getResource("/imgSource/SubmitBtn_hover.png")));
			}
			@Override
			public void mousePressed(MouseEvent e) {
				btn_submit.setIcon(new ImageIcon(AccountRecoveryFrame.class.getResource("/imgSource/SubmitBtn_clicked.png")));
			}
		});
		btn_submit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				verifyAnswers();
			}
		});
		btn_submit.setFont(new Font("Baskerville Old Face", Font.PLAIN, 18));
		btn_submit.setBounds(179, 339, 115, 45);
		userVerificationPanel.add(btn_submit);
		
		passwordResetPanel = new JPanel();
		passwordResetPanel.setOpaque(false);
		passwordResetPanel.setVisible(false);
		passwordResetPanel.setBounds(10, 11, 474, 449);
		contentPane.add(passwordResetPanel);
		passwordResetPanel.setLayout(null);
		
		JLabel lbl_title_2 = new JLabel("Enter your new password");
		lbl_title_2.setHorizontalAlignment(SwingConstants.LEFT);
		lbl_title_2.setFont(new Font("Baskerville Old Face", Font.PLAIN, 30));
		lbl_title_2.setBounds(10, 28, 454, 44);
		passwordResetPanel.add(lbl_title_2);
		
		JLabel lbl_subtitle_2 = new JLabel("Enter and confirm your new password");
		lbl_subtitle_2.setFont(new Font("Baskerville Old Face", Font.PLAIN, 16));
		lbl_subtitle_2.setBounds(22, 71, 442, 28);
		passwordResetPanel.add(lbl_subtitle_2);
		
		JLabel lbl_newPassword = new JLabel("New Password");
		lbl_newPassword.setFont(new Font("Baskerville Old Face", Font.PLAIN, 16));
		lbl_newPassword.setBounds(94, 172, 93, 28);
		passwordResetPanel.add(lbl_newPassword);
		
		JLabel lbl_confirmPassword = new JLabel("Confirm Password");
		lbl_confirmPassword.setFont(new Font("Baskerville Old Face", Font.PLAIN, 16));
		lbl_confirmPassword.setBounds(69, 228, 118, 28);
		passwordResetPanel.add(lbl_confirmPassword);
		
		field_newPassword = new JPasswordField();
		field_newPassword.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_ENTER) {
					changePassword();
				}
			}
		});
		field_newPassword.setBounds(202, 165, 200, 35);
		passwordResetPanel.add(field_newPassword);
		
		field_confirmPassword = new JPasswordField();
		field_confirmPassword.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_ENTER) {
					changePassword();
				}
			}
		});
		field_confirmPassword.setBounds(202, 221, 200, 35);
		passwordResetPanel.add(field_confirmPassword);
		
		JButton btn_save = new JButton("");
		btn_save.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseExited(MouseEvent e) {
				btn_save.setIcon(new ImageIcon(AccountRecoveryFrame.class.getResource("/imgSource/SCBtn.png")));
			}
			@Override
			public void mouseReleased(MouseEvent e) {
				btn_save.setIcon(new ImageIcon(AccountRecoveryFrame.class.getResource("/imgSource/SCBtn.png")));
			}
			@Override
			public void mouseEntered(MouseEvent e) {
				btn_save.setIcon(new ImageIcon(AccountRecoveryFrame.class.getResource("/imgSource/SCBtn_hover.png")));
			}
			@Override
			public void mousePressed(MouseEvent e) {
				btn_save.setIcon(new ImageIcon(AccountRecoveryFrame.class.getResource("/imgSource/SCBtn_clicked.png")));
			}
		});
		btn_save.setIcon(new ImageIcon(AccountRecoveryFrame.class.getResource("/imgSource/SCBtn.png")));
		btn_save.setBorder(new LineBorder(new Color(0, 0, 0)));
		btn_save.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				changePassword();
			}
		});
		btn_save.setFont(new Font("Baskerville Old Face", Font.PLAIN, 18));
		btn_save.setBounds(170, 310, 132, 45);
		passwordResetPanel.add(btn_save);
		
		JLabel Background = new JLabel("");
		Background.setIcon(new ImageIcon(AccountRecoveryFrame.class.getResource("/imgSource/AccountRecoveryBackground.jpg")));
		Background.setBounds(0, 0, 494, 471);
		contentPane.add(Background);
		
		
	}
	
	public void loadQuestions() {
		try {
			String query = "SELECT `security question 1`,`security question 2` FROM studentinfo.credentialsdb WHERE `id` = '" + field_empID.getText() + "';";
			PreparedStatement pst = db.con().prepareStatement(query);
			ResultSet rs = pst.executeQuery(query);
			
			if(rs.next()) {
				String securityQuestion1 = rs.getString(1);
				String securityQuestion2 = rs.getString(2);
				
				field_securityQ1.setText(securityQuestion1);
				field_securityQ2.setText(securityQuestion2);
			}
			db.con().close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void verifyAnswers() {
		try {
			String query = "SELECT `security answer 1`,`security answer 2` FROM studentinfo.credentialsdb WHERE `id` = '" + field_empID.getText() + "';";
			PreparedStatement pst = db.con().prepareStatement(query);
			ResultSet rs = pst.executeQuery(query);
			
			if(rs.next()) {
				String securityAnswer1 = rs.getString(1);
				String securityAnswer2 = rs.getString(2);
				
				if(field_securityA1.getText().equalsIgnoreCase(securityAnswer1) && field_securityA2.getText().equalsIgnoreCase(securityAnswer2)) {
					userVerificationPanel.setVisible(false);
					passwordResetPanel.setVisible(true);
				}
				else {
					JOptionPane.showMessageDialog(null, "Incorrect Answer", "Please try again", JOptionPane.ERROR_MESSAGE);
					field_securityA1.setText("");
					field_securityA2.setText("");
				}
			}
			db.con().close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void changePassword() {
		String newPassword = field_newPassword.getText();
		String confirmPassword = field_confirmPassword.getText();
		
		if(newPassword.equals("") || confirmPassword.equals("")) {
			JOptionPane.showMessageDialog(null, "Field cannot be empty", "Please try again", JOptionPane.ERROR_MESSAGE);
		}
		else {
			if(newPassword.equals(confirmPassword)) {
				try {
					String query = "UPDATE `studentinfo`.`credentialsdb` SET `Password` = '"+confirmPassword+"' WHERE (`id` = '"+field_empID.getText()+"');";
					PreparedStatement pst = db.con().prepareStatement(query);
					pst.execute();
					JOptionPane.showMessageDialog(null, "Password reset successfully","Password Reset", JOptionPane.INFORMATION_MESSAGE);
					JOptionPane.showMessageDialog(null, "You will now be directed to the login screen","Confirmation", JOptionPane.INFORMATION_MESSAGE);
					dispose();
					LoginFrame frame = new LoginFrame();
					frame.setVisible(true);
					db.con().close();
				}
				catch(SQLException e) {
					e.printStackTrace();
				}
			}
			else {
				JOptionPane.showMessageDialog(null, "Password Mismatch", "Please try again", JOptionPane.ERROR_MESSAGE);
			}
		}
	}
}
